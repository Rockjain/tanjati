/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import Store from './src/Redux/Store';
import { Provider } from 'react-redux';
import App from './App';
import {name as appName} from './app.json';
const appdatas = () => (
    <Provider store = { Store }>
      <App />
    </Provider>
  )
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => appdatas);
