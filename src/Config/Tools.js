import greyback from '../assets/Images/greyback.png';

export default class Tools {

  /**
   * Get data image size base on the width
   */
  static getImageSize(data, widthScreen) {
    var size = { width: widthScreen, height: 0 }

    if (data.better_featured_image) {
      const { width, height } = data.better_featured_image['media_details'][
        'sizes'
      ]['large']
      size.height = (widthScreen * height) / width
    }else{
      size.height = (widthScreen * 280) / 336
    }
    return size
  }

}
