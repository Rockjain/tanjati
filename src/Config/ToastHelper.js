import React from "react";
import {
  Toast,
} from "native-base";

class ToastHelper {
  static danger = function(message) {
    Toast.show({
      text: message,
      duration: 2500,
      position: "top",
      type: "danger",
      textStyle: {textAlign: "center", color: 'white'},
      buttonText: 'Okay'
    })
  }

  static success = function(message) {
    Toast.show({
      text: message,
      duration: 2500,
      position: "top",
      type: 'success',
      textStyle: {textAlign: "center", color: 'white'},
      buttonText: 'Okay'
    })
  }

}

export default ToastHelper;