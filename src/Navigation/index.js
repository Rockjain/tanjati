import React from 'react';
import { Image } from 'react-native';
import {createSwitchNavigator,createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from 'react-redux';
import Splashscreen from '../Screens/Authscreen/Splash';
import Homescreen from '../Screens/Mainscreens/Home';
import Mapscreen from '../Screens/Mainscreens/Map';
import Profilecreen from '../Screens/Mainscreens/Profile';
import Categoryscreen from '../Screens/Mainscreens/Category';
import Bookmarkscreen from '../Screens/Mainscreens/Bookmarks';
import Issuescreen from '../Screens/Mainscreens/Issue';
import Loginscreen from '../Screens/Mainscreens/Login';
import Settingscreen from '../Screens/Mainscreens/Settings';
import Contactscreen from '../Screens/Mainscreens/Contact';
import VerificationScreen from '../Screens/Mainscreens/Verification';
import FeaturesScreen from '../Screens/Mainscreens/Features';
import SubCategoryScreen from '../Screens/Mainscreens/SubCategory';
import PostDetailScreen from '../Screens/Mainscreens/PostDetail';
import ListDetailScreen from '../Screens/Mainscreens/ListDetail';
import CategoryDataScreen from '../Screens/Mainscreens/CategoryData';
import CategoryTabScreen from '../Screens/Mainscreens/CategoryTab';
import GetIssueScreen from '../Screens/Mainscreens/GetIssue';
import MylistingScreen from '../Screens/Mainscreens/Mylisting';
import IssueDetailScreen from '../Screens/Mainscreens/IssueDetail';
import IssueMapScreen from '../Screens/Mainscreens/Issuemap';
import Drawer from '../Drawer';
import TabBar from '../Components/TabBar';

import Colors from '../Config/Colors';
const homeimage = require('../assets/Images/icons/tab-home.png');
const categoryimage = require('../assets/Images/icons/tab-category.png');
const mapimage = require('../assets/Images/icons/tab-pin.png');
const profileimage = require('../assets/Images/icons/tab-user.png');
const plus = require('../assets/Images/ic-plus.png')
let add = null;
let issuecolor=Colors.tabbarTint
const homeStack = createStackNavigator({
    home:Homescreen,
    features:FeaturesScreen,
    subcat:SubCategoryScreen,
    list:ListDetailScreen,
    catdata:CategoryDataScreen,
},{
  initialRouteName:'home',
    headerMode:'none'
})
// const CatTabStack = createStackNavigator({
//   CatTab:CategoryTabScreen
// },{
//   headerMode:'none',
//   initialRouteName:'CatTab'
// })

const categoryStack = createStackNavigator({
    category:Categoryscreen,
    subcat:SubCategoryScreen,
    catdata:CategoryDataScreen,
    cattab:CategoryTabScreen,
},{
  initialRouteName:'category',
    headerMode:'none'
})

const mapStack = createStackNavigator({
    map:Mapscreen,
    list:ListDetailScreen,
    subcat:SubCategoryScreen,
     //postdetail:PostDetailScreen
    
},{
    initialRouteName:'map',
    headerMode:'none'
})

const issueStack = createStackNavigator({
    issue:Issuescreen,
    getissue:GetIssueScreen,
    issuemap:IssueMapScreen
},{
    headerMode:'none',
    initialRouteName:'issue'
})

const postDetailStack = createStackNavigator({
  postdetail:PostDetailScreen,
},{
  headerMode:'none',
  initialRouteName:'postdetail'
})

const loginStack = createStackNavigator({
  Login:Loginscreen,
  verification:VerificationScreen,
},{
  headerMode:'none',
  initialRouteName:'Login'
})

const settingStack = createStackNavigator({
  Setting:Settingscreen,
},{
  headerMode:'none',
  initialRouteName:'Setting'
})

const contactStack = createStackNavigator({
  Contact:Contactscreen
},{
  headerMode:'none',
  initialRouteName:'Contact'
})

const profileStack = createStackNavigator({
    profile:Profilecreen,
    mylisting:MylistingScreen,
    bookmark:Bookmarkscreen,
    issuedetail:IssueDetailScreen,
    verification:VerificationScreen,
    contact:contactStack,
},{
  initialRouteName:'profile',
    headerMode:'none'
})


  const MainStack = createDrawerNavigator({
    // bottom: bottomTab,
    Home: homeStack,
      Category: categoryStack,
      Map:mapStack,
      Profile: profileStack,
    setting:settingStack,
    contact:contactStack,
    login:loginStack,
    //postdetail:PostDetailScreen
},{
    initialRouteName: 'Home',
    contentComponent:Drawer,
})

const MStack = createSwitchNavigator({
       mains:MainStack,
      postdetail:PostDetailScreen,
      postdetails:PostDetailScreen,
},{
  initialRouteName:'mains',
  headerMode:'none'
})

const userMainStack = createDrawerNavigator({
    // userbottom: userBottomTab,
    Home: homeStack,
    Category: categoryStack,
     Issue: issueStack,
    Map:mapStack,
     Profile: profileStack,
    setting:settingStack,
    contact:contactStack,
    login:loginStack,
},{
    initialRouteName: 'Home',
    contentComponent:Drawer,
})

const MUser = createSwitchNavigator({
   usermains:userMainStack,
   postdetail:PostDetailScreen,
   postdetails:PostDetailScreen,
},{
  initialRouteName:'usermains',
  headerMode:'none'
})

const AllStack = createSwitchNavigator({
  splash: Splashscreen,
  user:MUser,
  main:MStack, 
  //postdetail:PostDetailScreen,
},{
  initialRouteName: 'splash'
})


export default Route = createAppContainer(AllStack);
