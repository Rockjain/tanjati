import React from 'react';
import { View, Dimensions ,TouchableOpacity,SafeAreaView,Image,AsyncStorage,StatusBar} from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import styles from './Styles';
import Listview from './List';
import Mapview from './Map';
import { connect } from 'react-redux';
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
import Animated from 'react-native-reanimated';
const backlong=require('../../../assets/Images/backlong.png')
import Footer from '../../../Components/Footer';
import Loadings from '../../../Components/Loadings';


 class CategoryTab extends React.Component{
  state = {
    index: 0,
    routes: [
      {
        key: 'first',
        title: Languages.Map,
      },
      {
        key: 'second',
        title: Languages.List,
      },
    ],
  }
  componentWillMount = async() =>{
    this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
  }
      MapView = () =>{
        return(
          <Mapview navigation ={this.props.navigation}/>
        )
      }

      SearchView = () =>{
        return(
          <Listview navigation ={this.props.navigation}/>
        )
      }
     
    render(){
      const {navigation,isFetching} = this.props;
      // if (isFetching) {
      //   return(
      //     <Loadings />
      //   )
      // }
        return(
          <SafeAreaView style={{ flex:1 }}>
             <View style={{padding:10,backgroundColor:'#0579D4'}}>
            <TouchableOpacity onPress={()=>navigation.navigate('category')} >
            <Image source={backlong} style={styles.headerImage}></Image>
            </TouchableOpacity>
            </View>
          <TabView
           navigationState={this.state}
          style={styles.container}
          renderScene={SceneMap({ first: this.MapView, second: this.SearchView })}
          onIndexChange={(index) => this.setState({ index })}
          initialLayout={{
            width: Dimensions.get('window').width,
          }}
        />
        <Footer />
        <StatusBar hidden={true} />
        </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
  return{
    isFetching:state.Post.isFetching,
  }
  }
  
export default connect(
    mapStateToProps
  )(CategoryTab)