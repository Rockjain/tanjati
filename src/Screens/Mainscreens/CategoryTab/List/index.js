import React from 'react';
import { View, Text, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, TextInput, AsyncStorage } from 'react-native';
import Styles from './Styles';
import { connect } from 'react-redux';
import Languages from '../../../../Config/Languages';
const demoimg = require('../../../../assets/Images/category/cate1.jpg');
import Rating from '../../../../Components/react-native-star-rating';
import Colors from '../../../../Config/Colors';
const starfill = require('../../../../assets/Images/star-fill.png');
const starempty = require('../../../../assets/Images/star.png');
 const greyback = require('../../../../assets/Images/greyback.png');
import Loadings from '../../../../Components/Loadings';

class Listview extends React.Component{
  constructor(props){
    super(props)
    this.state={
      inputtxt:'',
      navdata: props.navigation.getParam('navdata'),
    }
  }
    componentWillMount = () =>{
        console.log(this.props.posts)
        
    }
    renderRating=(value)=>{
       // if (item.totalRate && item.totalRate != 'undefined') {
           return (
               <View style={Styles.ratingView}>
                
                   <Rating
                     disabled={false}
                     emptyStar={'star-o'}
                     fullStar={'star'}
                     halfStar={'star-half-o'}
                     iconSet={'FontAwesome'}
                     maxStars={5}
                     starSize={12}
                     starStyle={{ marginLeft:2 }}
                     halfStarEnabled
                     rating={Number(value)}
                     fullStarColor={Colors.active}
                     emptyStarColor={'#ccc'}
                   />
                
               </View>
               )  
       //  } else{
       //      return null
       //  }  
         
       }
   renderRecents=({item})=>{
       let status=item.more_options.price_status
       let value = 5
       const priceFrom =item.more_options.list_price != '' && typeof item.more_options.list_price != 'undefined'
       ? item.more_options.list_price
       : ''
       let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
       let listing_Cat_replaced=listing_Cat_string.replace('-','_')
        let listing_Cat= JSON.parse(listing_Cat_replaced)
        //  let imageURL = 'http://'+item.gallery_images[0].substring(7, item.length);
        let imageURL = greyback;   
     return(
         <TouchableOpacity style={{ marginTop:15 }} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'list' })}>
       <View style={Styles.featureview}>
       <ImageBackground
        source={item.better_featured_image?{ uri:'https://tanjati.versiondigitale.net/wp-content/uploads/'+item.better_featured_image.media_details.file}:imageURL}
       style={Styles.featureimage}
       imageStyle={{ borderRadius:2 }}
       >
       {/* {this.renderPrice(item.more_options)}
       {this.renderRating(item.more_options)} */}
       </ImageBackground>
       <View style={{ marginLeft:5 }}>
       <Text style={Styles.featurename}>{item.title.rendered}</Text>
       <Text style={Styles.quality}>{priceFrom}</Text>
       <Text style={Styles.quality} numberOfLines={2}>{item.listing_data.lp_listingpro_options.tagline_text}</Text>
         {this.renderRating(value)}
       </View> 
   </View>
   </TouchableOpacity>
     )
   }

   renderRecentsSearch=({item})=>{
    console.log('item')
    console.log(item)
    let status=item.more_options.price_status
    let value = 5
    const priceFrom =item.more_options.list_price != '' && typeof item.more_options.list_price != 'undefined'
    ? item.more_options.list_price
    : ''
    let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
    let listing_Cat_replaced=listing_Cat_string.replace('-','_')
     let listing_Cat= JSON.parse(listing_Cat_replaced)
    //  let imageURL = 'http://'+item.gallery_images[0].substring(7, item.length);   
  return(
      <TouchableOpacity style={{ marginTop:15 }} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'list' })}>
    <View style={Styles.featureview}>
    <ImageBackground
     source={!item.gallery_images?greyback:{ uri:'http://'+item.gallery_images[0].substring(7, item.length)}}
    style={Styles.featureimage}
    imageStyle={{ borderRadius:2 }}
    >
    {/* {this.renderPrice(item.more_options)}
    {this.renderRating(item.more_options)} */}
    </ImageBackground>
    <View style={{ marginLeft:5 }}>
    <Text style={Styles.featurename}>{item.title.rendered}</Text>
    <Text style={Styles.quality}>{priceFrom}</Text>
    <Text style={Styles.quality} numberOfLines={2}>{item.listing_data.lp_listingpro_options.tagline_text}</Text>
      {this.renderRating(value)}
    </View> 
</View>
</TouchableOpacity>
  )
}
   gotoSubCat=(item)=>{
       console.log(item)
       AsyncStorage.setItem('CatID',item.id.toString())
       AsyncStorage.setItem('CatName',item.name)
     this.props.navigation.navigate('subcat')
   }

   renderSeparator = () => (
       <View
         style={{
           borderBottomColor: Colors.lightgrey,
           borderBottomWidth:0.5,
           marginTop:8
         }}
       />
     );
     renderList = (posts) =>{
       const {searchresult} = this.props;
       const {inputtxt} = this.state;
       console.log('searchresult')
       console.log(searchresult)
       if(searchresult && searchresult.length != 0 && inputtxt != ''){
         return(
           <FlatList
           data={searchresult}
           ItemSeparatorComponent={this.renderSeparator}
           keyExtractor={item => item.id}
           renderItem={this.renderRecentsSearch}
           style={{ marginBottom:50 }}
           ></FlatList>
          )
       } else {
         return(
           <FlatList
           data={posts}
           ItemSeparatorComponent={this.renderSeparator}
           keyExtractor={item => item.id}
           renderItem={this.renderRecents}
           style={{ marginBottom:50 }}
           ></FlatList>
          )
       }
        
     }
   checkInput = (text) =>{
    this.setState({ inputtxt:text })
     this.props.dispatch({type:'Search_Post_Request',payload:{url:'/listing?search='+text}})
   }  
   render(){
       const {categories,posts,navigation,isFetching}=this.props
       const {navdata} = this.state
       if (navdata != 'navdata') {
      }
       return(
           <View style={Styles.container}>
            <View style={[Styles.containers]}>
       <TextInput
         style={Styles.input}
         autoCapitalize={'none'}
         autoCorrect={false}
         placeholder={Languages.searchKeyword}
         placeholderTextColor="#999"
         underlineColorAndroid={'transparent'}
         onChangeText={(text)=>{this.checkInput(text)}}
         clearButtonMode={'while-editing'}
         />
     </View>
           <ScrollView>
           <Text style={Styles.featuretxt}>{Languages.categories}</Text>
           <View style={{ height:120 }}>
                <FlatList
            data = {categories}
            keyExtractor={item => item.id}
            horizontal={true}
            renderItem={({ item }) => (
                <TouchableOpacity onPress={()=>this.gotoSubCat(item)}>
                <ImageBackground source={demoimg} style={Styles.findlistview}  imageStyle={{borderRadius:8}}>
                <Text style={Styles.findtxt}>{item.name}</Text>
                </ImageBackground>
                </TouchableOpacity>
            )}
            ></FlatList>
            </View>
            <Text style={Styles.featuretxt}>{Languages.recents}</Text>
           {this.renderList(posts)}
           </ScrollView>
           </View>
       )
   }
}

const mapStateToProps=(state)=>{
   return{
    isFetching: state.Category.isFetching,
      categories:state.Category.Categories,
      posts:state.Post.posts ,
      user: state.Auth.UserInfo,
      searchresult:state.Post.searchpost
   }
}
export default connect(mapStateToProps)(Listview)