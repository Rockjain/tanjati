import { StyleSheet, Dimensions, Platform } from 'react-native';
import Colors from '../../../../Config/Colors';
const {width,height}=Dimensions.get('window')
export default StyleSheet.create({
    container:{
        flex:1
    },
    bannerview:{
        width:Dimensions.get('window').width,
        height:240,
        justifyContent:'center'
    },
    headtxt:{
        color:Colors.main,
        fontSize:20,
        marginLeft:20,
        fontWeight:'bold',
        
    },
    txtview:{
        marginBottom:20
    },
    inputview:{
        marginHorizontal:10,
        borderRadius:5,
        marginTop:-55,
        paddingHorizontal:10,
        //paddingVertical:5,
        backgroundColor:Colors.main,
        flexDirection:'row',
        height:45
    },
    findheadtxt:{
        marginTop:60,
        fontSize:22, 
        fontWeight:'bold',
        marginLeft:10
    },
    findlistview:{
        marginTop:16,
        borderRadius:6,
        marginLeft:6,
        height:102,
        width:width/2-20,
    },
    findtxt:{
        marginLeft:10,
        color:Colors.main,
        position:'absolute',
        bottom:8,
        fontSize:20,
        fontWeight:'bold'
    },
    plustxt:{
      marginLeft:10,
      fontSize:22, 
      fontWeight:'bold',
      marginTop:30  
    },
    desctxt:{
        marginLeft:10,
        fontSize:14,
        marginTop:5, 
        color:Colors.blackTextSecondary
    },
    bannerhotel:{
        height:180,
        width:Dimensions.get('window').width-10,
    },
    featuretxtview:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginHorizontal:10,
    },
    featuretxt:{
      fontSize:22, 
      fontWeight:'bold',
      marginTop:30,
      marginLeft:6,
      color:Colors.attributes.black 
    },
    seetxt:{
        color:Colors.red,
        marginTop:34,
        fontSize:12 ,
        fontWeight:'bold'
    },
    featureview:{
        flexDirection:'row',
        marginTop:16,
        borderRadius:3,
        marginLeft:6,
        height:100,
        width:'100%'
    },
    featureimage:{
        height:93,
        width:115,
        borderRadius:2
     },
    typetxt:{
        color:Colors.red,
        fontSize:13,
        marginTop:4
    },
    featurename:{
        color:Colors.attributes.black,
        fontSize:16,
    },
    quality:{
        fontSize:12,
        color:Colors.attributes.black
    },
    wrapPriceRange: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, .75)',
        padding: 5,
        borderTopRightRadius: 5,
      },
      priceRange: {
        fontSize: 11,
        marginLeft: 5,
        color: '#FFF',
      },
      ratingView: {
         marginTop:10, 
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .75)',
      },
      countText: {
        fontSize: 11,
        marginLeft: 8,
        color: '#666',
      },
      lineview:{
          borderBottomWidth:0.6,
          borderBottomColor:Colors.lightgrey,
          marginTop:5
      },
      containers: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        left: 0,
        alignItems: 'center',
        top: 0,
            shadowRadius: 2,
            shadowOpacity: 0.1,
          },
      searchIcon: {
        width: 16,
        height: 20,
        resizeMode: 'contain',
        tintColor: '#999',
        marginHorizontal: 10,
        marginTop: 5,
      },
      input: {
        marginBottom: 6,
        marginLeft: 15,
        marginRight: 15,
        flex: 1,
        fontSize: 15,
        paddingLeft: 4,
        textAlign: 'center',
        backgroundColor: '#eee',
        borderRadius: 40,
        paddingVertical:5,
    
            marginTop: 10,
            padding: 0,
          },
      btnFilSearch: {
        marginTop: 5,
        marginHorizontal: 10,
        zIndex: 999,
        marginRight: 15,
        shadowColor: 'rgba(0,0,0, .5)',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowRadius: 3,
        shadowOpacity: 0.1,
      },
      iconSearchAdvance: {
        width: 18,
        height: 18,
        tintColor: Colors.searchButton,
        resizeMode: 'contain',
      },
    })