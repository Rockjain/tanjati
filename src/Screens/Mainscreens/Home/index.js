import React from 'react';
import { View,ScrollView, ImageBackground, TextInput,Animated,Dimensions,
    Image, FlatList, TouchableOpacity, SafeAreaView,AsyncStorage,RefreshControl} from 'react-native';
import Styles from './Styles';
import { connect } from 'react-redux';
import {
    Container,
     Text, 
   } from 'native-base';
import Colors from '../../../Config/Colors';
import Constants from '../../../Config/Constants';
import IconMater from 'react-native-vector-icons/MaterialCommunityIcons'
const banner = require('../../../assets/Images/banner/banner3.jpg')
const search = require('../../../assets/Images/icons/tab-search.png')
const demoimg = require('../../../assets/Images/category/cate1.jpg')
const bannerhotel = require('../../../assets/Images/banner-hotel.png')
const featureimg = require('../../../assets/Images/category/cate2.jpg')
const activedoller = require('../../../assets/Images/icons/icon-dollar-active.png')
const inactivedoller = require('../../../assets/Images/icons/icon-dollar-inactive.png')
const menu = require('../../../assets/Images/menu.png')
import Rating from '../../../Components/react-native-star-rating';  
import Languages from '../../../Config/Languages';
import Loading from '../../../Components/Loadings';
const greyback = require('../../../assets/Images/greyback.png')
const newplus = require('../../../assets/Images/newplus.png');
const iconmap = require('../../../assets/Images/icon-map.png')
import Footer from '../../../Components/Footer';
let user = null;
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
const {HomeLayout,Layout}=Constants
import MasonryList from '@appandflow/masonry-list';
import Tools from '../../../Config/Tools';
const { width: SCREEN_WIDTH } = Dimensions.get('window')

 class Home extends React.Component{
    constructor(props) {
        super(props);
        this.state = { 
            Feature:[],
            navdata: props.navigation.getParam('navdata'),
            iconcolor:'#FFF',
            iconname:'heart-outline',
            myuser:null,
            isLoad:false,
            isMenu:false,
            refreshing: false,
            
         }
    }
    static navigationOptions = ({ navigation }) => ({
      header: null,
    })

    componentWillMount=async()=>{
        let userinfo = await AsyncStorage.getItem('User');
       user=JSON.parse(userinfo)
       
        this.props.dispatch({type:'Fetch_Category_Request',payload:{url:'/listing-category'}})
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        if(user){
          this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}}) 
        }
        this.setState({myuser:JSON.parse(userinfo)})
    }


    renderPrice=(item)=>{
        let status=item.price_status
        let value = 1
        const priceFrom =item.list_price != '' && typeof item.list_price != 'undefined'
        ? item.list_price
        : ''
        const priceTo =
        item.list_price_to != '' && typeof item.list_price_to != 'undefined'
        ? item.list_price_to
        : ''
        const priceRange = priceFrom != '' && priceTo != '' ? priceFrom + '-' + priceTo : ''
     
        if (typeof status !== 'undefined' && status!= 'notsay' && priceRange != '') {
          if (status == 'inexpensive') value = 1
          if (status == 'moderate') value = 2
          if (status == 'pricey') value = 3
          if (status == 'ultra_high_endif') value = 4
          return(
            <View style={Styles.wrapPriceRange}>
            <Rating
            disabled
            fullStar={activedoller}
            emptyStar={inactivedoller}
            maxStars={4}
            starSize={10}
            starStyle={{ marginLeft: -2 }}
            rating={Number(value)}
            fullStarColor='#D5D8DE'
            emptyStarColor={'#ccc'}
          />
           <Text style={Styles.priceRange}>
                  {priceRange ? priceRange : ''}
                </Text>
          </View> 
          )
        }
    }
    renderRating=(item)=>{
    // if (item.totalRate && item.totalRate != 'undefined') {
        return (
            <View style={Styles.ratingView}>
                <Rating
                  disabled={false}
                  emptyStar={'star-o'}
                  fullStar={'star'}
                  halfStar={'star-half-o'}
                  iconSet={'FontAwesome'}
                  maxStars={1}
                  starSize={10}
                  starStyle={{ marginLeft:2 }}
                  halfStarEnabled
                  rating={Number(5)}
                  fullStarColor={Colors.active}
                  emptyStarColor={'#ccc'}
                />
                <Text style={Styles.countText}>
                {item.totalRate ? item.totalRate : 5}
              </Text>
             
            </View>
            )  
    //  } else{
    //      return null
    //  }  
      
    }
        
      checkExist = (post) => {
        const {myuser} = this.state;
        if (myuser) {
          const {getwishlist} = this.props;
          console.log(getwishlist)
          if (getwishlist == null || getwishlist == undefined) {
            
          } else{
            const myarray = Object.values( getwishlist );
            console.log(myarray)
           const isExists = myarray.find((item) => item == post.id)
           return isExists
          }
           
        } else {
          
        }
      
      }
    heartClick = async(post) =>{
      const {getwishlist,navigation} = this.props;
      const {myuser} = this.state;
        if(myuser){
          console.log(user)
              if(this.checkExist(post)){
              this.props.dispatch({type:'Remove_Wishlist_Request',payload:{url:'/unsetWishlist?user_id='+myuser.id+'&post_id='+post.id}})
            } else{
              this.props.dispatch({type:'Add_Wishlist_Request',payload:{url:'/setWishlist?user_id='+myuser.id+'&post_id='+post.id}})
            }
       let userinfo = await AsyncStorage.getItem('User');
       user=JSON.parse(userinfo)
       this.setState({myuser:JSON.parse(userinfo)})
        if(user){
          this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}}) 
          this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        }
            
        } else {
            navigation.navigate('Login')
        }   
    }

    renderHeart = (post) =>{
      const {isLoad} = this.state;
        return(
          <TouchableOpacity style={[Styles.fixHeart, { top: 1, right: 0 }]}>
         <IconMater.Button
         style={Styles.newsIcons}
         onPress={()=>this.heartClick(post)}
          name={!this.checkExist(post) ? 'heart-outline' : 'heart'}
         size={22}
         color={
           !this.checkExist(post)
             ? '#FFF'
             : Colors.mainColorTheme
         }
        key={this.state.uniqueValue}
         backgroundColor={'transparent'}
       />
       </TouchableOpacity>
      )  
    }

    goToPostDetail = async(item) =>{
      const {navigation} = this.props;
       console.log('item')
       console.log(item)
       navigation.navigate('postdetail', { items:item, navigateTo:'home' })
    }

    renderFeatures=({item})=>{
        let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
        let listing_Cat_replaced=listing_Cat_string.replace('-','_')
         let listing_Cat= JSON.parse(listing_Cat_replaced)
         const imageSize = Tools.getImageSize(item, SCREEN_WIDTH / 2)
         console.log('hii features')   
      return(
       // <TouchableOpacity style={Styles.featureview} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'home' })}>
       <TouchableOpacity style={HomeLayout.homelayout == 1 ? Styles.recentview : HomeLayout.homelayout == 3 ? Styles.viewMansory : Styles.featureview} 
       onPress={()=>this.goToPostDetail(item)}>
        <ImageBackground
        imageStyle={{borderRadius:4}}
        source={item.better_featured_image == null?greyback:{uri:'https://tanjati.versiondigitale.net/wp-content/uploads/'+item.better_featured_image.media_details.file}}
        style={HomeLayout.homelayout == 1 ? Styles.recentimage : HomeLayout.homelayout == 3 ? [Styles.imageMansory, { height: imageSize.height }] :Styles.featureimage}
        >
        {this.renderHeart(item)}
        {this.renderPrice(item.more_options)}
        {this.renderRating(item.more_options)}
        </ImageBackground>
        <Text style={Styles.typetxt}>{listing_Cat&& listing_Cat.listing_category && listing_Cat.listing_category[0].name?listing_Cat.listing_category[0].name:''}</Text>
        <Text style={Styles.featurename} numberOfLines={1}>{item.title.rendered}</Text>
        <Text style={Styles.quality} numberOfLines={2}>{item.listing_data.lp_listingpro_options.tagline_text}</Text>
        </TouchableOpacity>
      )
    }

    renderRecents=({item})=>{
        let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
        let listing_Cat_replaced=listing_Cat_string.replace('-','_')
         let listing_Cat= JSON.parse(listing_Cat_replaced)
            
      return(
        <TouchableOpacity style={Styles.recentview} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'home' })}>
        <ImageBackground
         imageStyle={{borderRadius:4}}
        source={item.better_featured_image == null?greyback:{uri:'https://tanjati.versiondigitale.net/wp-content/uploads/'+item.better_featured_image.media_details.file}}
        style={Styles.recentimage}
        >
         {this.renderHeart(item)}
        {this.renderPrice(item.more_options)}
        {this.renderRating(item.more_options)}
        </ImageBackground>
        <Text style={Styles.typetxt}>{listing_Cat && listing_Cat.listing_category && listing_Cat.listing_category[0].name?listing_Cat.listing_category[0].name:''}</Text>
        <Text style={Styles.featurename} numberOfLines={1}>{item.title.rendered}</Text>
        <Text style={Styles.quality} numberOfLines={2}>{item.listing_data.lp_listingpro_options.tagline_text}</Text>
        </TouchableOpacity>
      )
    }
    gotoSubCat=(item)=>{
        console.log(item)
        AsyncStorage.setItem('CatID',item.id.toString())
        AsyncStorage.setItem('CatName',item.name)
      this.props.navigation.navigate('catdata')
    }

    _onRefresh = async() => {
      let userinfo = await AsyncStorage.getItem('User');
       user=JSON.parse(userinfo)
       this.setState({myuser:JSON.parse(userinfo)})
        this.props.dispatch({type:'Fetch_Category_Request',payload:{url:'/listing-category'}})
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        if(user){
          this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}}) 
        }
    }

    renderFeaturesPost=(posts)=>{
     if (HomeLayout.homelayout == 1) {
       return(
        <FlatList
        data={posts}
        keyExtractor={item => item.id.toString()}
        horizontal={true}
        renderItem={this.renderFeatures}
        ></FlatList>
       )
     } else if (HomeLayout.homelayout == 2){
      return(
        <FlatList
        data={posts}
        keyExtractor={item => item.id.toString()}
        numColumns={Layout.layout <= 3 ? Layout.layout : 2}
        renderItem={this.renderFeatures}
        ></FlatList>
       )
     }else if (HomeLayout.homelayout == 3){
      return (
        <MasonryList
          onRefresh={this._onRefresh}
          refreshing={this.state.refreshing}
          data={posts}
          renderItem={this.renderFeatures}
          getHeightForItem={({ item }) => 
          Tools.getImageSize(item, SCREEN_WIDTH / 2).height
        }
          numColumns={2}
          keyExtractor={item => item.id.toString()}
        />
      )
     }
    }

    render(){
        const { chooselist, Feature,iconcolor,iconnam,myuser,navdata } = this.state;
        const {categories,posts,navigation,isFetching}=this.props
        let features=[]
        if (posts) {
          for (let i = 0; i < 3; i++) {
            features.push(posts[i])
          }
        }
       
        console.log('navdata')
        console.log(navdata)
        if (navdata != 'navdata') {
          if (isFetching) {
            return(
              <Loading />
            )
         } 
        }
        console.log('hello renderrr')
        return(
            <SafeAreaView style={Styles.container}>
                  <ScrollView
                   refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh}
                    />
                  }
                  >
               <ImageBackground
               source={banner}
               style={Styles.bannerview}
               >
               <View style={Styles.txtview}>
               <Text style={Styles.headtxt}>{Languages.browseanything}</Text>
               <Text style={Styles.headtxt}>{Languages.explorecity}</Text>
               </View>
               </ImageBackground>
             <TouchableOpacity style={Styles.inputview} onPress={()=>navigation.navigate('list')}>  
             <Image source={iconmap} style={{ height:19,width:14,marginTop:15}}></Image>
             <Text style={Styles.inputtxt}>{Languages.browsemore}</Text>
             </TouchableOpacity>
             <View style={Styles.inputlineview}></View>
             <Text style={Styles.findheadtxt}>{Languages.canhelp}</Text>
             <FlatList
             data = {categories}
             extraData={this.state.isLoad}
             keyExtractor={item => item.id.toString()}
             horizontal={true}
             renderItem={({ item }) => (
                 <TouchableOpacity onPress={()=>this.gotoSubCat(item)} style={Styles.findlistview}>
                 <Image
                 source={demoimg}
                 style={{ height:70,width:'100%',borderRadius:4}}
                 ></Image>
                 <Text style={Styles.findtxt}>{item.name}</Text>
                 </TouchableOpacity>
             )}
             ></FlatList>
             <Text style={Styles.plustxt}>{Languages.introlistpro}</Text>
             <Text style={Styles.desctxt}>{Languages.selectionhome}</Text>
            <View style={{  marginTop:10,marginHorizontal:5 }}>
            <Image
            source={bannerhotel}
            style={Styles.bannerhotel}
            ></Image>
            </View>
            <View style={Styles.featuretxtview}>
                <Text style={Styles.featuretxt}>{Languages.homefeature}</Text>
                <TouchableOpacity onPress={()=>navigation.navigate('features')}>
                <Text style={Styles.seetxt}>{Languages.homeseeall}</Text>
                </TouchableOpacity>
            </View>
            <Text style={Styles.desctxt}>{Languages.bookactivity}</Text>
            {this.renderFeaturesPost(features)}
           
            <Text style={[Styles.featuretxt,{marginLeft:10,marginTop:20}]}>{Languages.homerecent}</Text>
            <FlatList
            data={posts}
            keyExtractor={item => item.id.toString()}
            numColumns={2}
            renderItem={this.renderRecents}
            style={{marginBottom:50}}
            ></FlatList>
             </ScrollView>
             <Footer />
            </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
       categories:state.Category.Categories,
       isFetching:state.Category.isFetching,
       posts:state.Post.posts ,
       user: state.Auth.UserInfo,
       addwishlist:state.Post.addWishlist,
       getwishlist:state.Post.getWishlist,
    }
}
export default connect(mapStateToProps)(Home)