import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../../Config/Colors';
const {width,height}=Dimensions.get('window')
import Constants from '../../../Config/Constants';
const {layout}=Constants.Layout

export default StyleSheet.create({
    container:{
        flex:1
    },
    bannerview:{
        width:Dimensions.get('window').width,
        height:240,
        justifyContent:'center'
    },
    headtxt:{
        color:Colors.main,
        fontSize:20,
        marginLeft:20,
        fontWeight:'bold',
        
    },
    txtview:{
        marginBottom:20
    },
    inputview:{
        marginHorizontal:10,
        borderRadius:5,
        // marginTop:-55,
        paddingHorizontal:10,
        //paddingVertical:5,
        backgroundColor:Colors.main,
        flexDirection:'row',
        height:45
    },
    findheadtxt:{
        marginTop:20,
        fontSize:22, 
        fontWeight:'bold',
        marginLeft:10
    },
    findlistview:{
        marginTop:16,
        borderRadius:3,
        marginLeft:12,
        height:120,
        width:140,
        elevation:2,shadowOffset:{width:10,height:10},shadowOpacity:5,shadowRadius:10,borderWidth:0.5,borderColor:'#E5E7E9',shadowColor:'#E5E7E9' 

    },
    findtxt:{
        marginLeft:5,
        color:Colors.attributes.black,
        marginTop:5,
        fontWeight:'bold'
    },
    plustxt:{
      marginLeft:10,
      fontSize:22, 
      fontWeight:'bold',
      marginTop:30  
    },
    desctxt:{
        marginLeft:10,
        fontSize:14,
        marginTop:5, 
        color:Colors.blackTextSecondary
    },
    bannerhotel:{
        height:180,
        width:Dimensions.get('window').width-10,
    },
    featuretxtview:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginHorizontal:10,
    },
    featuretxt:{
      fontSize:22, 
      fontWeight:'bold',
      marginTop:30 
    },
    seetxt:{
        color:Colors.red,
        marginTop:34,
        fontSize:12 ,
        fontWeight:'bold'
    },
    featureview:{
        marginTop:10,
        borderRadius:4,
        marginLeft:12,
        height:240,
        width:layout == 1 ? width-20 : layout == 2 ? width/2-20 : width/3-18,
        marginBottom:8
    },
    featureimage:{
        height:150,
        width:layout == 1 ? width-20 : layout == 2 ? width/2-20 : width/3-18,
        borderRadius:4
     },
     recentview:{
        marginTop:10,
        borderRadius:4,
        marginLeft:12,
        height:240,
        width:width/2-20,
        marginBottom:8
    },
    recentimage:{
        height:150,
        width: width/2-20 ,
        borderRadius:4
     },
     viewMansory: {
        marginTop: 4,
        marginRight: 0,
        marginBottom: 10,
        marginLeft: 8,
        width: width / 2 - 20,
        borderRadius: 3,
        flexDirection: 'column',
        position: 'relative',
        backgroundColor: '#fcc',
      },
      imageMansory: {
        width: width / 2,
        resizeMode: 'contain',
      },
    typetxt:{
        color:Colors.red,
        fontSize:13,
        marginTop:4
    },
    featurename:{
        color:Colors.attributes.black,
        marginTop:6,
        fontSize:13,
        fontWeight:'bold'
    },
    quality:{
        fontSize:12,
        color:Colors.attributes.black
    },
    wrapPriceRange: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, .75)',
        padding: 5,
        borderTopRightRadius: 5,
      },
      priceRange: {
        fontSize: 11,
        marginLeft: 5,
        color: '#FFF',
      },
      ratingView: {
        flexDirection: 'row',
        position: 'absolute',
        right: 0,
        bottom: 0,
        zIndex: 999,
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .75)',
        padding: 3,
      },
      countText: {
        fontSize: 11,
        marginLeft: 8,
        color: '#666',
      },
      newsIcons: {
        marginLeft: 2,
        paddingTop: 3,
        paddingRight: 3,
        paddingBottom: 8,
        paddingLeft: 3,
      },
      fixHeart: {
        position: 'absolute',
        top: 10,
        right: 5,
        zIndex: 9999,
      },
      footerrowview:{
          flexDirection:'row',
          height:55,
          width:Dimensions.get('window').width,
          position:'absolute',
          bottom:0,
          backgroundColor:Colors.main
      },
      menutxt:{
          color:Colors.bottomblue,
          fontSize:12
      },
      menuview:{
          marginHorizontal:10
      },
      bottomdarkview:{
          flexDirection:'row',
          backgroundColor:Colors.bottomblue,
          justifyContent:'center',
          alignItems:'center',
          width:Dimensions.get('window').width-30,
          height:'100%'
      },
      footertxt:{
          color:Colors.main,
          fontWeight:'bold'
      },
      inputlineview:{
          borderBottomWidth:0.7,
          borderBottomColor:Colors.grey,
          marginHorizontal:20
      },
      inputtxt:{
          marginTop:16, 
          marginLeft:8,
          fontSize:13,
          color:Colors.grey
        }
})