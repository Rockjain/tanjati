import { StyleSheet } from 'react-native';
import Colors from '../../../Config/Colors';

export default StyleSheet.create({
    container:{
        flex:1
    },
    menustyle:{
        height:15,
        width:22,
        margin:15
    },
    tabview:{
        flexDirection:'row',
        marginHorizontal:20,
        marginTop:50,
        justifyContent:'space-around'
    },
    tabtxt:{
        fontSize:32,
        fontWeight:'bold',
        color:Colors.grey
    },
    loginview:{
        marginTop:50,
        paddingHorizontal:30
    },

    txt:{
        marginLeft:5,
        fontWeight:'bold'
    },
    loginbtnviews:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:30
    },
    loginbtn:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.green,
        borderRadius:30,
        paddingVertical:10,
        paddingHorizontal:50
    },
    btntxt:{
        color:Colors.main,
        fontWeight:'bold'
    },
    facbookbtn:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.blue,
        borderRadius:30,
        paddingVertical:10,
        paddingHorizontal:30,

    },
    activeTab: {
        height: 3,
        backgroundColor: Colors.main,
      },
      textTab: {
        fontWeight: '600',
        fontSize: 35,
      },
      tab: {
        paddingBottom: 10,
        borderBottomWidth: 0,
        paddingTop: 0,
        paddingLeft: 0,
        paddingRight: 0,
      },
})