import React from 'react';
import { View,Image,TextInput,TouchableOpacity,SafeAreaView } from 'react-native';
import Styles from './Styles';
import Colors from '../../../Config/Colors';
import Footer from '../../../Components/Footer';
import {
    Container,
     Header,
     Content, 
     Text, 
     Left, 
     Body, 
     Right, 
     Button, 
     Title,
     Tab,
     Tabs,
     Item,
     Input,
     Icon
   } from 'native-base';
   const menu = require('../../../assets/Images/icon-burger.png')
   import ScrollableTabView, {
    ScrollableTabBar,
  } from 'react-native-scrollable-tab-view'
   import SignIn from './SignIn';
   import SignUp from './Signup';
   import Languages from '../../../Config/Languages';

export default class Login extends React.Component{
    constructor(props){
        super(props)
        this.state={
          isLogin:true,
          isSignUp:false
        }
    }

  
    render(){
        const {navigation} = this.props;
        return(
            <SafeAreaView style={Styles.container}>
                      <ScrollableTabView scrollWithoutAnimation={true}
                      style={{ marginTop:50 }}
                       initialPage={0}
                       locked={false}
                       tabBarUnderlineStyle={Styles.activeTab}
                       tabBarActiveTextColor={Colors.tabbarTint}
                       tabBarInactiveTextColor={'#ddd'}
                       tabBarTextStyle={Styles.textTab}
                       renderTabBar={() => (
                        <ScrollableTabBar
                          underlineHeight={0}
                          style={{ borderBottomColor: 'transparent' }}
                          tabsContainerStyle={{ paddingLeft: 0, paddingRight: 0 }}
                          tabStyle={Styles.tab}
                        />
                      )}>
                     
                       <SignIn tabLabel={Languages.login} navigation={navigation} />
                       <SignUp tabLabel={Languages.signup} navigation={navigation}/>
                    </ScrollableTabView>
                 <Footer />
            </SafeAreaView>
        )
    }

}
