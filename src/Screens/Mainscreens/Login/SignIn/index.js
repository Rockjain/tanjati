import React from 'react';
import { View,TouchableOpacity,NetInfo, Alert, SafeAreaView, AsyncStorage } from 'react-native';
import {
     Text, 
     Item,
     Input,
   } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Styles from './Styles';
import Colors from '../../../../Config/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux';
import ToastHelper from '../../../../Config/ToastHelper';
import Loading from '../../../../Components/Loadings';
import firebase from 'react-native-firebase';
import Languages from '../../../../Config/Languages';
let fcmToken = null;

class SignIn extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
          loading: false,
          email: '',
          password: '',
          name: '',
          phonenumber:'',
          userid:''
        }
      }


      doLogin=async(email,password)=>{
          if(email == '' || password == ''){
            ToastHelper.danger('Please fill required fields'); 
          } else {
              const enabled = await firebase.messaging().hasPermission();
              if(enabled){
                   fcmToken = await firebase.messaging().getToken();
                  console.log('fcmtoken '+fcmToken)
              }
            console.log('fcmtoken2 '+fcmToken)
             if (NetInfo.isConnected) {
                this.props.dispatch({type:'Login_Request',payload:{data:{ "username":email.trim(),"password":password.trim()},navigation:this.props.navigation,firebasetoken:fcmToken}})  
             } else {
                 Alert.alert('Net is not connected, Please check your internet connection!')
             }     
          }
          
      }
    render(){
        const {email,password}=this.state;
        if (this.props.isFetching) {
            return(
                <Loading/>
            )
        }
        return(
            <KeyboardAwareScrollView enableOnAndroid={true} scrollEnabled={false}>
            <View style={Styles.loginview}>
            <Text style={Styles.txt}>{Languages.email}</Text>
            <Item>
              <Input placeholder={Languages.enterEmail}  onChangeText={text=>this.setState({email:text.toLowerCase()})}
               placeholderTextColor={Colors.grey} keyboardType='email-address' />
           </Item>
           <Text style={[Styles.txt,{marginTop:20}]}>{Languages.regpass}</Text>
            <Item>
              <Input placeholder={Languages.enterPassword} onChangeText={text=>this.setState({password:text})}
               placeholderTextColor={Colors.grey} secureTextEntry={true} />
           </Item>
           <View style={Styles.loginbtnviews}>
               <TouchableOpacity onPress={()=>this.doLogin(email,password)} style={Styles.loginbtn}>
                   <Text style={Styles.btntxt}>{Languages.login}</Text>
               </TouchableOpacity>
               <TouchableOpacity style={Styles.facbookbtn}>
                   <View style={{ flexDirection:'row' }}>
                   <Icon name="facebook" size={16} color="#FFF" style={{ marginTop:2 }} />
                   <Text style={[Styles.btntxt,{marginLeft:6}]}>{Languages.facebook}</Text>
                   </View>
               </TouchableOpacity>
           </View>
          </View>
          </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
      isFetching:state.Auth.isFetching,
      Response: state.Auth,
      error:state.Auth.errorres
    }
    }
    
export default connect(
      mapStateToProps
    )(SignIn)