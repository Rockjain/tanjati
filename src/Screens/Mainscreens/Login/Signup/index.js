import React from 'react';
import { View,Image,TextInput,TouchableOpacity,NetInfo,Alert,Platform } from 'react-native';
import {
    Container,
     Header,
     Content, 
     Footer, 
     Text, 
     Left, 
     Body, 
     Right, 
     Button, 
     Title,
     Tab,
     Tabs,
     Item,
     Input,
     Icon
   } from 'native-base';
import { connect } from 'react-redux'
import Styles from './Styles';
import Colors from '../../../../Config/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Languages from '../../../../Config/Languages';
import Loading from '../../../../Components/Loadings';
import ToastHelper from '../../../../Config/ToastHelper';
import firebase from 'react-native-firebase';
let fcmToken = null;

class SignUp extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
          loading: false,
          email: '',
          password: '',
          name: '',
          phonenumber:'',
          userid:''
        }
      }

    //   componentWillUnmount() {
    //     this.notificationListener();
    // }
    doSignUp=async()=>{
        const {name,email,password,phonenumber}=this.state
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(name == '' || email == '' || password == '' || phonenumber == ''){
            ToastHelper.danger('Please fill required fields'); 
        } else if(reg.test(email) === false){
            ToastHelper.danger('Email is Not Correct');
        } else {
            let platform = Platform.OS == 'ios'?'ios':'android';
            const enabled = await firebase.messaging().hasPermission();
            if(enabled){
                 fcmToken = await firebase.messaging().getToken();
                console.log('fcmtoken '+fcmToken)
            }
            console.log('fcmtoken2 '+fcmToken)
            if (NetInfo.isConnected) {
                this.props.dispatch({type:'User_Register_Request',payload:{name:name.trim(),email:email.trim(),phone:phonenumber.trim(),password:password.trim(),navigation:this.props.navigation,deviceplatform:platform,firebasetoken:fcmToken}})
            } else {
                Alert.alert('Net is not connected, Please check your internet connection!')
            }
            
        }
        //this.props.navigation.navigate('verification')
    }

    render(){
        if (this.props.isFetching) {
            return(
                <Loading/>
            )
        }
        return(
            <KeyboardAwareScrollView enableOnAndroid={true} scrollEnabled>
            <View style={Styles.loginview}>
            <Text style={Styles.txt}>{Languages.name}</Text>
            <Item>
              <Input placeholder={Languages.name} 
              onChangeText={text=>this.setState({name:text})} placeholderTextColor={Colors.grey} />
           </Item>
            <Text style={[Styles.txt,{marginTop:20}]}>{Languages.email}</Text>
            <Item>
              <Input placeholder={Languages.email} 
              onChangeText={text=>this.setState({email:text})} placeholderTextColor={Colors.grey} keyboardType='email-address' />
           </Item>
           <Text style={[Styles.txt,{marginTop:20}]}>{Languages.regphone}</Text>
            <Item>
              <Input placeholder={Languages.phonenumber} 
              onChangeText={text=>this.setState({phonenumber:text})} placeholderTextColor={Colors.grey} keyboardType='phone-pad' />
           </Item>
           <Text style={[Styles.txt,{marginTop:20}]}>{Languages.regpass}</Text>
            <Item>
              <Input placeholder={Languages.enterPassword} 
              onChangeText={text=>this.setState({password:text})} placeholderTextColor={Colors.grey} secureTextEntry={true} />
           </Item>
               <TouchableOpacity style={Styles.loginbtn} onPress={this.doSignUp}>
                   <Text style={Styles.btntxt}>{Languages.regsign}</Text>
               </TouchableOpacity>
          </View>
          </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
      isFetching:state.Auth.isFetching,
      Response: state.Auth,
    }
    }
    
export default connect(
      mapStateToProps
    )(SignUp)