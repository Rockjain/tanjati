import { StyleSheet } from 'react-native';
import Colors from '../../../../Config/Colors';
export default StyleSheet.create({
    container:{
        flex:1
    },
    menustyle:{
        height:15,
        width:22,
        margin:15
    },
    tabview:{
        flexDirection:'row',
        marginHorizontal:20,
        marginTop:50,
        justifyContent:'space-around'
    },
    tabtxt:{
        fontSize:32,
        fontWeight:'bold',
        color:Colors.grey
    },
    loginview:{
        marginTop:40,
        paddingHorizontal:30
    },

    txt:{
        marginLeft:5,
        fontWeight:'bold',
        fontSize:14,
    },
   
    loginbtn:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.green,
        borderRadius:30,
        paddingVertical:10,
        paddingHorizontal:50,
        alignSelf:'center',
        marginVertical:20
    },
    btntxt:{
        color:Colors.main,
        fontWeight:'bold',
        fontSize:14
    },
    facbookbtn:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.blue,
        borderRadius:30,
        paddingVertical:10,
        paddingHorizontal:30,

    }
})