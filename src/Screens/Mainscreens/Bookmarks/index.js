import React from 'react';
import { View, Text, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, TextInput,BackHandler,SafeAreaView,AsyncStorage } from 'react-native';
import Styles from './Styles';
import {
    Container,
     Header,
     Content, 
     Left, 
     Body, 
     Right, 
     Button, 
     Title,
     Tab,
     Tabs,
     Item,
     Icon, Input
   } from 'native-base';
import { connect } from 'react-redux';
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
const back = require('../../../assets/Images/backlong.png')
const user = require('../../../assets/Images/icons/profileuser.png')
const demoimg = require('../../../assets/Images/category/cate1.jpg');
import Rating from '../../../Components/react-native-star-rating';
const greyback = require('../../../assets/Images/greyback.png');
import Footer from '../../../Components/Footer';
import Loadings from '../../../Components/Loadings';
const exists=[];
let users=null;

 class Bookmark extends React.Component{
        constructor(props){
            super(props)
            this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
            this.state={
                isClear:false,
                myuser:'',
                navdata: props.navigation.getParam('navdata'),
            }
        }

    componentWillMount=async()=>{
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        let userinfo = await AsyncStorage.getItem('User');
        let token =  await AsyncStorage.getItem('MUSERTOKEN');
        console.log('token')
        console.log(token)
       users=JSON.parse(userinfo)
       this.setState({ myuser:users })
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
         this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+users.id}})
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        const {navigation} = this.props;
          navigation.goBack();
          return true;
      }

    renderRecents=({item})=>{
        let status=item.more_options.price_status
        let value = 4
        const priceFrom =item.more_options.list_price != '' && typeof item.more_options.list_price != 'undefined'
        ? item.more_options.list_price
        : ''
        let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
        let listing_Cat_replaced=listing_Cat_string.replace('-','_')
         let listing_Cat= JSON.parse(listing_Cat_replaced)
        //  let imageURL = 'http://'+item.gallery_images[0].substring(7, item.length);   
      return(
          <TouchableOpacity style={{ marginTop:30 }} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'bookmark' })}>
        <View style={Styles.featureview}>
        <ImageBackground
         source={item.better_featured_image?{ uri:'https://tanjati.versiondigitale.net/wp-content/uploads/'+item.better_featured_image.media_details.file}:greyback}
        style={Styles.featureimage}
        imageStyle={{ borderRadius:2 }}
        >
        {/* {this.renderPrice(item.more_options)}
        {this.renderRating(item.more_options)} */}
        </ImageBackground>
        <View style={{ marginLeft:5 }}>
        <Text style={Styles.featurename}>{item && item.title &&item.title.rendered?item.title.rendered:''}</Text>
        <Text style={Styles.quality}>{priceFrom}</Text>
        <Text style={Styles.quality} numberOfLines={2}>{item && item.listing_data && item.listing_data.lp_listingpro_options  &&item.listing_data.lp_listingpro_options.tagline_text?item.listing_data.lp_listingpro_options.tagline_text:''}</Text>
          {this.renderRating(value)}
        </View> 
    </View>
    </TouchableOpacity>
      )
    }

    renderRating=(value)=>{
        // if (item.totalRate && item.totalRate != 'undefined') {
            return (
                <View style={Styles.ratingView}>
                 
                    <Rating
                      disabled={false}
                      emptyStar={'star-o'}
                      fullStar={'star'}
                      halfStar={'star-half-o'}
                      iconSet={'FontAwesome'}
                      maxStars={5}
                      starSize={12}
                      starStyle={{ marginLeft:2 }}
                      halfStarEnabled
                      rating={Number(value)}
                      fullStarColor={Colors.active}
                      emptyStarColor={'#ccc'}
                    />
                 
                </View>
                )  
        //  } else{
        //      return null
        //  }  
          
        }

    renderList = (posts,getwishlist,res) =>{
        const {isClear,myuser} = this.state;
        console.log('posts')
        console.log(posts)
        if(isClear == true){
            return(
                <Text style={Styles.nobookmarktxt}>{Languages.noBookmark}</Text>
            )

        } else if(isClear == false){
            console.log('getwishlist')
            console.log(getwishlist)
            res = []
              if (getwishlist == null || getwishlist == undefined) {
               
               
              } else {
                const myarray = Object.values( getwishlist );
                if(myarray.length != 0){
                    console.log('myarray')
                console.log(myarray)
                for(i=0;i<myarray.length;i++){
                  const rest = posts.filter(function(item){
                       return (item.id == myarray[i])
                     })
                     console.log('rest')
                     console.log(rest)
                     if (rest.length == 0) {
                         
                     } else {
                        res.push(
                            rest[0]
                        )
                     }
                }
                if (res[0] == undefined) {
                    // return(
                    //     <Text style={Styles.nobookmarktxt}>{Languages.noBookmark}</Text>
                    // )
                } else {
                    console.log('res')
                    console.log(res)
                    const finalresult = res.slice(0, myarray.length)
                    console.log('finalresult')
                    console.log(finalresult)
                    return(
                        <FlatList
                        data={finalresult}
                        ItemSeparatorComponent={this.renderSeparator}
                        keyExtractor={item => item.id}
                        renderItem={this.renderRecents}
                        style={{ marginBottom:50 }}
                        ></FlatList>
                       )
                }
                } else if(myarray.length == 0){
                    return(
                        <View style={Styles.noissueview}>
                        <Text style={Styles.nobookmarktxt}>{Languages.noBookmark}</Text>
                        </View>
                    )
                }
                res = []
              }
        }
        
    }

    clearAll = (getwishlist) =>{
      if (getwishlist == null || getwishlist == undefined) {

      } else {
        const myarray = Object.values( getwishlist );
        console.log('myarray')
        console.log(myarray[0])
        for(i=0;i<myarray.length;i++){
            this.props.dispatch({type:'Remove_Wishlist_Request',payload:{url:'/unsetWishlist?user_id='+users.id+'&post_id='+myarray[i]}})
            this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}})
            this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        }
        if (getwishlist.length == 0) {
            this.setState({ isClear:true })
        }
        
      } 
    }

    isClearText = (getwishlist) =>{
        console.log('getwishlist')
        console.log(getwishlist)
        if (getwishlist == null || getwishlist == undefined || getwishlist.length==0) {

        } else {
        if(this.state.isClear == true){

        } else {
            return(
                <TouchableOpacity onPress={()=>this.clearAll(getwishlist)} style={{ position:'absolute', top:15, right:5 }}>
                <Text style={{color:Colors.attributes.black,fontSize:14 }}>{Languages.clear}</Text>
                </TouchableOpacity>
            )
        }
        }
    }
    render(){
        const {posts,getwishlist,isFetching} = this.props;
        const {myuser,navdata} = this.state;
        if (navdata != 'navdata') {
        if (isFetching) {
            return(
                <Loadings />
            )
        }
    }
        return(
            <SafeAreaView style={Styles.container}>
                 <View style={Styles.headview}>
                 <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                     <Image
                     source={back}
                     style={Styles.menustyle}
                     ></Image>
                        </TouchableOpacity>
                 </View>
                 <Text style={{ fontWeight:'bold', marginLeft:10,color:Colors.attributes.black,fontSize:24,marginTop:10 }}>{Languages.textBookMark}</Text>
                 {this.isClearText(getwishlist)}
                 {this.renderList(posts,getwishlist,exists)}
                 <Footer />
                 </SafeAreaView>
          )
                 }
  }

  const mapStateToProps=(state)=>{
    return{
       posts:state.Post.posts ,
       user: state.Auth.UserInfo,
       getwishlist:state.Post.getWishlist,
       isFetching:state.Post.isFetching,
    }
}
export default connect(mapStateToProps)(Bookmark)
