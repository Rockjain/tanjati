import React from 'react';
import { View, Dimensions ,TouchableOpacity,SafeAreaView,Image} from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import styles from './Styles';
import Listview from './Listview';
import Mapview from './Mapview';
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
import Animated from 'react-native-reanimated';
import Footer from '../../../Components/Footer';
import {
  Container,
   Header,
   Left,
   Body,
   Icon,
   Toast
 } from 'native-base';
 const backlong=require('../../../assets/Images/backlong.png')


export default class Map extends React.Component{
  state = {
    index: 0,
    routes: [
      {
        key: 'first',
        title: Languages.Map,
      },
      {
        key: 'second',
        title: Languages.List,
      },
    ],
  }
      MapView = () =>{
        return(
          <Mapview navigation ={this.props.navigation}/>
        )
      }

      SearchView = () =>{
        return(
          <Listview navigation ={this.props.navigation}/>
        )
      }
     
    render(){
      const {navigation} = this.props;
        return(
          <SafeAreaView style={{ flex:1 }}>
            <View style={{padding:10,backgroundColor:'#0579D4'}}>
            <TouchableOpacity onPress={()=>navigation.navigate('home')} >
            <Image source={backlong} style={styles.headerImage}></Image>
            </TouchableOpacity>
            </View>
          <TabView
          navigationState={this.state}
          style={styles.container}
          renderScene={SceneMap({ first: this.MapView, second: this.SearchView })}
          onIndexChange={(index) => this.setState({ index })}
          initialLayout={{
            width: Dimensions.get('window').width,
          }}
        />
        <Footer />
        </SafeAreaView>
        )
    }
}