import { StyleSheet, Platform ,Dimensions} from 'react-native';
import Colors from '../../../Config/Colors';
const { width } = Dimensions.get('window')
export default StyleSheet.create({
      container: {
        // flex: 1,
        backgroundColor: '#0579D4',
        elevation: 0,
        borderWidth: 0,
      },
      header:{
    height:45,
    backgroundColor:Colors.main,
    marginHorizontal: 4
},
headerText:{
  fontSize:18,
  color:Colors.attributes.black,
  fontWeight:'bold'
},
headerImage:{
    width:28,
    height:12,
    tintColor:Colors.main
},
      tabBar: {
        flexDirection: 'row',
        paddingTop: 10,
        backgroundColor:'#fff',
      },
      tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
      },
    
      label: {
        color: '#333',
        margin: 0,
        ...Platform.select({
          ios: {
            marginTop: -15,
          },
          android: {},
        }),
      },
    
     
})