import React from 'react';
import { View, Text,Image,
    Picker,
    I18nManager,
    Alert,TouchableOpacity,AsyncStorage} from 'react-native';
import styles from './Styles';
import { connect } from 'react-redux';
const demoimg = require('../../../assets/Images/category/cate1.jpg')
import { Container,Content,Header,Body,Left,Icon, Col } from 'native-base';
import Colors from '../../../Config/Colors';
const iconburger=require('../../../assets/Images/icon-burger.png')
import Languages from '../../../Config/Languages';
import RNrestart from 'react-native-restart';
import Footer from '../../../Components/Footer';

class Setting extends React.Component{
  constructor(props) {
    super(props);
    this.state={
        lang:'en'
    }
    }
    componentWillMount=async()=>{
      const lang=await AsyncStorage.getItem('lang')  
      this.setState({lang:lang})
  }

    changeLocale = (locale) => {
      console.log(locale)
        Alert.alert(Languages.Confirm, Languages.SwitchRtlConfirm, [
          {
            text: Languages.CANCEL,
            onPress: () => undefined,
          },
          {
            text: Languages.OK,
            onPress: () => {
              Languages.setLanguage(locale)
              AsyncStorage.setItem('lang',locale)
              I18nManager.forceRTL(locale=='ar')
              RNrestart.Restart()
            },
          },
        ])
      }
    
    render(){
        const {navigation} = this.props;
        const {lang} = this.state;
        return(
            <Container>
            <Header style={styles.header}>
            <Left>
            </Left>
            <Body>
            <Text style={styles.headerText}>{Languages.setting}</Text>
            </Body>
            </Header>
            <Text style={styles.text}>{Languages.switchRTL}:</Text>
            <View style={styles.container}>
        <Picker
          style={styles.picker}
          selectedValue={lang}
          onValueChange={this.changeLocale}>
          <Picker.Item label={'🇺🇸 English'} value={'en'} />
          <Picker.Item label={'🇷🇺 Arabic'} value={'ar'} />
          <Picker.Item label={'🇫🇷 French'} value={'fr'} />
        </Picker>
        <View
          style={
            (I18nManager.isRTL && { flexDirection: 'row-reverse' },
            styles.centered)
          }>
          <Text style={styles.plainBanner}>
            {Languages.currentLanguage + ': '}
          </Text>
          <Text style={styles.plainBanner}>
            {lang == 'en' ? 'English' : 'Arabic'}
          </Text>
        </View>
        </View>
        <Footer />
            </Container>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
       lang:state.Auth.lang 
    }
}
export default connect(mapStateToProps)(Setting)