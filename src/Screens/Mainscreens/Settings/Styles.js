import { StyleSheet } from 'react-native';
import Colors from '../../../Config/Colors';

export default StyleSheet.create({
    findlistview:{
        borderRadius:5,
        margin:12,
        height:130,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    categoryMainText:{
        color:Colors.main,
        fontWeight:'bold',
        fontSize:22
    },
    categorySubText:{
        color:Colors.main,
        fontSize:14
    },
    burger:{
        width:22,
        height:15,
    },
    headerText:{
        fontSize:18,
        color:Colors.attributes.black,
        fontWeight:'bold'
    },
    header:{
        height:45,
        backgroundColor:Colors.main,
    },
    imageCategory:{
        margin:12,
        height:120,
        alignItems: 'center',
        justifyContent: 'center'
    },
    centered: {
        flexDirection: 'row',
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'center',
      },
      picker: { backgroundColor: '#EEE', width: '50%' },
      container: {
        paddingVertical: 10,
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
      },
      plainBanner: { fontSize: 14, marginTop: 15 },
      boxSetting: {
        padding: 10,
        margin: 10,
      },
      text: {
        fontSize: 18,
        color: '#000',
        margin:10
      }
})