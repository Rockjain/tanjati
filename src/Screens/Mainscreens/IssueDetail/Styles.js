import { StyleSheet,Platform,Dimensions } from 'react-native';
import Colors from '../../../Config/Colors';


export default StyleSheet.create({
    container:{
        backgroundColor:Colors.main,
        paddingHorizontal:10,
        flex:1
    },
    headview:{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:Colors.main,
        //paddingHorizontal:10,
        marginTop:40,
        ...Platform.select({
            ios: {
                paddingHorizontal:10,
            },
            android: {
                paddingHorizontal:0,
            },
          }),
    },
      menustyle:{
          height:15,
          width:34,
      },
      posttxt:{
        fontSize:24,
        marginTop:10,
        color:Colors.attributes.black
    },
    rowview:{
        flexDirection:'row',
        marginTop:20,
        ...Platform.select({
            ios: {
                paddingHorizontal:10,
            },
            android: {
                paddingHorizontal:0,
            },
          }),
    },
    headtxt:{
        color:Colors.attributes.black,
        fontWeight:'bold'
      },
      maintxt:{
        color:Colors.attributes.black,
        
      },
      lineview:{
        borderBottomWidth:8,
        borderBottomColor:Colors.lightGrey,
        marginTop:10
    }, 
    descriptiontxt:{
        color:Colors.attributes.black,
        marginTop:10,
        fontSize:18,
        fontWeight:'bold',
        ...Platform.select({
            ios: {
                marginHorizontal:10,
            },
            android: {
                marginHorizontal:0,
            },
          }),
    },
    descriptionmaintxt:{
        color:Colors.attributes.black,
        marginTop:5,
        ...Platform.select({
            ios: {
                marginHorizontal:10,
            },
            android: {
                marginHorizontal:0,
            },
          }),
    },
    imgstyle:{
        marginTop:20, 
        height:Dimensions.get('window').height/4, 
        width:Dimensions.get('window').width-200, 
        justifyContent:'center', 
        alignItems:'center',
        ...Platform.select({
            ios: {
                paddingHorizontal:10,
            },
            android: {
                paddingHorizontal:0,
            },
          }),
    }
})