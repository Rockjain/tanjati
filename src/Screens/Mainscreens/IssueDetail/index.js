import React from 'react';
import { View, Image, Switch, TouchableOpacity,ScrollView, SafeAreaView,BackHandler,Dimensions,Platform} from 'react-native';
import Styles from './Styles';
import { 
     Text, 
   } from 'native-base';
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
const arrow = require('../../../assets/Images/backlong.png');
import { connect } from 'react-redux';
import Loading from '../../../Components/Loadings';
import Footer from '../../../Components/Footer';
let attachdata = [];

 class IssueDetail extends React.Component{
    constructor(props){
        super(props)
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            issue: props.navigation.getParam('getissue'),
        }
    }

    componentWillMount = () =>{
        const {issue} = this.state;
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        console.log('issue')
        console.log(issue)
        attachdata = issue.attachments;
        console.log('attachdata')
        console.log(attachdata)
        if (attachdata == undefined) {
           
        } else {
         this.props.dispatch({type:'Get_Issuefile_Request', payload:{url:'/api/rest/issues/'+issue.id+'/files/'+issue.attachments[0].id,issueid:issue.id}})
        }
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
      const {navigation} = this.props;
        navigation.navigate('mylisting');
        return true;
    }

    renderStatus = (status) =>{
      if (status == 'open') {
          return(
             <View style={Styles.rowview}>
             <Text style={Styles.headtxt}>{Languages.status}:</Text>
             <Text style={[Styles.maintxt,{color:'#009900'}]}> {status}</Text>
          </View> 
          )
      }
      else if (status == 'close') {
         return(
            <View style={Styles.rowview}>
            <Text style={Styles.headtxt}>{Languages.status}:</Text>
            <Text style={[Styles.maintxt,{color:Colors.grey}]}> {status}</Text>
         </View> 
         )
     }
     else if (status == 'pending') {
         return(
            <View style={Styles.rowview}>
            <Text style={Styles.headtxt}>{Languages.status}:</Text>
            <Text style={[Styles.maintxt,{color:Colors.yellow}]}> {status}</Text>
         </View> 
         )
     }
   }

   renderView = () =>{
      const {issueinfo,filess} = this.props;
      const {issue} = this.state;
      if (attachdata == undefined) {
         return(
            <View>
           <View style={Styles.rowview}>
           <Text style={[Styles.headtxt,{ fontSize:18 }]}>{Languages.title}: </Text>
           <Text style={[Styles.maintxt,{ fontSize:18 }]}>{issue.summary}</Text>
        </View>
        <View style={Styles.rowview}>
           <Text style={Styles.headtxt}>{Languages.category}: </Text>
           <Text style={Styles.maintxt}>{issue.project.name}</Text>
        </View>
        {this.renderStatus(issue.resolution.name)}
        <View  style={Styles.imgstyle}>
             <Text style={{fontSize:12}}>{Languages.noimg}</Text>
        </View>
           <Text style={Styles.descriptiontxt}>{Languages.descriptionMap}</Text>
           <Text style={Styles.descriptionmaintxt}>{issue.description}</Text>
        </View>
         )
      }
      else{
         if(issueinfo.length == 0 && filess.length == 0){

         } else {
            return(
               <View>
              <View style={Styles.rowview}>
              <Text style={[Styles.headtxt,{ fontSize:18 }]}>{Languages.title}: </Text>
              <Text style={[Styles.maintxt,{ fontSize:18 }]}>{issueinfo[0].summary}</Text>
           </View>
           <View style={Styles.rowview}>
              <Text style={Styles.headtxt}>{Languages.category}: </Text>
              <Text style={Styles.maintxt}>{issueinfo[0].project.name}</Text>
           </View>
           {this.renderStatus(issueinfo[0].resolution.name)}
           <Image
           source={{uri:'data:image/png;base64,'+filess[0].content}}
           style={{ marginTop:20, height:Dimensions.get('window').height/4, width:Dimensions.get('window').width-200,marginHorizontal:Platform.OS == 'ios'?10:0 }}
           ></Image>
              <Text style={Styles.descriptiontxt}>{Languages.descriptionMap}</Text>
              <Text style={Styles.descriptionmaintxt}>{issueinfo[0].description}</Text>
           </View>
            )
         }
      }
   }

    render(){
        const {issue} = this.state;
        const {isFetching} = this.props;
        if (isFetching) {
           return(
              <Loading />
           )
        }
        return(
            <SafeAreaView style={Styles.container}>
                 <ScrollView>
                 <View style={Styles.headview}>
                 <TouchableOpacity onPress={()=>this.props.navigation.navigate('mylisting')}>
                     <Image
                     source={arrow}
                     style={Styles.menustyle}
                     ></Image>
                     </TouchableOpacity>
                 </View>
                {this.renderView()}
                 </ScrollView>
                 <Footer />
            </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
   return{
      filess:state.Issue.file,
      issueinfo:state.Issue.issuedata,
      isFetching:state.Issue.isFetching,
   }
}
export default connect(mapStateToProps)(IssueDetail)