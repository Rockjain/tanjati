import React from 'react';
import { View, Image, Switch, TouchableOpacity,ScrollView, SafeAreaView,RefreshControl,Alert,AsyncStorage} from 'react-native';
import Styles from './Styles';
import {
    Container,
     Header,
     Content, 
     Text, 
     Left, 
     Body, 
     Right, 
     Button, 
     Title,
     Tab,
     Tabs,
     Item,
     Icon, Input
   } from 'native-base';
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
const menu = require('../../../assets/Images/icon-burger.png')
const user_icon = require('../../../assets/Images/icons/profileuser.png')
const person = require('../../../assets/Images/icon-person.png')
const logout = require('../../../assets/Images/icons/profilelogout.png')
import { connect } from 'react-redux';
import Loadings from '../../../Components/Loadings';
import Footer from '../../../Components/Footer';
import ToastHelper from '../../../Config/ToastHelper';
const exists=[];
const finalresult=[];

class Profile extends React.Component{
     constructor(props){
         super(props)
         this.state={
             user:'',
             local:'ar',
             myarray:[],
             isLoad:false,
             refreshing: false,
         }
     }

    componentDidMount = async() =>{
        const {Users}=this.props
       let userstring = await AsyncStorage.getItem('User');  
        let user=JSON.parse(userstring)
        console.log(Users)
        if (user) {
        this.props.dispatch({type:'Fetch_User_Request',payload:{userid:user.id}}) 
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
         this.props.dispatch({type:'Get_AllIssue_Request', payload:{url:'/api/rest/issues?filter_id=reported'}})
        this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}})
        }
        // this.setState({
        //     user:JSON.parse(user),
        // })
    }

   

    checkUser = () =>{
        const {user,local} = this.state;
         const {Users}=this.props
         console.log('Users')
         console.log(Users)
        if(Users == null){

        } else {
            return(
                <View>
                <View locale={local} style={[Styles.optionview,{marginTop:12}]}>
                     <Text locale={local} style={Styles.txt}>{Languages.userProfileName}</Text>
                     <TouchableOpacity style={Styles.itemrow}>
                        <Text locale={local}>{Users.display_name}</Text>
                     </TouchableOpacity>
                 </View>
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.userProfileEmail}</Text>
                     <TouchableOpacity style={Styles.itemrow}>
                        <Text>{Users.user_email}</Text>
                     </TouchableOpacity>
                 </View>
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.Phone}</Text>
                     <TouchableOpacity style={Styles.itemrow}>
                        <Text>{Users.phone}</Text>
                     </TouchableOpacity>
                 </View>
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.userProfileAddress}</Text>
                     <TouchableOpacity style={Styles.itemrow}>
                        <Text></Text>
                     </TouchableOpacity>
                 </View>
                 </View>
            )
        }
    }
    getCheck = (res) =>{
        const {Users,navigation,issue,getwishlist,posts} = this.props;
        res = []
        // finalresult = []
         if(Users == null){
 
         } else {
                if(getwishlist == null || getwishlist == undefined){

                } else{
                const myarray = Object.values( getwishlist ); 
                console.log('myarray')
                console.log(myarray)
                for(i=0;i<myarray.length;i++){
                    const rest = posts.filter(function(item){
                         return (item.id == myarray[i])
                       })
                       console.log('rest')
                       console.log(rest)
                       if (rest.length == 0) {
                           
                       } else {
                          res.push(
                              rest[0]
                          )
                       }
                  }
                  if (res[0] == undefined) {
                } else {
                    console.log('res')
                    console.log(res)
                    //  finalresult = res.slice(0, myarray.length)
                    // console.log('finalresult')
                    // console.log(finalresult)
                }
             return(
                 <View>
                 <View style={Styles.optionview}>
                      <Text style={Styles.txt}>{Languages.myListings}</Text>
                      <TouchableOpacity style={Styles.itemrow} onPress={()=>navigation.navigate('mylisting')}>
                      <Text>{issue?issue.length:'0'} {Languages.items}</Text>
                        <Icon name='ios-arrow-forward' style={Styles.iconstyle}  />
                      </TouchableOpacity>
                  </View>
                  <View style={Styles.optionview}>
                      <Text style={Styles.txt}>{Languages.userProfileWishlist}</Text>
                      <TouchableOpacity style={Styles.itemrow} onPress={()=>navigation.navigate('bookmark')}>
                      <Text>{res.length!=0?res.length:'0'} {Languages.items}</Text>
                        <Icon name='ios-arrow-forward' style={Styles.iconstyle}  />
                      </TouchableOpacity>
                  </View>
                  <View style={Styles.optionview}>
                      <Text style={Styles.txt}>{Languages.chatList}</Text>
                      <View style={Styles.itemrow}>
                        <Icon name='ios-arrow-forward' style={Styles.iconstyle}  />
                     </View>
                  </View>
                  </View>
             )
         }
        }
    }

    doLogout = () =>{
        const {navigation,Users} = this.props;
        this.setState({ isLoad:true })
         AsyncStorage.clear();
         this.props.dispatch({type:'logout_success'})
          setTimeout(() => {
            ToastHelper.success(Languages.logoutMessage)  
            this.setState({ isLoad:false }) 
            navigation.navigate('main')
          }, 1);
       
    }

    goAlert = () =>{
        Alert.alert('Alert',
        'Do You want to Logout?',
        [{
            text:'Cancel',
            onPress:()=>console.log('Cancel Pressed'),
        },{
            text:'OK',
            onPress:()=>this.doLogout(),
        }],
        { cancelable: true });
    }

    getLogout = () =>{
        const {Users} = this.props;
        if(Users == null){
            return(
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('login')}>
                <Image
                source={user_icon}
                style={Styles.userstyle}
                ></Image>
                </TouchableOpacity>
            )
        } else {
            return(
                <TouchableOpacity onPress={()=>this.goAlert()}>
                <Image
                source={logout}
                style={Styles.loouticon}
                ></Image>
                </TouchableOpacity>
            )
        }
    }
    _renderVerifyMsg=()=>{
        const {Users} = this.props;
        if (Users) {
          if (Users.status==''|| Users.status==0) {
            return(
              <View style={{flexDirection:'row',marginTop:10}}>
             <Text style={{color:Colors.attributes.red}}>{Languages.verifymobile}</Text>
             <TouchableOpacity style={Styles.btnVerify}
             onPress={()=>this.props.dispatch({type:'Send_Code_Request',payload:{phone:Users.phone,isVerified:false,navigation:this.props.navigation}})}>
             <Text style={{color:Colors.main}}>{Languages.verify}</Text>
             </TouchableOpacity>
             </View>
              )
          }
        }
      }
      _onRefresh = async() => {
        const {Users}=this.props
        this.setState({refreshing: true});
        let userstring = await AsyncStorage.getItem('User');  
         let user=JSON.parse(userstring)
         console.log(Users)
         if (user) {
         this.props.dispatch({type:'Fetch_User_Request',payload:{userid:user.id}}) 
         this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
         this.props.dispatch({type:'Get_AllIssue_Request', payload:{url:'/api/rest/issues?filter_id=reported'}})
         this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}})
         }
         this.setState({refreshing: false});
      }
    render(){
        const {navigation,isFetching,wishFetching,issueFetching} = this.props;
        const {isLoad} = this.state;
        if (isLoad == true) {
            return(
                <Loadings />
            )
        }
        if (wishFetching) {
            return(
                <Loadings />
            )
        }
        return(
            <SafeAreaView style={Styles.container}>
            <ScrollView
             refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
                 <View style={Styles.headview}>
                   <View></View>
                   {this.getLogout()}
                 </View>
                 <View style={Styles.userview}>
                    <Image
                    source={person}
                    style={Styles.personstyle}
                    ></Image>
                    {this._renderVerifyMsg()}
                 </View>
                 {this.checkUser()}
                 <View style={{marginTop:12}}>
                 {this.getCheck(exists)}
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.userProfilePushNotif}</Text>
                    <Switch />
                 </View>
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.userProfilePrivacy}</Text>
                     <View style={Styles.itemrow}>
                        <Icon name='ios-arrow-forward' style={Styles.iconstyle}  />
                     </View>
                 </View>
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.userProfileContact}</Text>
                     <TouchableOpacity style={Styles.itemrow} onPress={()=>navigation.navigate('contact')}>
                        <Icon name='ios-arrow-forward' style={Styles.iconstyle}  />
                     </TouchableOpacity>
                 </View>
                 <View style={Styles.optionview}>
                     <Text style={Styles.txt}>{Languages.userProfileAbout}</Text>
                     <TouchableOpacity style={Styles.itemrow} onPress={()=>navigation.navigate('contact')}>
                        <Icon name='ios-arrow-forward' style={Styles.iconstyle}  />
                     </TouchableOpacity>
                 </View>
                 </View>
                 </ScrollView>
                 <Footer />
            </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
      isFetching:state.Auth.isFetching,
      vcode: state.Auth.VCode,
      register:state.Auth.Register,
      issue:state.Issue.issues,
      Users:state.Auth.UserInfo,
      getwishlist:state.Post.getWishlist,
      wishFetching:state.Post.isFetching,
      issueFetching:state.Issue.isFetching,
      posts:state.Post.posts ,
    }
    }
    
  export default connect(
      mapStateToProps
    )(Profile)