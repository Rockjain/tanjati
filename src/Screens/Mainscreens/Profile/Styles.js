import { StyleSheet } from 'react-native';
import Colors from '../../../Config/Colors';

export default StyleSheet.create({
    container:{
        backgroundColor:Colors.lightGrey,
        flex:1,
    },
    headview:{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:Colors.main,
        paddingHorizontal:10,
    },
    menustyle:{
        height:15,
        width:22,
        marginTop:15,
        marginLeft:10
    },
    userstyle:{
        height:20,
        width:18,
        marginTop:15,
        marginRight:10
    },
    loouticon:{
        height:18,
        width:18,
        marginTop:15,
        marginRight:10,
    },
    userview:{
        height:160,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:Colors.main,
    },
    personstyle:{
        height:80,
        width:80
    },
    optionview:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal:12,
        backgroundColor:Colors.main,
        marginTop:1,
        paddingVertical:15
    },
    txt:{
      color:Colors.grey
    },
    itemrow:{
        flexDirection:'row'
    },
    iconstyle:{
        color:Colors.lightgrey,
        marginLeft:10
    },
    btnVerify:{
        backgroundColor:Colors.attributes.red,
        alignItems:'center',
        borderRadius:4,
        paddingHorizontal:6,
        paddingVertical:2,
        marginLeft:20
      }

})