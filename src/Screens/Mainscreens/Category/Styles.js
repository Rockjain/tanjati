import { StyleSheet } from 'react-native';
import Colors from '../../../Config/Colors';

export default StyleSheet.create({
    container:{
        flex:1
    },
    findlistview:{
        borderRadius:5,
        margin:12,
        height:120,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    categoryMainText:{
        color:Colors.main,
        fontWeight:'bold',
        fontSize:22
    },
    categorySubText:{
        color:Colors.main,
        fontSize:14
    },
    burger:{
        width:22,
        height:15,
    },
    headerText:{
        fontSize:18,
        color:Colors.attributes.black,
        fontWeight:'bold'
    },
    header:{
        height:45,
        backgroundColor:Colors.main,
        marginHorizontal: 4
    },
    imageCategory:{
        margin:12,
        height:120,
        alignItems: 'center',
        justifyContent: 'center'
    },
})