import React from 'react';
import { View, Text,FlatList,Image,ImageBackground,TouchableOpacity,AsyncStorage,StatusBar,RefreshControl,ScrollView} from 'react-native';
import Styles from './Styles';
import { connect } from 'react-redux';
const demoimg = require('../../../assets/Images/category/cate1.jpg')
import { Container,Content,Header,Body,Left,Icon, Col } from 'native-base';
import Colors from '../../../Config/Colors';
const iconburger=require('../../../assets/Images/icon-burger.png')
import Footer from '../../../Components/Footer';
import Languages from '../../../Config/Languages';
import Loadings from '../../../Components/Loadings';

class Category extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            refreshing: false,
        }
    }
    componentWillMount=()=>{
        this.props.dispatch({type:'Fetch_Category_Request',payload:{url:'/listing-category'}})
    }
    gotoSubCat=(item)=>{
        const {navigation} = this.props
        console.log(item)
        AsyncStorage.setItem('CatID',item.id.toString())
        AsyncStorage.setItem('CatName',item.name)
      navigation.navigate('cattab')
    }
    _renderItem=({ item }) => {
        let catimage=item.term_image.replace('https:','')
        console.log(catimage)
        if (item.term_image=='') {
            return(
               
                 <TouchableOpacity onPress={()=>this.gotoSubCat(item)} style={Styles.findlistview}>
                <Text style={Styles.categoryMainText}>{item.name}</Text>
                <Text style={Styles.categorySubText}>{item.count} {Languages.placevisit}</Text>
                 </TouchableOpacity>
            )
        } else {
            return(
                <TouchableOpacity onPress={()=>this.gotoSubCat(item)}>
                <ImageBackground imageStyle={{borderRadius:5}} source={{uri:'http:'+catimage}} style={Styles.imageCategory}>
                <Text style={Styles.categoryMainText}>{item.name}</Text>
                <Text style={Styles.categorySubText}>{item.count} {Languages.placevisit}</Text>
                </ImageBackground>
                </TouchableOpacity>
            )  
        }
       
     }

     _onRefresh = async() => {
        this.props.dispatch({type:'Fetch_Category_Request',payload:{url:'/listing-category'}})
      }
    render(){
        const {navigation,isFetching} = this.props;
        if (isFetching) {
            return(
                <Loadings />
            )
        }
        return(
            <Container>
            <ScrollView
             refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
            <Header style={Styles.header}>
            <Left>
            </Left>
            <Body>
            <Text style={Styles.headerText}>{Languages.category}</Text>
            </Body>
            </Header>
            <FlatList
            data = {this.props.categories}
            keyExtractor={item => item.id}
            renderItem={this._renderItem}
            style={{marginBottom:50}}
            ></FlatList>
            </ScrollView>
            <Footer />
            <StatusBar hidden />
            </Container>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
       categories:state.Category.Categories,
       isFetching: state.Category.isFetching,
    }
}
export default connect(mapStateToProps)(Category)