import { StyleSheet, Dimensions, Platform } from 'react-native';
import Colors from '../../../Config/Colors';
const {width,height}=Dimensions.get('window')
export default StyleSheet.create({
    container:{
        paddingHorizontal:10,
        flex:1
    },
    headview:{
      flexDirection:'row',
      justifyContent:'space-between',
      backgroundColor:Colors.main,
      //paddingHorizontal:10,
      marginTop:40
  },
    menustyle:{
        height:15,
        width:34,
    },
    posttxt:{
      fontSize:24,
      marginTop:10,
      color:Colors.attributes.black,
      fontWeight:'bold'
  }, 
  listview:{
    //marginTop:10,
    paddingVertical:10

  },
  rowview:{
    flexDirection:'row',
    marginTop:5
  },
  headtxt:{
    color:Colors.attributes.black,
  },
  maintxt:{
    color:Colors.attributes.black,
    fontWeight:'bold'
  },
  lineview:{
    borderBottomWidth:8,
    borderBottomColor:Colors.lightGrey,
    marginTop:10
},
noissueview:{
  flex:1,
  alignItems:'center',
  justifyContent:'center',
  marginBottom:50
},
noissuetxt:{
  marginTop:15,
  color:Colors.grey,
  marginLeft:10,
  fontSize:18,
},
tabheadview:{
  marginHorizontal:10,
  flexDirection:'row',
  justifyContent:'space-between'
}

})