import React from 'react';
import { View, Text, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, TextInput,BackHandler, SafeAreaView, AsyncStorage } from 'react-native';
import { Container } from 'native-base';
import Styles from './Styles';
import { connect } from 'react-redux';
import Languages from '../../../Config/Languages';
import Colors from '../../../Config/Colors';
const arrow = require('../../../assets/Images/backlong.png');
import Loading from '../../../Components/Loadings';
import Footer from '../../../Components/Footer';


 class MyListing extends React.Component{
     constructor(props){
         super(props)
         this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

     }
     componentWillMount = async() =>{
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        const Mantis_Token_User= await AsyncStorage.getItem('MUSERTOKEN');
        const tokenjson = JSON.parse(Mantis_Token_User)
        console.log(tokenjson)
        const mentisid = tokenjson.mentis_id
        this.props.dispatch({type:'Get_AllIssue_Request', payload:{url:'/api/rest/issues?filter_id=reported'}})
     }

     componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        const {navigation} = this.props;
          navigation.navigate('profile');
          return true;
      }

      renderStatus = (status) =>{
         if (status == 'open') {
             return(
                <Text style={[Styles.maintxt,{color:'#009900'}]}> {status}</Text>
             )
         }
         else if (status == 'close') {
            return(
               <Text style={[Styles.maintxt,{color:Colors.grey}]}> {status}</Text>
            )
        }
        else if (status == 'pending') {
            return(
               <Text style={[Styles.maintxt,{color:Colors.yellow}]}> {status}</Text>
            )
        }
      }

      mylist = (issue) =>{
          const {navigation} = this.props;
        let colors = ['#FFF',Colors.lightGrey]
          return(
              <FlatList
              data={issue}
              keyExtractor={item => item.id}
              renderItem={({ item,index }) => (
                  <TouchableOpacity style={[Styles.listview,{backgroundColor:colors[index%colors.length]}]} onPress={()=>navigation.navigate('issuedetail',{ getissue:item })}>
                         <View style={Styles.tabheadview}>
                         <Text style={Styles.maintxt}> {item.id}</Text>
                         <Text style={Styles.maintxt}> {item.summary}</Text>
                      {this.renderStatus(item.resolution.name)}
                      </View>
                  </TouchableOpacity>
              )}
              ></FlatList>
          )
      }

     renderList = () =>{
         const {issue,navigation} = this.props;
         console.log(issue.length)
         if (issue.length == 0) {
             return(
                 <View style={Styles.noissueview}>
                     <Text style={Styles.noissuetxt}>{Languages.noListing}</Text>
                 </View>
             )
         } else if(issue.length != 0) {
             
            return(
                 <View style={{flex:1}}>
                 <View style={[Styles.tabheadview,{backgroundColor:Colors.lightGrey1,paddingHorizontal:5,paddingVertical:10,marginHorizontal:0,marginVertical:12}]}>
                     <Text style={Styles.maintxt}>{Languages.ticketid}</Text>
                     <Text style={Styles.maintxt}>{Languages.title}</Text>
                     <Text style={Styles.maintxt}>{Languages.status}</Text>
                     </View>
                    {this.mylist(issue)}
                 </View>
            )
         }
           
     }
     render(){
         if (this.props.isFetching) {
             return(
                 <Loading />
             )
         }
         return(
          <SafeAreaView style={Styles.container}>
                <View style={Styles.headview}>
                 <TouchableOpacity onPress={()=>this.props.navigation.navigate('profile')}>
                     <Image
                     source={arrow}
                     style={Styles.menustyle}
                     ></Image>
                     </TouchableOpacity>
                 </View>
                 <Text style={Styles.posttxt}>{Languages.myListings}</Text>
                 {this.renderList()}
                 <Footer />
          </SafeAreaView>
         )
     }
}
 

const mapStateToProps=(state)=>{
    return{
       categories:state.Category.Categories,
       issue:state.Issue.issues,
       isFetching:state.Issue.isFetching
    }
}
export default connect(mapStateToProps)(MyListing)