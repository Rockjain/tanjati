import React from 'react';
import { View, Text,PermissionsAndroid,TouchableOpacity,Image,Dimensions,Platform,
  ActivityIndicator,BackHandler,SafeAreaView,Alert,AsyncStorage} from 'react-native';
import { Toast, Icon } from 'native-base';
import styles from './Styles';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import { connect } from 'react-redux'
import Colors from '../../../Config/Colors';
import Loading from '../../../Components/Loadings';
const back = require('../../../assets/Images/backlong.png');
const location = require('../../../assets/Images/issuelocation.png')
import Footer from '../../../Components/Footer';
const ANCHOR = { x: 0.5, y: 0.5 }
const ANCHOR_DETAIL = { x: 0.5, y: 1 }
const ANCHOR_ANDROID = { x: 0, y: -1.5 }
const CENTER = { x: 0, y: 0 }
const CENTER_DETAIL = { x: 5, y: 3 }
const CENTER_ANDROID = { x: 0, y: -3 }
import Languages from '../../../Config/Languages';
import Permissions from 'react-native-permissions';


class Issuemap extends React.Component{
    constructor(props){
     super(props)
     this.state={
       region:'',
       summary:props.navigation.getParam('nextheading'),
       project:props.navigation.getParam('nextproject'),
       docname:props.navigation.getParam('documentname'),
       document:props.navigation.getParam('doc'),
       lat:null,
       long:null,
       position:null,
       isClick:false
     }
     this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount=()=>{
      const {lat,long} = this.state;
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
      if (lat == null && long == null) {
      this._getLocationAsync()
      }
    }
    _getLocationAsync = async () => {
       
      try {
        if (Platform.OS=='ios') {
          const granted=await Permissions.check('location')
            if (granted=='authorized') {
              navigator.geolocation.getCurrentPosition(
                (position) => {
                 console.log('position')
                 console.log(position)
                  const myPosition = position.coords
        
                  this.setState({ myPosition,position:myPosition })
                  const current = {
                    address_lat: myPosition.latitude,
                    address_long: myPosition.longitude,
                  }
                this.props.dispatch({type:'SET_REGION_MAP_REQUEST',region:current,index:0})
              
                },
                (error) =>  console.log(error),
                { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
              );
            } else {
              const granted=await Permissions.request('location')
              if (granted=='authorized') {
                navigator.geolocation.getCurrentPosition(
                  (position) => {
                   console.log('position')
                   console.log(position)
                    const myPosition = position.coords
          
                    this.setState({ myPosition,position:myPosition })
                    const current = {
                      address_lat: myPosition.latitude,
                      address_long: myPosition.longitude,
                    }
                  this.props.dispatch({type:'SET_REGION_MAP_REQUEST',region:current,index:0})
                
                  },
                  (error) =>  console.log(error),
                  { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
                );
              }
            }
        } else {
          const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        if (granted != PermissionsAndroid.RESULTS.GRANTED) {
         Toast.show({type:'danger',text:'Permission denied',position:'top',duration:2000})
        }
    
        navigator.geolocation.getCurrentPosition(
            (position) => {
             console.log('position')
             console.log(position)
              const myPosition = position.coords
    
              this.setState({ myPosition,position:myPosition })
              const current = {
                address_lat: myPosition.latitude,
                address_long: myPosition.longitude,
              }
            this.props.dispatch({type:'SET_REGION_MAP_REQUEST',region:current,index:0})
          
            },
            (error) =>  console.log(error),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
          );
        }
        } catch (err) {
          console.warn(err);
        }
          // not use for ListingPro due to Database could not be fetch
          // fetchNearestLocations(true, myPosition.latitude, myPosition.longitude)
       
      }


    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

  handleBackButtonClick() {
      const {navigation} = this.props;
      const {summary,project,docname,document} = this.state;
        navigation.navigate('issue',{mapsummary:summary,mapproject:project,mapdocname:docname,mapdocument:document});
        return true;
    }

    renderMarkers = (pos) => {
      const {isFetchingmap,initialRegion} = this.props;
      // const {lat,long,position} = this.state;
      console.log('position')
      console.log(pos)
        const { width, height } = Dimensions.get('window')
        const ASPECT_RATIO = width / height
        const LATITUDE = 0
        const LONGITUDE = 0
        const LATITUDE_DELTA = 1.832
        const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO 
        if (initialRegion) {
          let coordinates = {
            latitude: initialRegion.latitude,
            longitude: initialRegion.longitude,
            LATITUDE_DELTA,
            LONGITUDE_DELTA,
          }
              
                // warn(['markerItemMap', index]);
                return (
                  <MapView.Marker
                    key={'marker-' + 0}
                    ref={`marker${0}`}
                    anchor={ANCHOR}
                    centerOffset={CENTER}
                    coordinate={coordinates}
                    pinColor={Colors.map.defaultPinColor}
                    style={[styles.marker]}>
                 
                  </MapView.Marker>
                  //  {this.renderCalloutMaker(item, index)}
                )
        } else {
          
        }
  }

  componentWillReceiveProps = (nextProps) =>{
    const {isClick} = this.state;
      console.log('nextProps')
      console.log(nextProps)
      if (nextProps) {
         if (isClick) {
          this.props.dispatch({type:'Get_AllIssue_Request', payload:{url:'/api/rest/issues?filter_id=reported'}})
           this.setState({isClick:false})
         }      
        }
  }

  goToSave = async() =>{
    const {summary,project,description,docname,document,lat,long} = this.state;
    const {initialRegion} = this.props;
    console.log(document)
    console.log('location')
    console.log(initialRegion.longitude)
    console.log(initialRegion.latitude)
    let userstring = await AsyncStorage.getItem('User');  
        let user=JSON.parse(userstring)
    
        https://mantis.versiondigitale.net/api/rest/projects/
        this.props.dispatch({type:'Add_Issues_Request', payload:{url:'/api/rest/issues',
        data:{ "summary": summary,
        "description": summary,
        "longitude":initialRegion.longitude,
        "latitude":initialRegion.latitude,
        "category": {
          "name": "General"
        },
        "project": {
          "name": project
        }},navigation:this.props.navigation, name:docname, content:document,userid:user.id}})  
        this.setState({ loading:this.props.isFetching,isClick:true })
}
   
    render(){
      const {lat,long,position,summary,docname,document,project} = this.state;
      const {isFetching,initialRegion,isFetchingmap,navigation} = this.props;
      if (isFetchingmap) {
        return(
          <Loading />
        )
    }
      console.log('initialRegion')
      console.log(initialRegion)
      const { width, height } = Dimensions.get('window')
      const ASPECT_RATIO = width / height
        const LATITUDE = 0
        const LONGITUDE = 0
        const LATITUDE_DELTA = 1.832
        const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
      console.log('latitudedelta '+LATITUDE_DELTA)
      console.log('longitudedelta '+LONGITUDE_DELTA)
    let coordinate = {
        latitude: parseFloat(lat),
        longitude: parseFloat(long),
        latitudeDelta:LATITUDE_DELTA,
        longitudeDelta:LONGITUDE_DELTA,
      } 
      if (isFetching) {
          return(
            <Loading />
          )
      }
        return(
          <SafeAreaView style={styles.container}>
            <View style={styles.headview}>
                 <TouchableOpacity onPress={()=> navigation.navigate('issue',{mapsummary:summary,mapproject:project,mapdocname:docname,mapdocument:document})}>
                     <Image
                     source={back}
                     style={styles.menustyle}
                     ></Image>
                     </TouchableOpacity>
                     {/* <TouchableOpacity onPress={()=>this.goToSave()}>
                       <Text style={styles.txt}>{Languages.save}</Text>
                     </TouchableOpacity> */}
                 </View>
                 <Text style={styles.publishtxt}>{Languages.creationclaim}</Text>
                 <View style={styles.locationtagrow}>
                       <Image
                       source={location}
                       ></Image>
                       <View>
                        <Text style={styles.localtxt}>{Languages.localisation}</Text>
                        <View style={{borderBottomWidth:0.7,borderBottomColor:'grey',width:Dimensions.get('window').width-50}}></View> 
                       </View>
                 </View>
            <View style={{ height:270, width:Dimensions.get('window').width, marginTop:20 }}>
            <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            ref={(ref) => (this.map = ref)}
            region={initialRegion}
            loadingEnabled
            showsUserLocation
            minZoomLevel={14}
            userLocationAnnotationTitle={"You're Here"}
            loadingIndicatorColor={Colors.map.loading}>
            {this.renderMarkers(initialRegion)}
          </MapView>
          </View>
          <TouchableOpacity style={styles.btnview} onPress={()=>this.goToSave()}>
                        <Text style={styles.btntxt}> {Languages.save} </Text>
                 </TouchableOpacity>
          <Footer />
          </SafeAreaView>
        )
    }
}
const mapStateToProps=(state)=>{
  return{
    isFetching:state.Issue.isFetching,
     isFetchingmap:state.Map.isFetching,
      initialRegion:  state.Map.region,
     latitudeDelta:  state.Map.latitudeDelta,
     longitudeDelta:  state.Map.longitudeDelta,
     isSearching:  state.Map.isSearching,
     indexActive:  state.Map.index,
     posts:state.Post.posts ,
}
    }
    
  export default connect(
      mapStateToProps
    )(Issuemap)
      