import { StyleSheet } from 'react-native';
import Colors from '../../../Config/Colors';

export default StyleSheet.create({
    container:{
        flex:1
    },
    burger:{
        width:22,
        height:15,
    },
})