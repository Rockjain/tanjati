import React from 'react';
import { View,Text,WebView, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import Styles from './Styles';
const iconburger=require('../../../assets/Images/icon-burger.png')

export default class Contact extends React.Component{
    render(){
        const {navigation} = this.props;
        return(
            <SafeAreaView style={Styles.container}>
                <TouchableOpacity onPress={()=>navigation.toggleDrawer()} style={{marginTop:10,marginLeft:5}}>
            <Image source={iconburger} style={Styles.burger}></Image>
            </TouchableOpacity>
                <WebView
                style={{marginTop:20}}
                source={{uri:'https://inspireui.com/about/'}}
                startInLoadingState={true}
                ></WebView>
            </SafeAreaView>
        )
    }
}

