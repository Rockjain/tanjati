/** @format */

import React, {
    StyleSheet,
    Platform,
    Dimensions,
    PixelRatio,
  } from 'react-native'
  
  const { width, height, scale } = Dimensions.get('window'),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh)
  import Colors from '../.../../../../Config/Colors';
  export default StyleSheet.create({
    container:{
      padding:20
    },
    wrapVButton: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    
    btnverify: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgb(72,194,172)',
      flex: 1,
      padding: 10,
      margin: 10,
      borderRadius: 25,
    },
   
    btnText: {
      color: 'white',
      fontWeight: 'bold',
      fontSize: 14,
    },
    headerText:{
      fontSize:14,
        color:Colors.attributes.black,
        fontWeight:'bold',
        marginRight:10
  },
  header:{
      height:45,
      backgroundColor:Colors.main,
      marginHorizontal: 4
  },
  })
  