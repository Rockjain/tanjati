import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput,ScrollView,ActivityIndicator,NetInfo,Alert,Platform,AsyncStorage} from 'react-native'
import css from './styles'
import { connect } from 'react-redux'
import CodeInput from 'react-native-code-input';
import {Container,Content,Header,Left,Right,Body} from 'native-base';
import Languages from '../../../Config/Languages';
import Colors from '../../../Config/Colors';
import firebase from 'react-native-firebase';
let fcmToken = null;

class Verification extends Component{
    constructor(props) {
        super(props)
        this.state = {
          loading: false,
          email:props.navigation.getParam('email',''),
          password:props.navigation.getParam('password',''),
          name: '',
          phonenumber:props.navigation.getParam('phone',''),
          isvarifing: props.navigation.getParam('isVerified',true),
          bordercolor: Colors.tabbarTint,
          isFourty: false,
          userid:'',
          user:'',
          isSent:false,
          isLater:false,
          isNow:false,
          isClick:false
        }
      }
      componentWillMount = async() =>{
        const {email,password,isvarifing}=this.state;
        let user = await AsyncStorage.getItem('User');
        if (!isvarifing) {
          this.setState({
              user:JSON.parse(user),
              isFourty: false,
          }) 
          setInterval(() => {
            this.setState({
              isFourty: true,
            })
          }, 40000);
        }
        
    }
    btnVerifyResend=()=>{
      this.props.dispatch({type:'Send_Code_Request',payload:{phone:this.state.phonenumber}})
      this.refs.codeInputRef2.clear();
      this.setState({
        bordercolor:Colors.tabbarTint,
        isFourty: false,
        isSent:true,
      })
      setInterval(() => {
        this.setState({
          isFourty: true,
        })
      }, 40000);
    }
    

      btnVerify=()=>{
        const {isClick} = this.state;
        this.props.dispatch({type:'Send_Code_Request',payload:{phone:this.state.phonenumber}})
        if(isClick){
          this.refs.codeInputRef2.clear();
          this.setState({
            bordercolor:Colors.tabbarTint,
            isClick:false
          })
        }
        this.setState({
          isFourty: false,
          isSent:true
        })
        setInterval(() => {
          this.setState({
            isFourty: true,
          })
        }, 40000);
      }
    
      // show resend button
      renderResend=()=>{
        const {isFourty}=this.state;
        if (isFourty) {
          return(
            <TouchableOpacity onPress={this.btnVerifyResend}>
            <Text style={{alignSelf:'flex-end',marginTop:30,marginRight:15}}>{Languages.resend}</Text>
            </TouchableOpacity>
          )
        }else{
          return null;
        }
      }
      doLater = async () =>{
        const {email,password,isvarifing,user}=this.state;
        const enabled = await firebase.messaging().hasPermission();
        if(enabled){
             fcmToken = await firebase.messaging().getToken();
            console.log('fcmtoken '+fcmToken)
        }
      console.log('fcmtoken2 '+fcmToken)
        this.setState({isLater:true})
        if (isvarifing) {        
            this.props.dispatch({type:'Login_Request',
            payload:{data:{ "username":email,"password":password},navigation:this.props.navigation,firebasetoken:fcmToken}}) 

        } else {
          this.props.navigation.navigate('profile')
        }
       
  }
      _onFinishCheckingCode= async(code)=>{
      const {register,vcode}=this.props
      const {email,password,isvarifing,user}=this.state;
      const enabled = await firebase.messaging().hasPermission();
      if(enabled){
           fcmToken = await firebase.messaging().getToken();
          console.log('fcmtoken '+fcmToken)
      }
    console.log('fcmtoken2 '+fcmToken)
          if (code==vcode) {
            if (isvarifing) {
              this.props.dispatch({
                type:'Update_Status_Login_Request',
                payload:{userid:register.user_id,data:{ "username":email,"password":password},navigation:this.props.navigation,firebasetoken:fcmToken}
              })
             
            } else {
              this.props.dispatch({
                type:'Update_Status_Request',
                payload:{userid:user.id,navigation:this.props.navigation}  
              })
            }
          } else {
            this.setState({bordercolor: Colors.active,isClick:true})
          }
      }
      renderSendCode=()=>{
        const {email,password,isvarifing,isLater}=this.state;
        if (isvarifing) {
          // this.setState({ isNow:true })
          
          return(
            <TouchableOpacity style={css.btnverify} onPress={this.btnVerify}>
            {this.props.isFetching && !isLater ? (
              <ActivityIndicator color={'#FFF'} size={Platform.OS?'small':20} />
            ) : (
              <Text style={css.btnText}> {Languages.sendcode} </Text>
            )}
          </TouchableOpacity>
          )
        }
      }
      renderCode = () =>{
        const {email,password,isvarifing,isSent,bordercolor}=this.state;
        if (!isvarifing) {
          return(
           <CodeInput
           ref="codeInputRef2"
           secureTextEntry
           activeColor={Colors.tabbarTint}
           inactiveColor='rgba(49, 180, 4, 1.3)'
           autoFocus={false}
           inputPosition='center'
           codeLength={6}
           space={5}
           size={40}
           onFulfill={(code) => this._onFinishCheckingCode(code)}
           containerStyle={{ marginTop: 30}}
           codeInputStyle={{ borderWidth: 1.5,borderColor: bordercolor,height:56,borderRadius:4}}
           />
          )
     }else if (isvarifing && isSent) {
             return(
              <CodeInput
              ref="codeInputRef2"
              secureTextEntry
              activeColor={Colors.tabbarTint}
              inactiveColor='rgba(49, 180, 4, 1.3)'
              autoFocus={false}
              inputPosition='center'
              codeLength={6}
              space={5}
              size={40}
              onFulfill={(code) => this._onFinishCheckingCode(code)}
              containerStyle={{ marginTop: 30}}
              codeInputStyle={{ borderWidth: 1.5,borderColor: bordercolor,height:56,borderRadius:4}}
              />
             )
        }
      }
    render(){
      const {bordercolor,isLater}=this.state
        return(
            <Container>
            <Header style={css.header}>
            <Left></Left>
            <Body style={{marginRight:27}}>
            <Text style={css.headerText}>{Languages.verifymobile}</Text>
            </Body>
            {/* <Right /> */}
            </Header>
          
            <Text style={{fontSize:16,marginTop:25,marginLeft:20}}>{Languages.verifyphone} {this.state.phonenumber}</Text>
            <Text style={{fontSize:16,marginLeft:20}}>{Languages.accessmodule}</Text>
            <View style={{height:70}}>
           {this.renderCode()}
            </View>
            {this.renderResend()}
    
            <View style={css.wrapVButton}>
            {this.renderSendCode()}
            <TouchableOpacity
              style={css.btnverify}
              onPress={this.doLater}>
               {this.props.isFetching && isLater ? (
              <ActivityIndicator color={'#FFF'} size={Platform.OS?'small':20} />
            ) : (
              <Text style={css.btnText}> {Languages.dolater} </Text>
            )}
            </TouchableOpacity>
          </View>
          
            </Container>
        )
    }
}  

const mapStateToProps=(state)=>{
  return{
    isFetching:state.Auth.isFetching,
    vcode: state.Auth.VCode,
    register:state.Auth.Register
  }
  }
  
export default connect(
    mapStateToProps
  )(Verification)
    