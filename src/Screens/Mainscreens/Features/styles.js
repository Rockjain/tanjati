import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../../Config/Colors';
const {width,height}=Dimensions.get('window')
const hw=width/2
export default StyleSheet.create({
    container:{
        flex:1
    },
    featureview:{
        marginTop:16,
        borderRadius:4,
        marginLeft:12,
        height:240,
         width:hw-20,
    },
    featureimage:{
        height:150,
        width:hw-20,
        borderRadius:4 
    },
    typetxt:{
        color:Colors.red
    },
    featurename:{
        color:Colors.attributes.black,
        marginTop:7,
        fontSize:12.5,
        fontWeight:'bold'
    },
    quality:{
        fontSize:12,
        color:Colors.attributes.black
    },
    header:{
        height:45,
        backgroundColor:Colors.main,
        marginHorizontal: 4
    },
    headerText:{
        fontSize:18,
        color:Colors.attributes.black,
        fontWeight:'bold'
    },
    headerImage:{
        width:28,
        height:12,
    },
    wrapPriceRange: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, .75)',
        padding: 5,
        borderTopRightRadius: 5,
      },
      priceRange: {
        fontSize: 11,
        marginLeft: 5,
        color: '#FFF',
      },
      ratingView: {
        flexDirection: 'row',
        position: 'absolute',
        right: 0,
        bottom: 0,
        zIndex: 999,
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .75)',
        padding: 3,
      },
      countText: {
        fontSize: 11,
        marginLeft: 8,
        color: '#666',
      },
      fixHeart: {
        position: 'absolute',
        top: 10,
        right: 5,
        zIndex: 9999,
      },
})