import React from 'react';
import { View, Image, FlatList, TouchableOpacity,ImageBackground,AsyncStorage } from 'react-native';
import Styles from './styles';
import { connect } from 'react-redux';
import {
    Container,
     Text,
     Header,
     Left,
     Body,
     Icon
   } from 'native-base';
import Colors from '../../../Config/Colors';
import Footer from '../../../Components/Footer';
const backlong=require('../../../assets/Images/backlong.png')
const activedoller = require('../../../assets/Images/icons/icon-dollar-active.png')
const inactivedoller = require('../../../assets/Images/icons/icon-dollar-inactive.png')
import Rating from '../../../Components/react-native-star-rating';
import IconMater from 'react-native-vector-icons/MaterialCommunityIcons'
import Languages from '../../../Config/Languages';
const greyback = require('../../../assets/Images/greyback.png')
let user = null;

 class Features extends React.Component{
    constructor(props) {
        super(props);
        this.state = { 
           
         }
    }

    componentWillMount=async()=>{
        let userinfo = await AsyncStorage.getItem('User');
       user=JSON.parse(userinfo)
       this.setState({myuser:JSON.parse(userinfo)})
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        if(user){
            this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}}) 
          }
    }

    checkExist = (post) => {
        const {myuser} = this.state;
        if (myuser) {
          const {getwishlist} = this.props;
          console.log(getwishlist)
          if (getwishlist == null || getwishlist == undefined) {
            
          } else{
            const myarray = Object.values( getwishlist );
            console.log(myarray)
           const isExists = myarray.find((item) => item == post.id)
           return isExists
          }
           
        } else {
          
        }
      
      }
    heartClick = async(post) =>{
      const {getwishlist,navigation} = this.props;
      const {myuser} = this.state;
        if(myuser){
          console.log(user)
          //  const myarray = Object.values( getwishlist );
          //  console.log(myarray)
          // console.log('getwishlist')
          // console.log(getwishlist)
          // const isExists = myarray.find((item) => item == post.id)
          // console.log('post.id')
          // console.log(post.id)
            //  if(isExists == post.id){
              if(this.checkExist(post)){
              this.props.dispatch({type:'Remove_Wishlist_Request',payload:{url:'/unsetWishlist?user_id='+myuser.id+'&post_id='+post.id}})
            } else{
              this.props.dispatch({type:'Add_Wishlist_Request',payload:{url:'/setWishlist?user_id='+myuser.id+'&post_id='+post.id}})
            }
       let userinfo = await AsyncStorage.getItem('User');
       user=JSON.parse(userinfo)
       this.setState({myuser:JSON.parse(userinfo)})
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        if(user){
          this.props.dispatch({type:'Get_Wishlist_Request',payload:{url:'/getWishlist?user_id='+user.id}}) 
        }
            
        } else {
            navigation.navigate('Login')
        }   
    }

    renderHeart = (post) =>{
      const {isLoad} = this.state;
        return(
          <TouchableOpacity style={[Styles.fixHeart, { top: 1, right: 1 }]}>
         <IconMater.Button
         style={Styles.newsIcons}
         onPress={()=>this.heartClick(post)}
          name={!this.checkExist(post) ? 'heart-outline' : 'heart'}
         size={22}
         color={
           !this.checkExist(post)
             ? '#FFF'
             : Colors.mainColorTheme
         }
        key={this.state.uniqueValue}
         backgroundColor={'transparent'}
       />
       </TouchableOpacity>
      )  
    }
    renderPrice=(item)=>{
        let status=item.price_status
        let value = 1
        const priceFrom =item.list_price != '' && typeof item.list_price != 'undefined'
        ? item.list_price
        : ''
        const priceTo =
        item.list_price_to != '' && typeof item.list_price_to != 'undefined'
        ? item.list_price_to
        : ''
        const priceRange = priceFrom != '' && priceTo != '' ? priceFrom + '-' + priceTo : ''
     
        if (typeof status !== 'undefined' && status!= 'notsay' && priceRange != '') {
          if (status == 'inexpensive') value = 1
          if (status == 'moderate') value = 2
          if (status == 'pricey') value = 3
          if (status == 'ultra_high_endif') value = 4
          return(
            <View style={Styles.wrapPriceRange}>
            <Rating
            disabled
            fullStar={activedoller}
            emptyStar={inactivedoller}
            maxStars={4}
            starSize={10}
            starStyle={{ marginLeft: -2 }}
            rating={Number(value)}
            fullStarColor='#D5D8DE'
            emptyStarColor={'#ccc'}
          />
           <Text style={Styles.priceRange}>
                  {priceRange ? priceRange : ''}
                </Text>
          </View> 
          )
        }
    }
    renderRating=(item)=>{
    // if (item.totalRate && item.totalRate != 'undefined') {
        return (
            <View style={Styles.ratingView}>
             
                <Rating
                  disabled={false}
                  emptyStar={'star-o'}
                  fullStar={'star'}
                  halfStar={'star-half-o'}
                  iconSet={'FontAwesome'}
                  maxStars={1}
                  starSize={10}
                  starStyle={{ marginLeft:2 }}
                  halfStarEnabled
                  rating={Number(5)}
                  fullStarColor={Colors.active}
                  emptyStarColor={'#ccc'}
                />
                <Text style={Styles.countText}>
                {item.totalRate ? item.totalRate : 5}
              </Text>
             
            </View>
            )  
    //  } else{
    //      return null
    //  }  
      
    }
    renderFeatures=({item})=>{
        let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
        let listing_Cat_replaced=listing_Cat_string.replace('-','_')
         let listing_Cat= JSON.parse(listing_Cat_replaced)
            
      return(
        <TouchableOpacity style={Styles.featureview} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'features' })}>
        <ImageBackground
         imageStyle={{borderRadius:4}}
        source={item.better_featured_image == null?greyback:{uri:'https://tanjati.versiondigitale.net/wp-content/uploads/'+item.better_featured_image.media_details.file}}
        style={Styles.featureimage}
        >
         {this.renderHeart(item)}
        {this.renderPrice(item.more_options)}
        {this.renderRating(item.more_options)}
        </ImageBackground>
        <Text style={Styles.typetxt}>{listing_Cat&& listing_Cat.listing_category && listing_Cat.listing_category[0].name?listing_Cat.listing_category[0].name:''}</Text>
        <Text style={Styles.featurename} numberOfLines={1}>{item.title.rendered}</Text>
        <Text style={Styles.quality} numberOfLines={2}>{item.listing_data.lp_listingpro_options.tagline_text}</Text>
    </TouchableOpacity>
      )
    }
    render(){
       
        const {posts,navigation}=this.props
        return(
            <Container>
           <Header style={Styles.header}>
           <Left>
            <TouchableOpacity onPress={()=>navigation.navigate('home')}>
            <Image source={backlong} style={Styles.headerImage}></Image>
            </TouchableOpacity>
            </Left>
               <Body><Text style={Styles.headerText}>{Languages.featurename}</Text></Body>
           </Header>
            <FlatList
            data={posts}
            keyExtractor={item => item.id}
            numColumns={2}
            renderItem={this.renderFeatures}
            style={{marginBottom:50}}
            ></FlatList>
           <Footer />
            </Container>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
       categories:state.Category.Categories,
       posts:state.Post.posts,
       user: state.Auth.UserInfo,
       addwishlist:state.Post.addWishlist,
       getwishlist:state.Post.getWishlist, 
    }
}
export default connect(mapStateToProps)(Features)