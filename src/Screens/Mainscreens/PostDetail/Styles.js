import { StyleSheet, Dimensions, Platform } from 'react-native';
import Colors from '../../../Config/Colors';

const { width, height, scale } = Dimensions.get('window'),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh)


export default StyleSheet.create({
    container:{
        backgroundColor:Colors.main,
        flex:1
    },
    footerview:{
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
        borderTopWidth:1,
        borderTopColor:Colors.lightgrey,
        flexDirection:'row',
        justifyContent:'space-around',
        height:50,
        backgroundColor:Colors.main
    },
    callicon:{
        height:20,
        width:20,
        marginTop:14
    },
    hearticon:{
        height:20,
        width:23.4,
        marginTop:14
    },
    shareicon:{
        height:22,
        width:20,
        marginTop:14
    },
    closeText: {
        color: 'white',
        textAlign: 'right',
        padding: 30,
      },
      dot: {
        backgroundColor: 'rgba(255, 244, 235, 0.5)',
        width: 10,
        height: 2,
        borderRadius: 2,
        marginLeft: 4,
        marginRight: 4,
      },
    
      dotActive: {
        backgroundColor: 'rgba(255, 244, 235, 0.9)',
        width: 20,
        height: 2,
        borderRadius: 2,
        marginLeft: 4,
        marginRight: 4,
      },
    
      image: {
        width: width - 45,
        height: 0.4 * height - 30,
        resizeMode: 'cover',
        borderRadius: 12
      },
      imageView: {
        width: width - 45,
        height: 0.4 * height - 30,
        marginLeft: 15,
        marginBottom: 15,
        shadowColor: '#000',
        shadowOpacity: 0.2,
        shadowRadius: 6,
        shadowOffset: { width: 0, height: 4 },
        borderRadius: 12,
        elevation: 5, 
      },
      backarrow:{
          position:'absolute',
          top:10,
          left:10
      },
      nameview:{
          paddingHorizontal:15,
          marginTop:20
      },
      nametxt:{
          color:Colors.attributes.black,
          fontWeight:'bold',
          fontSize:24
      },
      qualitytxt:{
        color:Colors.attributes.black,
        marginTop:5,
        fontSize:14
      },
      wrapPriceRange: {
        flexDirection: 'row',
        marginTop:10
      },
      priceRange: {
        fontSize: 14,
        marginLeft: 5,
        color: Colors.attributes.black,

      },
      ratingView: {
        flexDirection: 'row',
        marginTop:15,
      },
      countText: {
        fontSize: 14,
        color: '#666',
      },
      lineview:{
          borderBottomWidth:8,
          borderBottomColor:Colors.lightGrey,
          marginTop:10
      },
      descview:{
          marginTop:20,
          paddingHorizontal:15,
          marginBottom:45
      },
      descnameview:{
          flexDirection:'row'
      },
      descimg:{
          height:20,
          width:20
      },
      desctxt:{
          fontSize:20,
          marginLeft:8,
          marginTop:-4
      },
      descrestxt:{
          fontSize:14,
          marginTop:15
      },
      inforowview:{
          flexDirection:'row',
          marginTop:12
      },
      infotitle:{
          fontSize:12,
          fontWeight:'bold',
          color:Colors.attributes.black
      },
     infores:{
         fontSize:12,
         color:Colors.attributes.black,
         marginLeft:60
     },
     smallline:{
         borderBottomWidth:1.4,
         borderBottomColor:Colors.lightGrey,
        marginTop:5
        },
        bussview:{
            marginTop:20,
            paddingHorizontal:15,
            marginBottom:45
        }, 
        hourview:{
            flexDirection:'row',
            marginTop:15
        },
        hourtitleview:{
            flexDirection:'row'
        },
        opentxt:{
            fontSize:12,
            fontWeight:'bold',
            marginLeft:6
        },
        timeview:{
            marginLeft:60
        },
        timetxt:{
            fontSize:12,
            color:Colors.attributes.black,
            marginTop:2
        },
        featureview:{
            marginTop:20,
            paddingHorizontal:15,
            marginBottom:25
        },
        featureres:{
            flexDirection:'row'
        },
        featuretxt:{
            color:Colors.attributes.black,
            marginLeft:5
        },
        categoryview:{
            flexDirection:'row',
            marginTop:15
        },
        faqview:{
            marginTop:15,
        },

        faqrowview:{
            flexDirection:'row',
            justifyContent:'space-between'
        },
        catname:{
            fontSize:12,
            marginLeft:6
        },
        btnBack: {
            position: 'absolute',
            top: 10,
            left: 10,
            zIndex: 9999,
            shadowColor: 'rgba(15, 15, 15, 0.7)',
            shadowOpacity: 0.3,
            shadowRadius: 5,
            shadowOffset: { width: 0, height: 6 },
            elevation: 5,
          },
          mapview:{
              flexDirection:'row',
              justifyContent:'space-between'
          },
          getdirectiontxt:{
              fontSize:12,
              fontWeight:'bold',
              marginRight:15
          },
          mapshow:{
              width:Dimensions.get('window').width,
              height:250,
              marginTop:15,
              marginLeft:-10
          },
          map: {
            ...StyleSheet.absoluteFillObject,
          },
          linearGradientMap: {
            top: 0,
            left: 0,
            width: width,
            height: Platform.OS == 'ios' ? height / 3 + 15 : height / 2 + 20,
            position: 'absolute',
            zIndex: 999,
          },
          seeMore: {
            position: 'absolute',
            top: 0,
            left: 15,
            zIndex: 999,
            backgroundColor: 'transparent',
            width: width,
          },
          rowHead: {
            flexDirection: 'row',
            width: width - 30,
            alignItems: 'center',
            marginTop: 10,
          },
          rowHeadLeft: {
            flex: 1,
            alignItems: 'flex-start',
          },
          rowHeadRight: {
            flex: 1,
            flexDirection: 'row',
            position: 'absolute',
            left: 0,
            top: -40,
            alignItems: 'flex-end',
            backgroundColor: 'rgba(255, 255, 255, .9)',
            paddingHorizontal: 20,
            paddingVertical: 10,
          },
          textDesMore: {
            color: 'rgb(69,69,83)',
            fontSize: 24,
          },
          iconDirections: {
            width: 20,
            height: 18,
            marginRight: 10,
          },
          viewMapFull: {
            color: '#000',
            fontSize: 12,
          
          },
          marker: {
            alignItems: 'center',
            justifyContent: 'center',
          },
          markerImg: {
            // "zIndex": 9
          },
          markerImgActive: {
            resizeMode: 'contain',
            height: 20,
            width: 20,
            zIndex: 9999,
          },
          iconFull: {
            alignItems: 'center',
            justifyContent: 'center',
          },
          circle: {
            width: 15,
            height: 15,
            borderRadius: 40,
            backgroundColor: 'rgb(27, 229, 141)',
            alignItems: 'center',
            justifyContent: 'center',
          },
          circleChild: {
            width: 7,
            height: 7,
            borderRadius: 10,
            backgroundColor: '#FFF',
          },
          arrowDown: {
            width: 0,
            height: 0,
            borderLeftWidth: 10,
            borderLeftColor: 'transparent',
            borderStyle: 'solid',
            borderRightWidth: 10,
            borderRightColor: 'transparent',
            borderTopWidth: 10,
            borderTopColor: 'rgba(255,255,255,.8)',
            position: 'absolute',
            bottom: -10,
            right: 45,
          },
          titleBox: {
            position: 'absolute',
            backgroundColor: '#FFF',
            zIndex: 9999,
            top: 25,
            left: -30,
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingLeft: 10,
            borderRadius: 7,
            shadowColor: '#000',
            shadowOpacity: 0.4,
            shadowRadius: 8,
            shadowOffset: { width: 0, height: 12 },
            elevation: 10,
          },
          title: {
            fontSize: 15,
            color: '#333',
            marginTop: 3,
            marginRight: 7,
            letterSpacing: 1.5,
            lineHeight: 14,
            textAlign: 'left',
            backgroundColor: 'transparent',
          },
          textMore: {
            fontSize: 11,
            textAlign: 'right',
            color: Colors.toolbarTint,
            marginTop: 12,
          },
          wrapMarker: {
            bottom: 13,
            left: 10,
            zIndex: 9999,
            backgroundColor: 'rgba(255, 255, 255, .8)',
            paddingTop: 5,
            paddingRight: 5,
            paddingBottom: 5,
            paddingLeft: 5,
            borderRadius: 7,
            shadowColor: '#000',
            shadowOpacity: 0.4,
            shadowRadius: 8,
            shadowOffset: { width: 0, height: 12 },
            elevation: 10,
          },
          imgLocatePost: {
            width: 80,
            height: 80,
            borderRadius: 4,
          },
          foodMain: {
            bottom: 15,
            left: -10,
            zIndex: 9999,
            backgroundColor: 'rgba(255, 255, 255, .8)',
            paddingTop: 5,
            paddingRight: 5,
            paddingBottom: 5,
            paddingLeft: 5,
            borderRadius: 7,
            shadowColor: '#000',
            shadowOpacity: 0.4,
            shadowRadius: 8,
            shadowOffset: { width: 0, height: 12 },
            elevation: 10,
          },
          imgLocate: {
            resizeMode: 'contain',
            width: 120,
            borderRadius: 4,
          },
          youreHere: {
            fontSize: 11,
            alignSelf: 'center',
          },
          slideInnerContainer: {
            width: width / 3 + 30,
            height: Platform.OS == 'ios' ? width / 3 : width / 3 + 10,
            borderRadius: 5,
            marginBottom: 5,
            zIndex: 9999,
          },
          imageContainer: {
            height: width / 3,
            shadowColor: '#FFF',
            shadowOpacity: 0.3,
            shadowRadius: 8,
            shadowOffset: { width: 0, height: 6 },
            elevation: 10,
            borderRadius: 5,
            zIndex: 9999,
          },
          image: {
            resizeMode: 'cover',
            flex: 1,
            borderRadius: 5,
            zIndex: 9999,
          },
          wrapText: {
            position: 'absolute',
            left: 0,
            top: 0,
            zIndex: 9999,
            backgroundColor: 'rgba(255, 255, 255, 1)',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            elevation: 10,
            shadowColor: '#FFF',
            shadowOpacity: 0.3,
            shadowRadius: 8,
            padding: 4,
            shadowOffset: { width: 0, height: -12 },
          },
          //head Address
          row: {
            flexDirection: 'row',
            paddingTop: 4,
            paddingBottom: 4,
            paddingLeft: 4,
            alignItems: 'center',
            justifyContent: 'flex-start',
          },
          label: {
            width: 80,
            paddingLeft: 12,
            paddingTop: Platform.OS == 'android' ? 4 : 4,
            fontSize: 10,
            color: '#212121',
            lineHeight: 18,
          },
          text: {
            color: '#555555',
            fontSize: 10,
            paddingTop: Platform.OS == 'android' ? 4 : 5,
            marginLeft: 3,
            lineHeight: 14,
            alignSelf: 'flex-start',
          },
          imageIcon: {
            resizeMode: 'contain',
            width: 14,
            height: 14,
            zIndex: 999,
          },
          containerDetail: {
            marginTop: 10,
          },
          mapDetail: {
            height: height / 3,
            width: width,
            backgroundColor: 'red',
          },
          containerPick: {
            flex: 1,
          },
          mapPick: {
            height: height * 0.7,
            width: width,
            backgroundColor: '#FFF',
          },
        
          searchBar: {
            backgroundColor: 'transparent',
            zIndex: 9999,
            // top: Device.isIphoneX ? 40 : 20,
            top: 0,
            right: 0,
            left: 0,
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute',
            elevation: 5,
          },
          section: {
            marginTop: 10,
            paddingVertical: 20,
        
            backgroundColor: '#FFF',
            width: width,
            shadowColor: '#000',
            shadowOpacity: 0.05,
           
            shadowRadius: 2,
            shadowOffset: { width: 0, height: 1 },
            borderRadius: 3,
          },
          sectionMap: {
            paddingBottom: 0,
          },
        
          // content
          content: {
            width: width - 10,
            paddingHorizontal: 22,
            paddingVertical: 10,
          },
        
          iconRow: {
            resizeMode: 'contain',
            width: 20,
            height: 20,
            marginRight: 7,
            marginTop: 3,
            opacity: 0.5,
          },
        
          // for section type is data
          row: {
            flexDirection: 'row',
            marginHorizontal: 20,
            marginVertical: 8,
            borderBottomWidth: 0.5,
            borderColor: '#F0F0F0',
            paddingVertical: 3,
          },
          label: {
            color: '#000',
            width: 80,
            fontSize: 11,
            lineHeight: 18,
            alignSelf: 'flex-start',
          },
          imageIcon: {
            tintColor: '#000',
            resizeMode: 'contain',
            width: 22,
            marginRight: 5,
          },
          text: {
            color: '#000',
            fontSize: 12,
            lineHeight: 18,
            alignSelf: 'flex-start',
          },
        
          boxItems: {
            marginHorizontal: 20,
          },
          //features
          boxFeature: {
            padding: 12,
            flexWrap: 'wrap',
            alignItems: 'stretch',
            flexDirection: 'row',
            marginBottom: 15,
          },
          lineTitle: {
            flexDirection: 'row',
            width: '80%',
            alignItems: 'center',
            paddingVertical: 10,
            paddingBottom: 15,
            paddingLeft: 20,
          },
          title: {
            fontSize: 20,
            color: 'rgb(69,69,83)',
          },
          item: {
            flexDirection: 'row',
            marginVertical: 5,
            width: '50%',
            alignItems: 'center',
          },
          iconFeature: {
            marginRight: 5,
          },
          nameFeature: {
            fontSize: 13,
          },
        
          //cates
          lineTitleCates: {
            width: '40%',
          },
        
          //map
          lineTitleMap: {
            width: '60%',
          },
          lineMapRight: {
            width: '40%',
            justifyContent: 'center',
            alignItems: 'center',
          },
          textMap: {
            fontSize: 12,
          },
        
          //related
          boxRelatedItems: {
            marginLeft: 10,
          },
        
          //FAQ
          boxFAQ: {
            padding: 12,
            borderRadius: 3,
            borderColor: '#eee',
            marginBottom: 15,
          },
          headerSection: {
            padding: 10,
            marginHorizontal: 4,
            flexDirection: 'row',
            borderRadius: 5,
          },
          headerText: {
            textAlign: 'center',
            fontSize: 16,
            fontWeight: '500',
          },
          contentSection: {
            paddingVertical: 20,
            paddingLeft: 10,
            backgroundColor: '#fff',
          },
          active: {
            backgroundColor: Colors.mainColorTheme,
          },
          inactive: {
            backgroundColor: '#fff',
          },
          contactustxt:{
            color:Colors.attributes.black,
            marginTop:12
          },
          rightview:{
            backgroundColor:Colors.tabbarTint,
            justifyContent:'center',
            alignItems:'center',
            height:'100%',
            width:'36%',
            marginRight:-10
          },
          rightrowview:{
            flexDirection:'row'
          },
          booktxt:{
            color:Colors.main,
            marginLeft:10
          }
})