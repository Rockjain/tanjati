import React from 'react';
import { View, Image, FlatList,ScrollView, TouchableOpacity, PermissionsAndroid, Platform, BackHandler, Linking, Share, Dimensions, SafeAreaView } from 'react-native';
import { Container, Text } from 'native-base';
import Colors from '../../../Config/Colors';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './Styles';
const { width } = Dimensions.get('window')
import * as Animatable from 'react-native-animatable';
import Accordion from 'react-native-collapsible/Accordion'
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
const call = require('../../../assets/Images/icons/call.png');
const share = require('../../../assets/Images/icons/icon-share.png');
const heart = require('../../../assets/Images/icons/icon-love.png');
const message = require('../../../assets/Images/icons/speech-bubble.png');
const back = require('../../../assets/Images/ic_black_back.png');
import Rating from '../../../Components/react-native-star-rating';
const activedoller = require('../../../assets/Images/icons/icon-dollar-active.png');
const inactivedoller = require('../../../assets/Images/icons/icon-dollar-inactive.png');
const desc = require('../../../assets/Images/icons/icon-desc.png');
const info = require('../../../assets/Images/icons/icon-table.png');
const tag = require('../../../assets/Images/icons/icon-tag.png');
const maptag = require('../../../assets/Images/icons/icon-location.png');
const time = require('../../../assets/Images/detail/icon-time.png');
const success = require('../../../assets/Images/ic_success.png');
const add = require('../../../assets/Images/ic_add.png');
const date = require('../../../assets/Images/date.png')
import Languages from '../../../Config/Languages';
import { connect } from 'react-redux';
import Loading from '../../../Components/Loadings';
const ANCHOR = { x: 0.5, y: 0.5 }
const ANCHOR_DETAIL = { x: 0.5, y: 1 }
const ANCHOR_ANDROID = { x: 0, y: -1.5 }

const CENTER = { x: 0, y: 0 }
const CENTER_DETAIL = { x: 5, y: 3 }
const CENTER_ANDROID = { x: 0, y: -3 }


 class PostDetail extends React.Component{
    constructor(props){
        super(props)
        this.state={
            post: props.navigation.getParam('items'),
            region:'',
            activeSections: [],
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount = () =>{
        const {post} = this.state;
        console.log(post)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        this._getLocationAsync()
    }
  
  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  
  handleBackButtonClick() {
    const {navigation} = this.props;
      navigation.navigate(navigation.getParam('navigateTo'),{navdata:'navdata'});
      return true;
  }

    renderPrice=(item)=>{
        let status=item.price_status
        let value = 1
        const priceFrom =item.list_price != '' && typeof item.list_price != 'undefined'
        ? item.list_price
        : ''
        const priceTo =
        item.list_price_to != '' && typeof item.list_price_to != 'undefined'
        ? item.list_price_to
        : ''
        const priceRange = priceFrom != '' && priceTo != '' ? priceFrom + '-' + priceTo : ''

     
        if (typeof status !== 'undefined' && status!= 'notsay' && priceRange != '') {
          if (status == 'inexpensive') value = 1
          if (status == 'moderate') value = 2
          if (status == 'pricey') value = 3
          if (status == 'ultra_high_end') value = 4
          return(
            <View style={styles.wrapPriceRange}>
            <Rating
            disabled
            fullStar={activedoller}
            emptyStar={inactivedoller}
            maxStars={4}
            starSize={14}
            starStyle={{ marginLeft: -2 }}
            rating={Number(value)}
            fullStarColor='#D5D8DE'
            emptyStarColor={'#ccc'}
          />
           <Text style={styles.priceRange}>
                  {priceRange ? priceRange : ''}
                </Text>
                <Text style={styles.priceRange}>
                  ({status})
                </Text>
          </View> 
          )
        }
    }
    renderRating=(item)=>{
        // if (item.totalRate && item.totalRate != 'undefined') {
            return (
                <View style={styles.ratingView}>
                    <Text style={styles.countText}>
                    {item.totalRate ? item.totalRate : 5}
                  </Text>
                    <Rating
                      disabled={false}
                      emptyStar={'star-o'}
                      fullStar={'star'}
                      halfStar={'star-half-o'}
                      iconSet={'FontAwesome'}
                      maxStars={5}
                      starSize={14}
                      starStyle={{ marginLeft:5 }}
                      halfStarEnabled
                      rating={Number(5)}
                      fullStarColor={Colors.active}
                      emptyStarColor={'#ccc'}
                    />
                </View>
                )  
        //  } else{
        //      return null
        //  }            
        }
    _renderContent = (data) => {
        let result = []
        if (data && data.gallery_images && data.gallery_images != null) {
          data.gallery_images.map((item, index) => {
            //let imageURL ='http://'+item.substring(8, item.length);
            let imageURL =item.replace('https://','http://');
            console.log("imageURL : "+imageURL)
            result.push(
              <View style={styles.imageView} key={index}>
                <Image
                  key={index}
                 // defaultSource={Images.imageHolderBooking}
                  style={styles.image}
                  source={{
                    uri: imageURL,
                  }}
                />
              </View>
            )
          })
        return result.map((item) => item)
        } else {
         
        }
         
      }
      renderHours = (hourss) =>{
        if (hourss && hourss.more_options && hourss.more_options.business_hours) {
          let hours = hourss.more_options.business_hours;
          return(
            <View style={styles.hourview}>
                <View style={styles.hourtitleview}>
                    <Image
                    source={time}
                    ></Image>
                    <Text style={styles.opentxt}>{Languages.open}</Text>
                </View>
                <View style={styles.timeview}>
                    <Text style={styles.timetxt}>{!hours.monday?'':'+ Monday '+hours.Monday.open+' '+hours.Monday.close}</Text>
                    <Text style={styles.timetxt}>{!hours.Tuesday?'':'+ Tuesday '+hours.Tuesday.open+' '+hours.Tuesday.close}</Text>
                    <Text style={styles.timetxt}>{!hours.Wednesday?'':'+ Wednesday '+hours.Wednesday.open+' '+hours.Wednesday.close}</Text>
                    <Text style={styles.timetxt}>{!hours.Thursday?'':'+ Thursday '+hours.Thursday.open+' '+hours.Thursday.close}</Text>
                    <Text style={styles.timetxt}>{!hours.Friday?'':'+ Friday '+hours.Friday.open+' '+hours.Friday.close}</Text>
                </View>
            </View>
        )
        }
       
      }
      renderFeature = (feature) =>{
        if(feature && feature.features && feature.features.length != 0){
          return(
            <View style={styles.featureview}>
                  <View style={styles.descnameview}>
                    <Image
                    source={tag}
                    style={styles.descimg}
                    ></Image>
                    <Text style={styles.desctxt}>{Languages.featurename}</Text>
                 </View>
                 {/* <View>
                     <Image
                     source={success}
                     
                     ></Image>
                     <Text style={styles.featuretxt}></Text>
                 </View> */}
                  <View style={styles.lineview}></View>
            </View>
        )
        }
         
      }
      renderCategory = (category) =>{
        if (category && category.pure_taxonomies) {
          let listing_Cat_string=JSON.stringify(category.pure_taxonomies)
          let listing_Cat_replaced=listing_Cat_string.replace('-','_')
           let listing_Cat= JSON.parse(listing_Cat_replaced)
           if (listing_Cat.length !=0) {
            return(
              <View style={styles.featureview}>
                    <View style={styles.descnameview}>
                      <Image
                      source={tag}
                      style={styles.descimg}
                      ></Image>
                      <Text style={styles.desctxt}>{Languages.category}</Text>
                   </View>
                  <FlatList
                  data={listing_Cat.listing_category}
                  keyExtractor={item => item.term_id}
                  renderItem={({ item }) => (
                      <View style={styles.categoryview}>
                          <Image
                          source={success}
                          style={{ height:20, width:20 }}
                          ></Image>
                          <Text style={styles.catname}>{item.name}</Text>
                      </View>
                  )}
                  ></FlatList>
                   <View style={styles.lineview}></View>
              </View>
          ) 
           }
        }
       
    }

    renderTags = (tagss) =>{
        if (tagss && tagss.pure_taxonomies) {
          let listing_Cat_string=JSON.stringify(tagss.pure_taxonomies)
          let listing_Cat_replaced=listing_Cat_string.replace('-','_')
           let listing_Cat= JSON.parse(listing_Cat_replaced)
           if (listing_Cat.length !=0) {
            return(
              <View style={styles.featureview}>
                    <View style={styles.descnameview}>
                      <Image
                      source={tag}
                      style={styles.descimg}
                      ></Image>
                      <Text style={styles.desctxt}>{Languages.tags}</Text>
                   </View>
                   <FlatList
                    data={listing_Cat.listing_category}
                    keyExtractor={item => item.term_id}
                    renderItem={({ item }) => (
                        <View style={styles.categoryview}>
                            <Image
                            source={success}
                            style={{ height:20, width:20 }}
                            ></Image>
                            <Text style={styles.catname}>{item.name}</Text>
                        </View>
                    )}
                    ></FlatList>
                    <View style={styles.lineview}></View>
              </View>
          ) 
           }
        }
       
  }
    

     renderFaq = (faq) =>{
      let sections = []
      Object.keys(faq.more_options.faqs.faq).forEach((k) => {
        if (k && faq.more_options.faqs['faq'][k] != '') {
          // console.warn([post.faqs['faq'][k], post.faqs['faqans'][k]])
          sections.push({
            title: faq.more_options.faqs['faq'][k],
            content: faq.more_options.faqs['faqans'][k],
          })
        }
      })
      if (sections.length != 0) {
        return (
          <View style={styles.section}>
           <View style={[styles.descnameview,{marginTop:15,marginLeft:20}]}>
                      <Image
                      source={tag}
                      style={styles.descimg}
                      ></Image>
                      <Text style={styles.desctxt}>{Languages.faqs}</Text>
                   </View>
            <View style={[styles.boxItems, styles.boxRelatedItems]}>
              <Accordion
                sections={sections}
                activeSections={this.state.activeSections}
                touchableComponent={TouchableOpacity}
                expandMultiple={true}
                renderHeader={this._renderHeaderFAQ}
                renderContent={this._renderContentFAQ}
                onChange={this._updateSections}
              />
            </View>
          </View>
        )
      }
      return <View />
     }

     _renderHeaderFAQ = (section, _, isActive) => {
      return (
        <Animatable.View
          duration={100}
          style={[
            styles.headerSection,
            isActive ? styles.active : styles.inactive,
          ]}
          transition="backgroundColor">
          <View style={{ flex: 2, alignItems: 'flex-start' }}>
            <Text
              style={[styles.headerText, { color: isActive ? '#FFF' : '#000' }]}>
              {section.title}
            </Text>
          </View>
  
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon
              name={isActive ? 'remove-circle-outline' : 'add-circle-outline'}
              size={18}
              color={isActive ? '#FFF' : Colors.mainColorTheme}
            />
          </View>
        </Animatable.View>
      )
    }

    _renderContentFAQ = (section, _, isActive) => {
      return (
        <Animatable.View
          duration={100}
          style={[styles.contentSection]}
          transition="backgroundColor">
          <Animatable.Text animation={isActive ? 'bounceIn' : undefined}>
            {section.content}
          </Animatable.Text>
        </Animatable.View>
      )
    }

    _updateSections = (activeSections) => {
      this.setState({ activeSections })
    }

     _getLocationAsync = async () => {
        try {
          const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
          if (granted != PermissionsAndroid.RESULTS.GRANTED) {
           Toast.show({type:'danger',text:'Permission denied',position:'top',duration:2000})
          }
      
          navigator.geolocation.getCurrentPosition(
              (position) => {
                console.log('position')
                console.log(position)
                const myPosition = position.coords
      
                this.setState({ myPosition })
                const current = {
                  address_lat: myPosition.latitude,
                  address_long: myPosition.longitude,
                }
              this.props.dispatch({type:'SET_REGION_MAP',region:current,index:0})
            
              },
              (error) =>  console.log(error),
              { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
            );
          } catch (err) {
            console.warn(err);
          }
            // not use for ListingPro due to Database could not be fetch
            // fetchNearestLocations(true, myPosition.latitude, myPosition.longitude)
         
        }
        renderCalloutMaker = (item, index) => {
          const name = typeof item.title != 'undefined' ? item.title.rendered : ''
          const location = item.job_location
          const phone = item.phone
      
          return (
            <MapView.Callout onPress={() => this.onViewPost(item, index)}>
              <TouchableOpacity
                activeOpacity={0.9}
                disabled={true}
                style={styles.slideInnerContainer}
                key={`calloutMarker-${index + 1}`}
                onPress={() => this.onViewPost(item, index)}>
                <View style={styles.wrapText}>
                  <View
                    style={styles.row}
                    onPress={() => this.onViewPost(item, index)}>
                    <Text style={[styles.title]}>
                      {Tools.getDescription(name, 100)}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    {Platform.OS == 'ios' && (
                      <Image style={styles.imageIcon} source={Images.icons.iconPin} />
                    )}
                    <Text style={styles.text}>{location}</Text>
                  </View>
                  <View
                    activeOpacity={0.9}
                    onPress={this.openPhone}
                    style={styles.row}>
                    {Platform.OS == 'ios' && (
                      <Image
                        style={styles.imageIcon}
                        source={Images.icons.iconPhone}
                      />
                    )}
                    <Text style={styles.text}>{phone}</Text>
                  </View>
      
                  <Text style={styles.textMore}>{Languages.readMore + '...'}</Text>
                </View>
              </TouchableOpacity>
            </MapView.Callout>
          )
        }
        renderMarkers = (mapdetail) => {
            console.log(mapdetail)
            const { width, height } = Dimensions.get('window')
            const ASPECT_RATIO = width / height
            const LATITUDE = 0
            const LONGITUDE = 0
            const LATITUDE_DELTA = 1.832
            const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
          const {
            listMarkers,
            // latitudeDelta,
            // longitudeDelta,
            isSearching,
            listMarkersSearch,
          } = this.props
          
          let listMarkersRender = mapdetail
          if (isSearching) {
            listMarkersRender = listMarkersSearch
          }
          let coordinates = {
            latitude: parseFloat(listMarkersRender.more_options.latitude),
            longitude: parseFloat(listMarkersRender.more_options.longitude),
            LATITUDE_DELTA,
            LONGITUDE_DELTA,
          }
              
                // warn(['markerItemMap', index]);
                return (
                  <MapView.Marker
                    key={'marker-' + 0}
                    ref={`marker${0}`}
                    anchor={ANCHOR}
                    centerOffset={CENTER}
                    coordinate={coordinates}
                    pinColor={Colors.map.defaultPinColor}
                    style={[styles.marker]}>
                 
                  </MapView.Marker>
                  //  {this.renderCalloutMaker(item, index)}
                )
        }

        openMap = (mapdetail) => {
            const url = `http://maps.apple.com/maps?daddr=${mapdetail.more_options.latitude},${mapdetail.more_options.longitude}`
            const urlGG = `http://maps.google.com/maps?daddr=${mapdetail.more_options.latitude},${mapdetail.more_options.longitude}`
            //WebBrowser.openBrowserAsync(Platform.OS == 'ios' ? url : urlGG)
            Linking.openURL(Platform.OS == 'ios' ? url : urlGG)
          }   

     renderMap = (mapdetail) =>{
        const {
            listMarkers,
            // latitudeDelta,
            // longitudeDelta,
            isSearching,
            listMarkersSearch,
          } = this.props
          if (mapdetail && mapdetail.more_options && mapdetail.more_options.latitude) {
            const { width, height } = Dimensions.get('window')
            const ASPECT_RATIO = width / height
              const LATITUDE = 0
              const LONGITUDE = 0
              const LATITUDE_DELTA = 1.832
              const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
            console.log('latitudedelta '+LATITUDE_DELTA)
            console.log('longitudedelta '+LONGITUDE_DELTA)
          let coordinate = {
              latitude: parseFloat(mapdetail.more_options.latitude),
              longitude: parseFloat(mapdetail.more_options.longitude),
              latitudeDelta:LATITUDE_DELTA,
              longitudeDelta:LONGITUDE_DELTA,
            } 
          return(
              <View>
                  <View style={styles.mapview}>
                  <View style={{ flexDirection:'row' }}>
                      <Image
                      source={maptag}
                      style={{ height:20, width:20 }}
                      ></Image>
                      <Text style={styles.desctxt}>{Languages.Map}</Text>
                  </View>
                  <Text style={styles.getdirectiontxt}   onPress={()=>this.openMap(mapdetail)}>{Languages.getdirection}</Text>
                  </View>
                  <View style={styles.mapshow}>
                  <MapView
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              ref={(ref) => (this.map = ref)}
              region={coordinate}
              loadingEnabled
              showsUserLocation
              userLocationAnnotationTitle={"You're Here"}
              loadingIndicatorColor={Colors.map.loading}>
              {this.renderMarkers(mapdetail)}
            </MapView>
                  </View>
              </View>
          ) 
          } else {
            
          }
     }

     _call = (post) => {
      var phone = post.more_options.phone
      const phoneOpen =
        Platform.OS == 'ios' ? `telprompt:${phone}` : `tel:${phone}`
      Linking.canOpenURL(phoneOpen)
        .then((supported) => {
          if (supported) {
            return Linking.openURL(phoneOpen).catch((err) => console.error(err))
          }
        })
        .catch((err) => console.error(['err:', err]))
    }

    _share = (post) => {

      Share.share({
        // message: post.content.replace(/(<([^>]+)>)/gi, '') + post.link,
        message: post.link,
        url: post.link,
        title: post.title.rendered,
      })
    }

    showDesc = (post) =>{
      if (post && post.content && post.content.rendered != '') {
        return(
          <View style={styles.descview}>
          <View style={styles.descnameview}>
             <Image
             source={desc}
             style={styles.descimg}
             ></Image>
             <Text style={styles.desctxt}>{Languages.descriptionList}</Text>
          </View>
          <Text style={styles.descrestxt}>{post.content.rendered.substring(3, post.content.rendered.length-5)}</Text>
          <View style={styles.lineview}></View>
       </View>
        ) 
      } else {

      }
    }
  
    render(){
        const {post} = this.state;
        const {navigation,isFetching} = this.props;
        return(
            <SafeAreaView style={styles.container}>
               <View style={{flex: 1}}>
                <TouchableOpacity style={styles.btnBack} onPress={()=>navigation.navigate(navigation.getParam('navigateTo'),{navdata:'navdata'})}>
                    <Image
                    source={back}
                    style={{ tintColor:Colors.attributes.black }}
                    ></Image>
                </TouchableOpacity>
                <ScrollView style={{ height:Dimensions.get('window').height-45 }}>
                <ScrollView
                showsHorizontalScrollIndicator={false}
                snapToInterval={width - 30}
                snapToAlignment={'start'}
                decelerationRate={'fast'}
                horizontal={true}>
                {this._renderContent(post)}
                </ScrollView>
                <View style={styles.nameview}>
                    <Text style={styles.nametxt}>{post && post.title && post.title.rendered?post.title.rendered:''}</Text>
                    <Text style={styles.qualitytxt}>{post.listing_data.lp_listingpro_options.tagline_text}</Text>
                    {this.renderPrice(post.more_options)}
                    {this.renderRating(post.more_options)}
                </View>
                <View style={styles.lineview}></View>
                {this.showDesc(post)}
                <View style={styles.descview}>
                   <View style={styles.descnameview}>
                      <Image
                      source={info}
                      style={styles.descimg}
                      ></Image>
                      <Text style={styles.desctxt}>{Languages.information}</Text>
                   </View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                     <Text style={styles.infotitle}>{Languages.Address}</Text>
                     </View>
                        <Text style={styles.infores}>{post.more_options.gAddress}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.Phone}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.phone}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.Email}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.email}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.twitter}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.twitter}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.facebook}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.facebook}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.linkedin}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.linkedin}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.youtube}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.youtube}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                     <View style={styles.inforowview}>
                     <View style={{ width:60 }}>
                        <Text style={styles.infotitle}>{Languages.website}</Text>
                        </View>
                        <Text style={styles.infores}>{post.more_options.website}</Text>
                     </View>
                     <View style={styles.smallline}></View>
                </View>
                <View style={styles.lineview}></View>
                <View style={styles.bussview}>
                {this.renderMap(post)}
                <View style={[styles.descnameview,{marginTop:15}]}>
                      <Image
                      source={tag}
                      style={styles.descimg}
                      ></Image>
                      <Text style={styles.desctxt}>{Languages.business_hours}</Text>
                   </View>
                   {this.renderHours(post)}
                   <View style={styles.lineview}></View>
                   {this.renderFeature(post)}
                   {this.renderCategory(post)}
                   {this.renderTags(post)}
                   {this.renderFaq(post)}
                </View>
                </ScrollView>
              <View style={styles.footerview}>
                  <Text style={styles.contactustxt}>{Languages.contactuspost}</Text>
                <TouchableOpacity onPress={()=>this._call(post)}>
                   <Image
                    source={call}
                    style={styles.callicon}
                   ></Image>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={()=>this._share(post)}>
                   <Image
                    source={share}
                    style={styles.shareicon}
                   ></Image>
                   </TouchableOpacity>
                   <Image
                    source={heart}
                    style={styles.hearticon}
                   ></Image>
                   <Image
                    source={message}
                    style={styles.callicon}
                   ></Image>
                   <View style={styles.rightview}>
                       <View style={styles.rightrowview}>
                         <Image
                         source={date}
                         style={{height:16,width:16,marginTop:2}}
                         ></Image>
                            <Text style={styles.booktxt}>{Languages.book}</Text>  
                       </View>  
                   </View>
                </View>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
      isFetching:state.Post.isFetching,
      initialRegion:  state.Map.region,
    //  latitudeDelta:  state.Map.latitudeDelta,
    //  longitudeDelta:  state.Map.longitudeDelta,
     isSearching:  state.Map.isSearching,
     indexActive:  state.Map.index,
    }
    }
    
  export default connect(
      mapStateToProps
    )(PostDetail)