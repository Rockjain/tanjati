import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../../Config/Colors';
const {width,height}=Dimensions.get('window');

export default StyleSheet.create({
    conainer:{
        backgroundColor:Colors.main,
        flex:1
    },
    arrow:{
        marginTop:14,
        width:28,
        height:12,
        marginLeft:10
    },
    cattxt:{
        fontSize:20,
        color:Colors.attributes.black,
        marginTop:20,
        marginLeft:10
    },
    bannerview:{
        width:Dimensions.get('window').width,
        height:240,
        justifyContent:'center'
    },
    headtxt:{
        color:Colors.main,
        fontSize:20,
        marginLeft:20,
        fontWeight:'bold',
        
    },
    txtview:{
        marginBottom:20
    },
    inputview:{
        marginHorizontal:10,
        borderRadius:5,
        marginTop:-55,
        paddingHorizontal:10,
        //paddingVertical:5,
        backgroundColor:Colors.main,
        flexDirection:'row',
        height:45
    },
    findheadtxt:{
        marginTop:60,
        fontSize:22, 
        fontWeight:'bold',
        marginLeft:10
    },
    findlistview:{
        marginTop:16,
        borderRadius:3,
        marginLeft:12,
        height:120,
        width:140,
        elevation:2,shadowOffset:{width:10,height:10},shadowOpacity:5,shadowRadius:10,borderWidth:0.5,borderColor:'#E5E7E9',shadowColor:'#E5E7E9' 

    },
    findtxt:{
        marginLeft:5,
        color:Colors.attributes.black,
        marginTop:5,
        fontWeight:'bold'
    },
    plustxt:{
      marginLeft:10,
      fontSize:22, 
      fontWeight:'bold',
      marginTop:30  
    },
    desctxt:{
        marginLeft:10,
        fontSize:14,
        marginTop:5, 
        color:Colors.blackTextSecondary
    },
    bannerhotel:{
        height:180,
        width:Dimensions.get('window').width-10,
    },
    featuretxtview:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginHorizontal:10,
    },
    featuretxt:{
      fontSize:22, 
      fontWeight:'bold',
      marginTop:30 
    },
    seetxt:{
        color:Colors.red,
        marginTop:34,
        fontSize:12 ,
        fontWeight:'bold'
    },
    featureview:{
        marginTop:16,
        borderRadius:3,
        marginLeft:12,
        height:240,
        width:width/2-20,
    },
    featureimage:{
        height:150,
        width:width/2-20,
        borderRadius:2
     },
    typetxt:{
        color:Colors.red,
        fontSize:13,
        marginTop:4
    },
    featurename:{
        color:Colors.attributes.black,
        marginTop:6,
        fontSize:13,
        fontWeight:'bold'
    },
    quality:{
        fontSize:12,
        color:Colors.attributes.black
    },
    wrapPriceRange: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, .75)',
        padding: 5,
        borderTopRightRadius: 5,
      },
      priceRange: {
        fontSize: 11,
        marginLeft: 5,
        color: '#FFF',
      },
      ratingView: {
        flexDirection: 'row',
        position: 'absolute',
        right: 0,
        bottom: 0,
        zIndex: 999,
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .75)',
        padding: 3,
      },
      countText: {
        fontSize: 11,
        marginLeft: 8,
        color: '#666',
      },
})