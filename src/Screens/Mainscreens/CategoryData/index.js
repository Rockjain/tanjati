import React from 'react';
import { View, Text,PermissionsAndroid,TouchableOpacity,Image,
  Dimensions,Platform,AsyncStorage,FlatList, ImageBackground, BackHandler,SafeAreaView} from 'react-native';
import styles from './Styles';
import Colors from '../../../Config/Colors';
import Footer from '../../../Components/Footer';
import {
    Container,
     Header,
     Left,
     Body,
     Icon,
     Toast
   } from 'native-base';
   import { connect } from 'react-redux'
   import Loadings from '../../../Components/Loadings';
   const arrow = require('../../../assets/Images/backlong.png');
   import Rating from '../../../Components/react-native-star-rating';
   const activedoller = require('../../../assets/Images/icons/icon-dollar-active.png')
const inactivedoller = require('../../../assets/Images/icons/icon-dollar-inactive.png')
const greyback = require('../../../assets/Images/greyback.png')


    class CategoryData extends React.Component{
    constructor(props){
        super(props)
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state={
          catId:'',
          catName:'',
          navdata: props.navigation.getParam('navdata'),
        }
       }

       componentWillMount=async()=>{
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        let catid=await AsyncStorage.getItem('CatID')
        let catname=await AsyncStorage.getItem('CatName')
         console.log('Category id')
         console.log(catid)
         this.props.dispatch({type:'Fetch_Search_Category_Request',payload:{url:'/wp-json/search/v1/listing?categories='+catid}})
         this.setState({catId:catid,catName:catname})
        // console.log(this.props.searchcategory)
       }

       componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
      this.props.navigation.navigate('home');
      return true;
  }

       renderPrice=(item)=>{
        let status=item.price_status
        let value = 1
        const priceFrom =item.list_price != '' && typeof item.list_price != 'undefined'
        ? item.list_price
        : ''
        const priceTo =
        item.list_price_to != '' && typeof item.list_price_to != 'undefined'
        ? item.list_price_to
        : ''
        const priceRange = priceFrom != '' && priceTo != '' ? priceFrom + '-' + priceTo : ''
     
        if (typeof status !== 'undefined' && status!= 'notsay' && priceRange != '') {
          if (status == 'inexpensive') value = 1
          if (status == 'moderate') value = 2
          if (status == 'pricey') value = 3
          if (status == 'ultra_high_endif') value = 4
          return(
            <View style={styles.wrapPriceRange}>
            <Rating
            disabled
            fullStar={activedoller}
            emptyStar={inactivedoller}
            maxStars={4}
            starSize={10}
            starStyle={{ marginLeft: -2 }}
            rating={Number(value)}
            fullStarColor='#D5D8DE'
            emptyStarColor={'#ccc'}
          />
           <Text style={styles.priceRange}>
                  {priceRange ? priceRange : ''}
                </Text>
          </View> 
          )
        }
    }
    renderRating=(item)=>{
    // if (item.totalRate && item.totalRate != 'undefined') {
        return (
            <View style={styles.ratingView}>
             
                <Rating
                  disabled={false}
                  emptyStar={'star-o'}
                  fullStar={'star'}
                  halfStar={'star-half-o'}
                  iconSet={'FontAwesome'}
                  maxStars={1}
                  starSize={10}
                  starStyle={{ marginLeft:2 }}
                  halfStarEnabled
                  rating={Number(5)}
                  fullStarColor={Colors.active}
                  emptyStarColor={'#ccc'}
                />
                <Text style={styles.countText}>
                {item.totalRate ? item.totalRate : 5}
              </Text>
             
            </View>
            )  
    //  } else{
    //      return null
    //  }  
      
    }

       renderRecents=({item})=>{
        let listing_Cat_string=JSON.stringify(item.pure_taxonomies)
        let listing_Cat_replaced=listing_Cat_string.replace('-','_')
         let listing_Cat= JSON.parse(listing_Cat_replaced)
          let img =  item.gallery_images == null?greyback:'http://'+item.gallery_images[0].substring(7, item.length);   
      return(
        <TouchableOpacity style={styles.featureview} onPress={()=>this.props.navigation.navigate('postdetail', { items:item, navigateTo:'catdata' })}>
        <ImageBackground
        source={item.gallery_images == null?greyback:{uri:img}}
        style={styles.featureimage}
        >
        {this.renderPrice(item.more_options)}
        {this.renderRating(item.more_options)}
        </ImageBackground>
        <Text style={styles.typetxt}>{listing_Cat.listing_category[0].name}</Text>
        <Text style={styles.featurename} numberOfLines={1}>{item.title.rendered}</Text>
        <Text style={styles.quality} numberOfLines={2}>{item.listing_data.lp_listingpro_options.tagline_text}</Text>
        </TouchableOpacity>
      )
    }
       render(){
           const {catName,navdata} = this.state;
           const {searchcategory,navigation,isFetching} = this.props;
           console.log('my success')
           console.log(searchcategory)
           if (navdata != 'navdata') {
           if (isFetching) {
               return(
                 <Loadings />
               )
           }
          }
           return(
               <SafeAreaView style={styles.conainer}>
                   <TouchableOpacity onPress={()=>navigation.navigate('home')}>
                   <Image
                   source={arrow}
                   style={styles.arrow}
                   ></Image>
                   </TouchableOpacity>
                   <Text style={styles.cattxt}>{catName}</Text>
                   <View style={{ alignItems:'center' }}>
                   <FlatList
                    data={searchcategory}
                    keyExtractor={item => item.id}
                    numColumns={2}
                    renderItem={this.renderRecents}
                    styl={{marginBottom:50}}
                    ></FlatList>
                    </View>
                    <Footer />
               </SafeAreaView>
           )
       }
   }

   const mapStateToProps=(state)=>{
    return{
     posts:state.Post.posts,
     searchcategory:state.Category.FetchCategories,
     isFetching:state.Category.isFetching,
    }
    }
    
  export default connect(
      mapStateToProps
    )(CategoryData)