import React from 'react';
import { View,  ImageBackground, TouchableOpacity, Dimensions, Image, TextInput, Picker, ScrollView, Modal,
    TouchableWithoutFeedback, ActivityIndicator, BackHandler, SafeAreaView, Platform, PermissionsAndroid, Linking, Alert } from 'react-native';
import {
    Container,
    Text, 
    Item,
    Input,
    Textarea
  } from 'native-base';
import Styles from './Styles';
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
import ImagePicker from 'react-native-image-picker';
const back = require('../../../assets/Images/backlong.png');
const plus = require('../../../assets/Images/ic_add.png');
import { connect } from 'react-redux';
import ToastHelper from '../../../Config/ToastHelper';
import axios from 'axios';
import Loading from '../../../Components/Loadings';
import Permissions from 'react-native-permissions';
import GPSState from 'react-native-gps-state';
import Footer from '../../../Components/Footer';

const arrow = require('../../../assets/Images/ic_down_arrow.png')

 class GetIssue extends React.Component{
    constructor(props){
        super(props)
        this.state = {
             summary:props.navigation.getParam('nextheading'),
             project:props.navigation.getParam('nextproject'), 
             docname:props.navigation.getParam('documentname'),
             document:props.navigation.getParam('doc'), 
             description:'',
             loading:false,
            //  lat:null,
            //  long:null
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    componentWillMount = () =>{
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

    }

    handleBackButtonClick() {
        const {navigation} = this.props;
          navigation.navigate('issue');
          return true;
      }

    goToSave = () =>{
        const {summary,project,description,docname,document,lat,long} = this.state;
        console.log(document)
        if(description == ''){
            ToastHelper.danger('Please add description!');  
        } else {
            if (Platform.OS == 'ios') {
                this.props.navigation.navigate('issuemap',{ projects:project, sum:summary, des:description, doc:document, documentname:docname })
            } else {
                GPSState.addListener((status)=>{
                    switch(status){
                        case GPSState.NOT_DETERMINED:
                            console.log('NOT_DETERMINED')
                            Alert.alert('Alert',
                            Languages.enablelocation,
                            [{
                                text:'Cancel',
                                onPress:()=>console.log('Cancel Pressed'),
                            },{
                                text:'OK',
                                onPress:()=>GPSState.openLocationSettings(),
                            }],
                            { cancelable: true });
                        break;
             
                        case GPSState.RESTRICTED:
                            console.log('RESTRICTED')
                            Alert.alert('Alert',
                            Languages.enablelocation,
                            [{
                                text:'Cancel',
                                onPress:()=>console.log('Cancel Pressed'),
                            },{
                                text:'OK',
                                onPress:()=>GPSState.openLocationSettings(),
                            }],
                            { cancelable: true });
                            // GPSState.openLocationSettings()
                        break;
             
                        case GPSState.DENIED:
                            console.log('DENIED')
                            Alert.alert('Alert',
                            Languages.enablelocation,
                            [{
                                text:'Cancel',
                                onPress:()=>console.log('Cancel Pressed'),
                            },{
                                text:'OK',
                                onPress:()=>GPSState.openLocationSettings(),
                            }],
                            { cancelable: true });
                        break;
             
                        case GPSState.AUTHORIZED_ALWAYS:
                            //TODO do something amazing with you app
                            console.log('AUTHORIZED_ALWAYS')
                            this.props.navigation.navigate('issuemap',{ projects:project, sum:summary, des:description, doc:document, documentname:docname })
                        break;
             
                        case GPSState.AUTHORIZED_WHENINUSE:
                            console.log('AUTHORIZED_WHENINUSE')
                            //TODO do something amazing with you app
                            this.props.navigation.navigate('issuemap',{ projects:project, sum:summary, des:description, doc:document, documentname:docname })
                        break;
                    }
                })
                GPSState.requestAuthorization(GPSState.AUTHORIZED_WHENINUSE)   
            }
        }
    }
    
    render(){
        const {navigation} = this.props;
        return(
            <SafeAreaView style={Styles.container}>
            <ScrollView contentContainerStyle={Platform.OS=='ios'?{marginHorizontal:10}:{}}>
                <View style={Styles.headview}>
                 <TouchableOpacity onPress={()=>navigation.navigate('issue')}>
                     <Image
                     source={back}
                     style={Styles.menustyle}
                     ></Image>
                     </TouchableOpacity>
                 </View>
                 <Text style={Styles.posttxt}>{Languages.descriptionList}</Text>
                 <Textarea  onChangeText={text=>this.setState({description:text})}
                 style={{ borderWidth:1, borderColor:Colors.blue, borderRadius:5,marginTop:10 }} rowSpan={5}>
        
                 </Textarea>
                 <TouchableOpacity style={Styles.btnview} onPress={()=>this.goToSave()}>
                        <Text style={Styles.btntxt}> {Languages.next} </Text>
                 </TouchableOpacity>
                 </ScrollView>
                 <Footer />
            </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
        isFetching:state.Issue.isFetching,
    }
}
export default connect(mapStateToProps)(GetIssue)