import { StyleSheet,Dimensions, Platform } from 'react-native';
import Colors from '../../../Config/Colors';

export default StyleSheet.create({
    container:{
        paddingHorizontal:10,
        flex:1
    },
    ioscontainer:{
      paddingLeft:10,
      paddingRight:10,
      flex:1
  },
    headview:{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:Colors.main,
        //paddingHorizontal:10,
        marginTop:13
    },
    menustyle:{
        height:15,
        width:34,
    },
    iosmenustyle:{
      height:15,
      width:34,
      marginLeft:10
  },
    nexttxt:{
        color:Colors.blue
    },
    publishtxt:{
        fontSize:26,
        fontWeight:'bold',
        marginTop:25,
        color:Colors.attributes.black
    },
    posttxt:{
        fontSize:18,
        marginTop:25
    },
    lineview:{
        borderBottomWidth:1,
        borderBottomColor:Colors.lightGrey,
    },
    categoryview:{
        borderColor:Colors.blue,
        borderRadius:25,
        marginHorizontal:10,
        marginTop:10,
        borderWidth:1,
        flexDirection:'row',
        justifyContent:'space-between',
        padding:10,
        height:45
    },
    ioscategoryview:{
      borderColor:Colors.blue,
      borderRadius:25,
      marginTop:10,
      borderWidth:1,
      flexDirection:'row',
      justifyContent:'space-between',
      padding:10,
      height:45
  },
    cattxt:{
        color:Colors.attributes.black
    },
    button:{
        height: 34,
        paddingHorizontal: 10,
        borderRadius: 17,
        borderWidth: 0.5,
        borderColor: Colors.appColor,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
      },
      dropdownIcon:{
        width: 10,
        height: 10,
        resizeMode: 'contain',
        marginLeft: 5
      },
      value:{
        color: Colors.appColor,
        fontSize: 14,
      },
      background:{
        flex:1,
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: 'center',
        justifyContent: 'center',
      },
      content:{
        backgroundColor: 'white',
        borderRadius: 8,
        width: 300,
        paddingBottom: 10
      },
      title:{
        fontSize: 13,
        color: 'rgba(191, 192, 192, 1)',
        textAlign: 'center',
        marginVertical: 10
      },
      separator:{
        height: 0.5,
        backgroundColor: 'rgba(191, 192, 192, 1)',
        marginBottom:10
      },
      item:{
        flexDirection:'row',
        alignItems:'center',
        paddingVertical: 15,
        paddingHorizontal: 40
      },
      icon:{
        width: 20,
        height: 20,
         resizeMode: 'contain',
         marginRight: 10
      },
      text:{
        fontSize: 16,
        color: 'rgba(191, 192, 192, 1)'
      },
      selected:{
        backgroundColor: Colors.main,
        marginTop:10,
        marginLeft:10
      },
      selectedIcon:{
        tintColor: Colors.appColor
      },
      selectedText:{
        color: Colors.grey
      },
      imgWrap:{
        height: 250,
        borderRadius: 5,
        alignItems:'center',
        justifyContent:'center',
        marginTop:20
      },
      btnview:{
        backgroundColor:Colors.tabbarTint,
        alignItems:'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingVertical:5,
         marginTop:10,
        borderRadius:4,
        alignSelf:'flex-end',
        marginHorizontal:10,
        marginBottom:20
    },
    btntxt:{
      color:Colors.main,
  },
  footerrowview:{
    flexDirection:'row',
    height:55,
    width:Dimensions.get('window').width,
    position:'absolute',
    bottom:0,
    backgroundColor:Colors.main
},
menutxt:{
    color:Colors.bottomblue,
    fontSize:12
},
menuview:{
    marginHorizontal:10
},
bottomdarkview:{
    flexDirection:'row',
    backgroundColor:Colors.bottomblue,
    justifyContent:'center',
    alignItems:'center',
    width:Dimensions.get('window').width-30,
    height:'100%'
},
footertxt:{
    color:Colors.main,
    fontWeight:'bold'
},
inputcomponentstyle:{
  
  ...Platform.select({
    ios: {
      marginBottom:0,
      marginTop:5
    },
    android: {
      marginBottom:-10
    },
  }),
}
})