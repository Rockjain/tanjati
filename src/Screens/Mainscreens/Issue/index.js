import React from 'react';
import { View,  ImageBackground, TouchableOpacity, Platform, Image, TextInput, Picker, ScrollView, Modal,
    TouchableWithoutFeedback, SafeAreaView, PermissionsAndroid,AsyncStorage, Alert  } from 'react-native';
import {
    Container,
    Text, 
    Item,
    Input,
  } from 'native-base';
import Styles from './Styles';
const ANCHOR = { x: 0.5, y: 0.5 }
const ANCHOR_DETAIL = { x: 0.5, y: 1 }
const ANCHOR_ANDROID = { x: 0, y: -1.5 }
const CENTER = { x: 0, y: 0 }
const CENTER_DETAIL = { x: 5, y: 3 }
const CENTER_ANDROID = { x: 0, y: -3 }
import Colors from '../../../Config/Colors';
import Languages from '../../../Config/Languages';
import ImagePicker from 'react-native-image-picker';
const back = require('../../../assets/Images/backlong.png');
const plus = require('../../../assets/Images/ic_add.png');
import { connect } from 'react-redux';
import ToastHelper from '../../../Config/ToastHelper';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import axios from 'axios';
import ImageResizer from 'react-native-image-resizer';
import RNFetchBlob from 'react-native-fetch-blob';
import GPSState from 'react-native-gps-state';
var RNGRP = require('react-native-get-real-path');
var RNFS = require('react-native-fs');
const arrow = require('../../../assets/Images/ic_down_arrow.png')
const myimg = require('../../../assets/Images/click.png')
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Permissions from 'react-native-permissions';
const menu = require('../../../assets/Images/menu.png')
const newplus = require('../../../assets/Images/newplus.png');
import Footer from '../../../Components/Footer';
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

 class Issue extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            img:myimg,
            itemSelected: '',
            modalVisible: false,
            selectedIndex: 0,
            itemname:'',
            headings:'',
            name:'',
            showimg:myimg,
            isTrue:false,
            myuser:null,
        }
    }

    componentWillMount = async() =>{
        const {navigation} = this.props;
        let userinfo = await AsyncStorage.getItem('User');
        user=JSON.parse(userinfo)
        this.setState({myuser:JSON.parse(userinfo)})
        this.props.dispatch({type:'Fetch_Product_Request', payload:{url:'/api/rest/projects'}})
        try {
          if (Platform.OS=='ios') {
            const granted=await Permissions.check('location')
            if (granted=='authorized') {
              this.setState({isTrue:false})
              navigator.geolocation.getCurrentPosition(
                (position) => {
                 console.log('position')
                 console.log(position)
                },
                (error) =>  console.log(error),
                { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
              );
            } else {
              // this.setState({isTrue:true})
              //ToastHelper.danger(Languages.allowlocation);
              const granted=await Permissions.request('location')
              if (granted=='authorized') {
                this.setState({isTrue:false})
                navigator.geolocation.getCurrentPosition(
                  (position) => {
                   console.log('position')
                   console.log(position)
                  },
                  (error) =>  console.log(error),
                  { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
                );
              } else {
                this.setState({isTrue:true})
                ToastHelper.danger(Languages.allowlocation); 
              }
            }
          } else {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
            console.log('granted')
            console.log(granted)
          if (granted != PermissionsAndroid.RESULTS.GRANTED) {
              this.setState({isTrue:true})
              ToastHelper.danger(Languages.allowlocation); 
          } else {
            this.setState({isTrue:false})
          }
          }
          } catch (err) {
            console.warn(err);
          }
    }

    show = () => {
        this.setState({ modalVisible: true })
      }
    
      hide = () => {
        this.setState({ modalVisible: false })
      }

    categoryModel = () =>{
        const {itemname} = this.state;
        return(
            <TouchableOpacity style={Platform.OS=='ios'?Styles.ioscategoryview:Styles.categoryview} onPress={this.show}>
               <Text style={Styles.cattxt}>{itemname == ''?Languages.typeclaim:itemname}</Text>
               <Image 
               source={arrow}
               ></Image>
            </TouchableOpacity>
        )
    }

    renderModel = () =>{
        let { products } = this.props
        const {modalVisible} = this.state;
        console.log('my products')
        console.log(products.projects)
        if (products.projects == undefined || products.projects.length == 0) {
          if (modalVisible) {
            ToastHelper.danger(Languages.nointernet) 
          }
          return(
            <View />
          ) 
        } else {
            console.log('in else')
            return (
                <Modal
                  animationType="fade"
                  transparent={true}
                  visible={this.state.modalVisible}
                  onRequestClose={this.hide}>
                  <TouchableWithoutFeedback onPress={this.hide}>
                    <View style={Styles.background}>
                      <View style={Styles.content}>
                        <Text style={Styles.title}>{Languages.typeclaim}</Text>
                        <View style={Styles.separator} />
                        {products.projects.map((item, index) => this.renderItem(item, index))}
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </Modal>
              )
        }
    }

    renderItem = (item, index) => {
        let { selectedIndex } = this.state
        return (
          <TouchableOpacity
            key={index}
            style={[Styles.item && Styles.selected]}
            onPress={() => this.onPressItem(item.name)}
            activeOpacity={0.85}>
            <Text
              style={[Styles.text && Styles.selectedText]}>
              {item.name}
            </Text>
          </TouchableOpacity>
        )
      }
    
      onPressItem = (name) => {
        this.setState({ itemname: name })
        this.hide()
      }

       getUpload = () =>{
        // const options = {
        //   title: 'Select Avatar',
        //   customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        //   storageOptions: {
        //     skipBackup: true,
        //     path: 'images',
        //   },
        // };
        ImagePicker.showImagePicker((response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = { uri: response.uri };
           // You can also display the image using data:
            const sourcedata = { uri: 'data:image/jpeg;base64,' + response.data };
            console.log('source')
            console.log(sourcedata)
            ImageResizer.createResizedImage(response.uri, response.width/2, response.height/2, "JPEG", 80)
            .then(res => {
              RNFetchBlob.fs
                .readFile(res.path, "base64")
                .then(data => {
                  console.log(data)
                   this.setState({
                    img: data,
                    showimg: source,
                    name: response.fileName
                  });
                })
                .catch(err => {
                  console.log(err);
                });
              })
            .catch(err => {
              console.log(err);
            });
           
          }
        });
       }

       goToNext = async() =>{
         const {navigation} = this.props;
         const {headings,itemname, img, name,showimg,isTrue} = this.state;
         console.log(name)
          if(headings == '' || itemname==''||img==myimg){
          ToastHelper.danger(Languages.plesefillallfield); 
         } else {
          if (Platform.OS == 'ios') {
            if (isTrue == true) {
              ToastHelper.danger(Languages.allowlocation);
             } else {
              navigation.navigate('issuemap',{ nextheading:headings, nextproject:itemname, documentname:name, doc:img});
              this.setState({ 
                headings:'',
                itemname:'',
                showimg:myimg,
               })
             }
        } else {
          GPSState.addListener((status)=>{
            switch(status){
                case GPSState.NOT_DETERMINED:
                    console.log('NOT_DETERMINED')
                    Alert.alert('Alert',
                    Languages.enablelocation,
                    [{
                        text:'Cancel',
                        onPress:()=>console.log('Cancel Pressed'),
                    },{
                        text:'OK',
                        onPress:()=>GPSState.openLocationSettings(),
                    }],
                    { cancelable: true });
                break;
     
                case GPSState.RESTRICTED:
                    console.log('RESTRICTED')
                    Alert.alert('Alert',
                    Languages.enablelocation,
                    [{
                        text:'Cancel',
                        onPress:()=>console.log('Cancel Pressed'),
                    },{
                        text:'OK',
                        onPress:()=>GPSState.openLocationSettings(),
                    }],
                    { cancelable: true });
                    // GPSState.openLocationSettings()
                break;
     
                case GPSState.DENIED:
                    console.log('DENIED')
                    Alert.alert('Alert',
                    Languages.enablelocation,
                    [{
                        text:'Cancel',
                        onPress:()=>console.log('Cancel Pressed'),
                    },{
                        text:'OK',
                        onPress:()=>GPSState.openLocationSettings(),
                    }],
                    { cancelable: true });
                break;
     
                case GPSState.AUTHORIZED_ALWAYS:
                    //TODO do something amazing with you app
                    console.log('AUTHORIZED_ALWAYS')
                    if (isTrue == true) {
                      ToastHelper.danger(Languages.allowlocation);
                     } else {
                      navigation.navigate('issuemap',{ nextheading:headings, nextproject:itemname, documentname:name, doc:img});
                      this.setState({ 
                        headings:'',
                        itemname:'',
                        showimg:myimg,
                       })
                     }
                break;
     
                case GPSState.AUTHORIZED_WHENINUSE:
                    console.log('AUTHORIZED_WHENINUSE')
                    //TODO do something amazing with you app
                    if (isTrue == true) {
                      ToastHelper.danger(Languages.allowlocation);
                     } else {
                      navigation.navigate('issuemap',{ nextheading:headings, nextproject:itemname, documentname:name, doc:img});
                      this.setState({ 
                        headings:'',
                        itemname:'',
                        showimg:myimg,
                       })
                     }
                break;
            }
        })
        GPSState.requestAuthorization(GPSState.AUTHORIZED_WHENINUSE)
      }
         }
       }
    
    render(){
        const {img,showimg,headings} = this.state;
        const {navigation} = this.props;
        return(
            <SafeAreaView style={Styles.container}>
            <KeyboardAwareScrollView style={Platform.OS=='ios'?{marginLeft:10,marginRight:10}:{}}>
              <ScrollView> 
                <View style={Styles.headview}>
                 <TouchableOpacity onPress={()=>navigation.navigate('home')}>
                     <Image
                     source={back}
                     style={Styles.menustyle}
                     ></Image>
                     </TouchableOpacity>
                 </View>
                 <Text style={Styles.publishtxt}>{Languages.createclaim}</Text>
                 <TouchableOpacity onPress={()=>{this.getUpload()}}>
                 <ImageBackground
                 style={Styles.imgWrap}
                 source={showimg}
                 >
                 </ImageBackground>
                 </TouchableOpacity>
                 <Text style={Styles.posttxt}>{Languages.titleclaim}</Text>
                 {/* <View style={{ borderWidth:1, borderColor:Colors.blue, borderRadius:5, height:45,marginTop:10 }}> */}
                 <TextInput
                 onChangeText={(text)=>this.setState({ headings:text })}
                 placeholder={Languages.problemdecl}
                 placeholderTextColor={Colors.attributes.black}
                 multiline={true}
                 value={headings}
                 style={Styles.inputcomponentstyle}
                 ></TextInput>
                 {/* </View> */}
                 <View style={Styles.lineview}></View>
                 {this.categoryModel()}
                 {this.renderModel()}
                 <TouchableOpacity style={Styles.btnview} onPress={()=>this.goToNext()}>
                        <Text style={Styles.btntxt}> {Languages.next} </Text>
                 </TouchableOpacity>
                 </ScrollView>
                 </KeyboardAwareScrollView>
                <Footer />
            </SafeAreaView>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
       categories:state.Category.Categories,
       posts:state.Post.posts ,
       user: state.Auth.UserInfo,
       products:state.Issue.products,
    }
}
export default connect(mapStateToProps)(Issue)