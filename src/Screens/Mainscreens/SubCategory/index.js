import React from 'react';
import { View, Text,PermissionsAndroid,TouchableOpacity,Image,
  Dimensions,Platform,AsyncStorage,BackHandler} from 'react-native';
import styles from './styles';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {
  Container,
   Header,
   Left,
   Body,
   Icon,
   Toast
 } from 'native-base';
import { connect } from 'react-redux'
import Colors from '../../../Config/Colors';
import GPSState from 'react-native-gps-state';
import Languages from '../../../Config/Languages';
import Loadings from '../../../Components/Loadings';
import Footer from '../../../Components/Footer';
const ANCHOR = { x: 0.5, y: 0.5 }
const ANCHOR_DETAIL = { x: 0.5, y: 1 }
const ANCHOR_ANDROID = { x: 0, y: -1.5 }

const CENTER = { x: 0, y: 0 }
const CENTER_DETAIL = { x: 5, y: 3 }
const CENTER_ANDROID = { x: 0, y: -3 }
const backlong=require('../../../assets/Images/backlong.png')
class SubCategory extends React.Component{
    constructor(props){
     super(props)
     this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
     this.state={
       region:'',
       catId:'',
       catName:''
     }
    }
    componentWillMount=async()=>{
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
     let catid=await AsyncStorage.getItem('CatID')
     let catname=await AsyncStorage.getItem('CatName')
      console.log('Category id')
      console.log(catid)
      if (Platform.OS == 'ios') {
        this._getLocationAsync()
      } else {
        GPSState.addListener((status)=>{
          switch(status){
              case GPSState.NOT_DETERMINED:
                  console.log('NOT_DETERMINED')
                  Alert.alert('Alert',
                  Languages.enablelocation,
                  [{
                      text:'Cancel',
                      onPress:()=>console.log('Cancel Pressed'),
                  },{
                      text:'OK',
                      onPress:()=>GPSState.openLocationSettings(),
                  }],
                  { cancelable: true });
              break;
   
              case GPSState.RESTRICTED:
                  console.log('RESTRICTED')
                  Alert.alert('Alert',
                  Languages.enablelocation,
                  [{
                      text:'Cancel',
                      onPress:()=>console.log('Cancel Pressed'),
                  },{
                      text:'OK',
                      onPress:()=>GPSState.openLocationSettings(),
                  }],
                  { cancelable: true });
                  // GPSState.openLocationSettings()
              break;
   
              case GPSState.DENIED:
                  console.log('DENIED')
                  Alert.alert('Alert',
                  Languages.enablelocation,
                  [{
                      text:'Cancel',
                      onPress:()=>console.log('Cancel Pressed'),
                  },{
                      text:'OK',
                      onPress:()=>GPSState.openLocationSettings(),
                  }],
                  { cancelable: true });
              break;
   
              case GPSState.AUTHORIZED_ALWAYS:
                  //TODO do something amazing with you app
                  console.log('AUTHORIZED_ALWAYS')
                  this._getLocationAsync()
              break;
   
              case GPSState.AUTHORIZED_WHENINUSE:
                  console.log('AUTHORIZED_WHENINUSE')
                  //TODO do something amazing with you app
                  this._getLocationAsync()
              break;
          }
      })
      GPSState.requestAuthorization(GPSState.AUTHORIZED_WHENINUSE)   
      }
       this.props.dispatch({type:'Fetch_Search_Category_Request',payload:{url:'/wp-json/search/v1/listing?categories='+catid}})
       this.setState({catId:catid,catName:catname})
    }
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
}
    _getLocationAsync = async () => {
      try {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        if (granted != PermissionsAndroid.RESULTS.GRANTED) {
         Toast.show({type:'danger',text:'Permission denied',position:'top',duration:2000})
        }
    
        navigator.geolocation.getCurrentPosition(
            (position) => {
              console.log('position')
              console.log(position)
              const myPosition = position.coords
    
              this.setState({ myPosition })
              const current = {
                address_lat: myPosition.latitude,
                address_long: myPosition.longitude,
              }
            this.props.dispatch({type:'SET_REGION_MAP',region:current,index:0})
          
            },
            (error) =>  console.log(error),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
          );
        } catch (err) {
          console.warn(err);
        }
          // not use for ListingPro due to Database could not be fetch
          // fetchNearestLocations(true, myPosition.latitude, myPosition.longitude)
       
      }
      onViewPost = (item, index) =>{
        this.props.navigation.navigate('postdetail', { items:item, navigateTo:'subcat' })
      }
      renderCalloutMaker = (item, index) => {
        // const name = typeof item.title != 'undefined' ? item.title.rendered : ''
        // const location = item.job_location
        // const phone = item.phone
        console.log(item)
        return (
          <MapView.Callout onPress={() => this.onViewPost(item, index)}>
            <TouchableOpacity
              activeOpacity={0.9}
              disabled={true}
              style={styles.slideInnerContainer}
              key={`calloutMarker-${index + 1}`}
              onPress={() => this.onViewPost(item, index)}>
              <View style={styles.wrapText}>
                <View
                  style={styles.row}
                 >
                  <Text style={[styles.title]}>
                    {/* {Tools.getDescription(name, 100)} */}
                    {item && item.title && item.title.rendered?item.title.rendered:''}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.text}>
                  {/* {location} */}
                  {item && item.more_options && item.more_options.gAddress?item.more_options.gAddress:''}
                  </Text>
                </View>
                <View
                  activeOpacity={0.9}
                  onPress={this.openPhone}
                  style={styles.row}>
                  <Text style={styles.text}>
                  {/* {phone} */}
                  {item && item.more_options && item.more_options.phone?item.more_options.phone:''}
                  </Text>
                </View>
    
                <Text style={styles.textMore}>{Languages.readMore + '...'}</Text>
              </View>
            </TouchableOpacity>
          </MapView.Callout>
        )
      }
      renderMarkers = () => {
        const {
          listMarkers,
          latitudeDelta,
          longitudeDelta,
          isSearching,
          listMarkersSearch,
          searchcategory
        } = this.props
    
        let listMarkersRender = searchcategory
        if (isSearching) {
          listMarkersRender = listMarkersSearch
        }
    
        const list =
          typeof listMarkersRender != 'undefined' ? (
            listMarkersRender.map((item, index) => {
              let coordinate = {
                latitude: parseFloat(item.more_options.latitude),
                longitude: parseFloat(item.more_options.longitude),
                latitudeDelta,
                longitudeDelta,
              }
              // warn(['markerItemMap', index]);
              return (
                <MapView.Marker
                  key={'marker-' + index}
                  ref={`marker${index}`}
                  anchor={ANCHOR}
                  centerOffset={CENTER}
                  coordinate={coordinate}
                  pinColor={Colors.map.defaultPinColor}
                  style={[styles.marker]}>
               {this.renderCalloutMaker(item, index)}
                </MapView.Marker>
                  
              )
            })
          ) : (
            <View />
          )
        return list
      }
    render(){
      const {initialRegion,navigation,isFetching}=this.props
      if (isFetching) {
        return(
          <Loadings />
        )
      }
        return(
          <Container style={styles.container}>
          <Header style={styles.header}>
           <Left>
            <TouchableOpacity onPress={()=>navigation.goBack()}>
            <Image source={backlong} style={styles.headerImage}></Image>
            </TouchableOpacity>
            </Left>
               <Body><Text style={styles.headerText}>{this.state.catName}</Text></Body>
           </Header>
            <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            ref={(ref) => (this.map = ref)}
            region={initialRegion}
            loadingEnabled
            showsUserLocation
            userLocationAnnotationTitle={"You're Here"}
            loadingIndicatorColor={Colors.map.loading}>
            {this.renderMarkers()}
          </MapView>
          <Footer />
          </Container>
        )
    }
}
const mapStateToProps=(state)=>{
    return{
      isFetching:state.Category.isFetching,
      initialRegion:  state.Map.region,
     latitudeDelta:  state.Map.latitudeDelta,
     longitudeDelta:  state.Map.longitudeDelta,
     isSearching:  state.Map.isSearching,
     indexActive:  state.Map.index,
     posts:state.Post.posts ,
     searchcategory:state.Category.FetchCategories
    }
    }
    
  export default connect(
      mapStateToProps
    )(SubCategory)
      