import React from 'react';
import { View, ImageBackground, Text, AsyncStorage } from 'react-native';
import Styles from './Styles';
import { connect } from 'react-redux';
import Languages from '../../../Config/Languages';
import Loadings from '../../../Components/Loadings';
import ToastHelper from '../../../Config/ToastHelper';
const img = require('../../../assets/icons/tanjatisplash.jpg')


class Splash extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            isNotify:false,
            myuser:null
        }
    }
    componentDidMount=async()=>{
        const {isNotify} = this.state;
        const lang=await AsyncStorage.getItem('lang')
        let user= await AsyncStorage.getItem('User')  
        let notitficationtype = await AsyncStorage.getItem('TYPE')
       await Languages.setLanguage(lang?lang:'en')
       this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        if(user){
            this.props.dispatch({type:'Get_AllIssue_Request', payload:{url:'/api/rest/issues?filter_id=reported'}})
            this.setState({
                myuser:user
            })  
        } 
        
            console.log('notitficationtype splash')
            console.log(notitficationtype)
            if (notitficationtype) {
                console.log('notify')
            } else {
                    // if(user == null){
                    //    console.log('check user')
                    //     setTimeout(()=>{
                    //      this.props.navigation.navigate('main')
                    //       },2000)
        
                    //  } else {
                    //     console.log('check non user')
                    //      setTimeout(()=>{
                    //          this.props.navigation.navigate('user')
                    //         },2000) 
                    //   }      
            }
    }
    componentDidUpdate = async() =>{
            let user= await AsyncStorage.getItem('User')
            let notitficationid = await AsyncStorage.getItem('ID')
            let notitficationtype = await AsyncStorage.getItem('TYPE')
            console.log('notitficationtype spalsh did')
            console.log(notitficationtype)
            if (notitficationtype) {
                this.checkNotification(notitficationid,notitficationtype)
            } else {  
                if(user == null){
                    console.log('check user')
                     setTimeout(()=>{
                      this.props.navigation.navigate('main')
                       },2000)
     
                  } else {
                     console.log('check non user')
                      setTimeout(()=>{
                          this.props.navigation.navigate('user')
                         },2000) 
                   }  
            }
    }

    checkNotification = (notitficationid,notitficationtype) =>{
        const {posts,issue} = this.props
        const {myuser} = this.state;
        if (notitficationtype == 'post') {
            console.log('notitficationid')
            console.log(notitficationid)
                if(posts && posts.length != 0){
            const rest = posts.filter(function(item){
        return (item.id == notitficationid)
      })
      this.props.navigation.navigate('postdetails',{items:rest[0], navigateTo:'home'})
      let keys = ['ID', 'TYPE'];
      AsyncStorage.multiRemove(keys, (err) => {
        // keys k1 & k2 removed, if they existed
        // do most stuff after removal (if you want)
      });
            }  

        }else if(notitficationtype == 'issue'){
            if (myuser != null) {
                console.log('issue')
                console.log(issue)
              if(issue && issue.length != 0){
                    const restissue = this.props.issue.filter(function(item){
                        return (item.id == notitficationid)
                      })
                      console.log('restissue')
                      console.log(restissue)
                     this.props.navigation.navigate('issuedetail',{getissue:restissue[0]})
                     let keys = ['ID', 'TYPE'];
                        AsyncStorage.multiRemove(keys, (err) => {
                            // keys k1 & k2 removed, if they existed
                            // do most stuff after removal (if you want)
                        });
                }
            } else {
                ToastHelper.danger(Languages.hasloggedout)
                this.props.navigation.navigate('main')
                let keys = ['ID', 'TYPE'];
                        AsyncStorage.multiRemove(keys, (err) => {
                            // keys k1 & k2 removed, if they existed
                            // do most stuff after removal (if you want)
                        });
            }
          
        }
    }
   
     render(){
         return(
             <ImageBackground
             style={Styles.container}
             source={img}
             ></ImageBackground>
         )
     }
} 

const mapStateToProps=(state)=>{
    return{
       lang:state.Auth.lang,
       posts:state.Post.posts,
     issue:state.Issue.issues,
     isFetchingPost:state.Post.isFetching,
     isFetchingIssue:state.Issue.isFetching, 
    }
}
export default connect(mapStateToProps)(Splash)