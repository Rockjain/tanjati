initialstate={
    isFetching:false,
    posts:[],
    addWishlist:null,
    removeWishlist:null,
    getWishlist:[],
    searchpost:[],
}

export default (state=initialstate,action)=>{
switch (action.type) {
    case 'Fetch_Post_Request':
     return{...state,isFetching:true}
     case 'Fetch_Post_Success':
     return{...state,isFetching:false,posts:action.payload}
     case 'Fetch_Post_Failure':
     return{...state,isFetching:false}
     case 'Add_Wishlist_Request':
     return{...state,isFetching:true}
     case 'Add_Wishlist_Success':
     return{...state,isFetching:false,addWishlist:action.payload}
     case 'Add_Wishlist_Failure':
     return{...state,isFetching:false}
     case 'Remove_Wishlist_Request':
     return{...state,isFetching:true}
     case 'Remove_Wishlist_Success':
     return{...state,isFetching:false,removeWishlist:action.payload}
     case 'Remove_Wishlist_Failure':
     return{...state,isFetching:false}
     case 'Get_Wishlist_Request':
     return{...state,isFetching:true}
     case 'Get_Wishlist_Success':
     return{...state,isFetching:false,getWishlist:action.payload}
     case 'Get_Wishlist_Failure':
     return{...state,isFetching:false}
     case 'Search_Post_Request':
     return{...state,isFetching:true}
     case 'Search_Post_Success':
     return{...state,isFetching:false,searchpost:action.payload}
     case 'Search_Post_Failure':
     return{...state,isFetching:false}
    default:
      return state;
}
}