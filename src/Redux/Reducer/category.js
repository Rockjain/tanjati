initialstate={
    isFetching:false,
    Categories:[],
    FetchCategories:[]
}

export default (state=initialstate,action)=>{
switch (action.type) {
    case 'Fetch_Category_Request':
     return{...state,isFetching:true}
     case 'Fetch_Categories_Success':
     return{...state,isFetching:false,Categories:action.payload}
     case 'Fetch_Categories_Failure':
     return{...state,isFetching:false}
     case 'Fetch_Search_Category_Request':
     return{...state,isFetching:true}
     case 'Fetch_Search_Categories_Success':
     return{...state,isFetching:false,FetchCategories:action.payload}
     case 'Fetch_Search_Categories_Failure':
     return{...state,isFetching:false}
    default:
      return state;
}
}