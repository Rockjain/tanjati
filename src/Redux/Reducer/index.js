import { combineReducers } from 'redux';
import Category from './category';
import Auth from './auth';
import Post from './post';
import Map from './map';
import Issue from './issue';
export default combineReducers({
Category,
Auth,
Post,
Map,
Issue
})