initialstate={
    isFetching:false,
    Register:null,
    VCode:'',
    StatusUpdate:null,
    User:null,
    UserInfo:null,
    lang:'en',
    usertoken:null,
    errorres:'',
    regerror:'',
}

export default (state=initialstate,action)=>{
switch (action.type) {
    case 'User_Register_Request':
     return{...state,isFetching:true}
     case 'User_Register_Success':
     return{...state,isFetching:false,Register:action.payload}
     case 'User_Register_Failure':
     return{...state,isFetching:false,regerror:action.reg_error}
     case 'Send_Code_Request':
     return{...state,isFetching:true}
     case 'Send_Code_Success':
     return{...state,isFetching:false,VCode:action.payload}
     case 'Update_Status_Request':
     return{...state,isFetching:true}
     case 'Update_Status_Success':
     return{...state,isFetching:false,StatusUpdate:action.payload,UserInfo:action.userinfo}
     case 'Update_Status_Login_Request':
     return{...state,isFetching:true}
     case 'Update_Status_Login_Success':
     return{...state,isFetching:false,User:action.payload}
     case 'Login_Request':
     return{...state,isFetching:true}
     case 'Login_Success':
     return{...state,isFetching:false,User:action.payload,UserInfo:action.userinfo,usertoken:action.usertokens}
     case 'Login_Failure':
     return{...state,isFetching:false,errorres:action.error}
     case 'Language_Change_Success':
     return{...state,lang:action.payload}
     case 'Fetch_User_Request':
     return{...state,isFetching:true}
     case 'Fetch_User_Success':
     return{...state,isFetching:false,UserInfo:action.userinfo}
     case 'Fetch_User_Failure':
     return{...state,isFetching:false}
     case 'logout_success':
     return{...state,isFetching:false,UserInfo:null,User:null}
    default:
      return state;
}
}