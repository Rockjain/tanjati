initialstate={
    isFetching:false,
    products:[],
    addissuesres:null,
    getallissue:null,
    issues:[],
    issueerror:null,
    file:[],
    issuedata:[]
}

export default (state=initialstate,action)=>{
switch (action.type) {
    case 'Fetch_Product_Request':
     return{...state,isFetching:true}
     case 'Fetch_Product_Success':
     return{...state,isFetching:false,products:action.payload}
     case 'Fetch_Product_Failure':
     return{...state,isFetching:false}
     case 'Add_Issues_Request':
        return{...state,isFetching:true}
        case 'Add_Issues_Success':
        return{...state,isFetching:false,addissuesres:action.payload}
        case 'Add_Issues_Failure':
        return{...state,isFetching:false,issueerror:action.error}
        case 'Get_AllIssue_Request':
        return{...state,isFetching:true}
        case 'Get_AllIssue_Success':
        return{...state,isFetching:false,issues:action.payload}
        case 'Get_AllIssue_Failure':
        return{...state,isFetching:false}
        case 'Get_Issuefile_Request':
        return{...state,isFetching:true}
        case 'Get_Issuefile_Success':
        return{...state,isFetching:false,file:action.filedata,issuedata:action.payload}
        case 'Get_Issuefile_Failure':
        return{...state,isFetching:false}
    default:
      return state;
}
}