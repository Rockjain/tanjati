import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE = 0
const LONGITUDE = 0
const LATITUDE_DELTA = 1.832
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

initialstate={
    isFetching:false,
    region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      latitudeDelta: LATITUDE_DELTA - 0.02,
      longitudeDelta: LONGITUDE_DELTA - 0.02,
      markers: [],
      index: 0,
      searchMarkers: [],
      myPosition: '',
      isFetching: false,
      isSearching: false,
}

export default (state=initialstate,action)=>{
    const { region } = action
switch (action.type) {
  case 'SET_REGION_MAP_REQUEST':
     return{...state,isFetching:true}
     case 'SET_REGION_MAP_SUCCESS':
     return{...state,isFetching:false, region: {
        ...state.region,
        latitude: region.address_lat || 0,
        longitude: region.address_long || 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      index: action.index,}
    default:
      return state;
}
}