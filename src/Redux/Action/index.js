import axios from 'axios';
import Constants from '../../Config/Constants';
import { AsyncStorage } from 'react-native';


export const _post =(url,data)=>{
    return axios({
        method:'POST',
        url: Constants.Base_url + url,
        data
    });
}

export const _get = (url) =>{
	return  axios({
		method: 'GET',
		url: Constants.Base_url + url,
	});
}

export const wp_post =(url,data)=>{
    return axios({
        method:'POST',
        url: Constants.Base_WP_url + url,
        data
    });
}

export const wp_get = (url) =>{
    return  axios({
        method: 'GET',
        url: Constants.Base_WP_url + url,
    });
}

export const wp_get_search = (url) =>{
    return  axios({
        method: 'GET',
        url: Constants.Base_WP_url_search + url,
    });
}
export const mantis_post =(url,data)=>{
        return axios({
            method:'POST',
            url: Constants.Base_url_Mantis + url,
            headers:{'Authorization': Constants.Mantis_Token,'Content-Type': 'application/json'},
            data
        });
    }
export const mantis_get = (url) =>{
        return  axios({
            method: 'GET',
            url: Constants.Base_url_Mantis + url,
            headers:{'Authorization': Constants.Mantis_Token._55},
        });
    }

    export const mantis_post_user =async(url,data)=>{
        const Mantis_Token_User= await AsyncStorage.getItem('MUSERTOKEN');
        const tokenjson = JSON.parse(Mantis_Token_User)
        console.log(tokenjson.token)
        const token = tokenjson.token
        return axios({
            method:'POST',
            url: Constants.Base_url_Mantis + url,
            headers:{'Authorization': token,'Content-Type': 'application/json'},
            data
        });
    }

    export const mantis_get_user = async(url) =>{
        const Mantis_Token_User= await AsyncStorage.getItem('MUSERTOKEN');
        const tokenjson = JSON.parse(Mantis_Token_User)
        console.log(tokenjson.token)
        const token = tokenjson.token
        return  axios({
            method: 'GET',
            url: Constants.Base_url_Mantis + url,
            headers:{'Authorization': token},
        });
    }