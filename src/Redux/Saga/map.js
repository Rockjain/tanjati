import {call, put, takeEvery} from 'redux-saga/effects';
import Map from '../Api/map';

function* setRegion(action){

    try {
            yield put({
                type:'SET_REGION_MAP_SUCCESS',
                region: action.region
            })
     } catch (error) {
        console.log(error)
    }

}



export default function* mapSaga() {
    yield takeEvery('SET_REGION_MAP_REQUEST', setRegion);
  }