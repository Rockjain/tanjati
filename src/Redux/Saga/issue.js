import {call, put, takeEvery} from 'redux-saga/effects';
import Issue from '../Api/issue';
import ToastHelper from '../../Config/ToastHelper';
import Languages from '../../Config/Languages';

function* fetchProduct(action){

    try {
        const response= yield call(Issue.fetchProduct,action.payload.url)
        console.log(response)
            yield put({
                type:'Fetch_Product_Success',
                payload: response
            })
     } catch (error) {
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Fetch_Product_Failure',
            })
        } else {
        ToastHelper.danger(Languages.somethingwrong)
        yield put({
            type:'Fetch_Product_Failure',
        })
    }
        console.log(error)
    }

}

function* addIssues(action){

    try {
        const response= yield call(Issue.addIssues,action.payload.url,action.payload.data)
        console.log('add issue response')
        console.log(response)
        console.log('content')
            console.log(action.payload.content)
         if(response){
            const responseAttachement = yield call(Issue.addIssueAttach,'/api/rest/issues/'+response.issue.id+'/files',{
                "files": [
                    {
                        "name":  action.payload.name,
                        "content": action.payload.content
                    }  	
                ]
              })
            yield put({
                type:'Add_Issues_Success',
                payload: responseAttachement
            })
            console.log('response attachement')      
            console.log(responseAttachement)
            action.payload.navigation.navigate('issue')
            if(responseAttachement == ''){
                ToastHelper.success(Languages.issueadd);
                console.log('getresponse')
                action.payload.navigation.navigate('profile')
                const getresponse= yield call(Issue.getAllIssue,'/api/rest/issues?filter_id=reported')
                console.log(getresponse.issues[0].id)
                const getNotification = yield call(Issue.getIssueNotification,'/sendnotification?user_id='+action.payload.userid+'&type=issue&tid='+getresponse.issues[0].id)
                console.log(getNotification)
            }
         } else {
            ToastHelper.danger('Something went wrong!');  
         }   
     } catch (error) {
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Add_Issues_Failure',
                error:'no connection'
            })
        } else {
            ToastHelper.danger(error.response.data.message);
            yield put({
                type:'Add_Issues_Failure',
                error:error.response.data.message
            })
    }
           
    }

}

function* getAllIssue(action){

    try {
        const response= yield call(Issue.getAllIssue,action.payload.url)
        console.log(response)
            yield put({
                type:'Get_AllIssue_Success',
                payload: response.issues
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Get_AllIssue_Failure',
            })
        } else {
            ToastHelper.danger(Languages.somethingwrong);
            yield put({
                type:'Get_AllIssue_Failure',
            })
    }
    }

}

function* getIssueFile(action){

    try {
        const response= yield call(Issue.getIssueFile,action.payload.url)
        console.log('file response')
        console.log(response)
        if (response) {
            const issueresponse= yield call(Issue.getIssueData,'/api/rest/issues/'+action.payload.issueid)
            console.log('issue response')
            console.log(issueresponse)
            yield put({
                type:'Get_Issuefile_Success',
                payload: issueresponse.issues,
                filedata:response.files
            })   
        }
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Get_Issuefile_Failure',
            })
        } else {
            ToastHelper.danger(Languages.somethingwrong);
            yield put({
                type:'Get_Issuefile_Failure',
            })
    }
    }

}

export default function* issueSaga() {
    yield takeEvery('Fetch_Product_Request', fetchProduct);
    yield takeEvery('Add_Issues_Request', addIssues);
    yield takeEvery('Get_AllIssue_Request', getAllIssue);
    yield takeEvery('Get_Issuefile_Request', getIssueFile);
  }