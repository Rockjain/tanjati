import {call, put, takeEvery} from 'redux-saga/effects';
import Category from '../Api/category';
import ToastHelper from '../../Config/ToastHelper';
import Languages from '../../Config/Languages';

function* fetchCategory(action){

    try {
        const response= yield call(Category.fetchCategory,action.payload.url)
        console.log(response)
            yield put({
                type:'Fetch_Categories_Success',
                payload: response
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Fetch_Categories_Failure',
            })
        } else {
        ToastHelper.danger(Languages.somethingwrong)
        yield put({
            type:'Fetch_Categories_Failure',
        })
    }
    }

}

function* fetchSearchCategory(action){

    try {
        const response= yield call(Category.fetchSearchCategory,action.payload.url)
        console.log(response)
            yield put({
                type:'Fetch_Search_Categories_Success',
                payload: response
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Fetch_Search_Categories_Failure',
            })
        } else {
        ToastHelper.danger(Languages.somethingwrong)
        yield put({
            type:'Fetch_Search_Categories_Failure',
        })
    }
    }

}

export default function* categorySaga() {
    yield takeEvery('Fetch_Category_Request', fetchCategory);
    yield takeEvery('Fetch_Search_Category_Request', fetchSearchCategory);
  }