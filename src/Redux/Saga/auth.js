import {call, put, takeEvery} from 'redux-saga/effects';
import Auth from '../Api/auth';
import ToastHelper from '../../Config/ToastHelper';
import { AsyncStorage } from 'react-native';
import Languages from '../../Config/Languages';

function* userRegister(action){

    try {
        const response= yield call(Auth.getNonce,'/api/get_nonce/?controller=mstore_user&method=register')
        if (response.status != 'undefined' && response.status == 'ok') {
        const responses= yield call(Auth.doRegister,'/api/mstore_user/register/?insecure=cool&nonce='
                        +response.nonce+'&email='+action.payload.email+'&username='+action.payload.email+'&display_name='
                        +action.payload.name+'&user_nicename='+action.payload.name+'&first_name='
                        +action.payload.name+'&user_pass='+action.payload.password) 
                        console.log('response')
                        console.log(response)
                        console.log('responses')
                        console.log(responses)
        if (responses.status != 'undefined' && responses.status == 'ok') {
            console.log(action.payload.email) 
            console.log(action.payload.password)
            console.log(action.payload.name)
            console.log(action.payload.email)
            const mantis_user= yield call(Auth.doMantisRegister,'/api/rest/users/',
            { 
            "username": action.payload.email,
            "password": action.payload.password,
            "real_name": action.payload.name,
            "email": action.payload.email,
            "access_level": { "name": "reporter" },
            "enabled": true,
           "protected": false,
           "fcmtoken":action.payload.firebasetoken,
           "deviceios":action.payload.deviceplatform           
        })
            console.log('mantis_user')
            console.log(mantis_user)
            if (mantis_user.user) {
                    const mentisresponse = yield call(Auth.fetchMentisToken,'/api/rest/users/token',    { 
                        "userid": mantis_user.user.id,
                        "usertokenname": "token_"+mantis_user.user.id
                     })
                    if(mentisresponse){
                    console.log('mentisresponse')
                    console.log(mentisresponse.token)    
                    let mentis = {
                        'token': mentisresponse.token,
                        'mentis_id':mantis_user.user.id
                    }
                const mantis_wp_reg= yield call(Auth.setMantisWp,'/setMantisUserId?user_id='+responses.user_id+'&muser_id='+JSON.stringify(mentis))
                console.log('mantis_wp_reg')
            console.log(mantis_wp_reg)
                if (mantis_wp_reg.status=='success') {
                        const phoneresponse= yield call(Auth.setPhoneNumber,'/setphone?user_id='+responses.user_id+'&phone='+action.payload.phone)
                           console.log('phoneresponse')
                           console.log(phoneresponse)
                        if (phoneresponse.status=='success') {
                            ToastHelper.success('Registration successfull');  
                            yield put({
                                type:'User_Register_Success',
                                payload: responses
                            }) 
                            action.payload.navigation.navigate('verification',{
                                phone:action.payload.phone,
                                email:action.payload.email,
                                password:action.payload.password})
                        }  
                } else {
                    ToastHelper.danger(Languages.somethingwrong); 
                    yield put({
                        type:'User_Register_Failure',
                        reg_error:Languages.somethingwrong
                    })
                }
               }
            } else {
                ToastHelper.danger(Languages.somethingwrong);  
                yield put({
                    type:'User_Register_Failure',
                    reg_error:Languages.somethingwrong
                })
            }
          
        }else{
            ToastHelper.danger(Languages.somethingwrong);   
            yield put({
                type:'User_Register_Failure',
                reg_error:Languages.somethingwrong
            })
        }

        } else {
            ToastHelper.danger(Languages.somethingwrong);
            yield put({
                type:'User_Register_Failure',
                reg_error:Languages.somethingwrong
            })
        }
           
     } catch (error) {
        //console.log(error)
        if(error.message == 'Network Error'){
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'User_Register_Failure',
                reg_error:'no internet'
            })
        } else{
            console.log(error.response)
            console.log('errorrrr')
           ToastHelper.danger(error.response.data.error);
       yield put({
           type:'User_Register_Failure',
           reg_error:error.response.data.error
       })
        } 
    }

}

function* sendVCode(action){

    try {
        const response= yield call(Auth.sendCode,'/sendCode?to='+action.payload.phone)
       
        if (response.status=='success') {
            ToastHelper.success(Languages.codesuccess);  
                yield put({
                    type:'Send_Code_Success',
                    payload: response.code
                }) 
                if (!action.payload.isVerified) {
                    action.payload.navigation.navigate('verification',{
                        phone:action.payload.phone,
                        isVerified:false
                    })
                }
            }
         else {
            //ToastHelper.danger(response.error);  
        }
           
     } catch (error) {
         console.log(error)
        //ToastHelper.danger(error.message);
    }

}

function* fetchUser(action){

    try {
       
         const responses= yield call(Auth.fetchUserInfo,'/getUser?user_id='+action.payload.userid) 
            if (responses.status=='success') {
                AsyncStorage.setItem('User', JSON.stringify(responses.data))  
                yield put({
                    type:'Fetch_User_Success',
                    userinfo:responses.data
                }) 
            } else {
                ToastHelper.danger(Languages.somethingwrong);
                yield put({
                    type:'Fetch_User_Failure',
                })     
            }
            
           
     } catch (error) {
         console.log(error)
        // ToastHelper.danger(error.message);
    }

}

function* updateVStatus(action){

    try {
        const response= yield call(Auth.updateStatus,'/userMobileStatus?user_id='+action.payload.userid+'&status=1')
       
        if (response.status=='success') {
            const responses= yield call(Auth.fetchUserInfo,'/getUser?user_id='+action.payload.userid) 
            if (responses.status=='success') {
                ToastHelper.success('Number verified!');
                AsyncStorage.setItem('User',JSON.stringify(responses.data))  
                yield put({
                    type:'Update_Status_Success',
                    payload: response,
                    userinfo:responses.data
                })
                action.payload.navigation.navigate('profile')
            } else {
                ToastHelper.danger(Languages.somethingwrong);    
            }
            }
         else {
        }
           
     } catch (error) {
         console.log(error)
       // ToastHelper.danger(error.message);
    }

}

function* updateVStatusLogin(action){

    try {
        const statusresponse= yield call(Auth.updateStatus,'/userMobileStatus?user_id='+action.payload.userid+'&status=1')
       
        if (statusresponse.status=='success') {
                yield put({
                    type:'Update_Status_Login_Success',
                    payload: statusresponse
                }) 
                const response= yield call(Auth.fetchToken,'/wp-json/jwt-auth/v1/token',action.payload.data)
       
                if (response.token != undefined) {
                const loginresponse= yield call(Auth.doLogin,'/api/mstore_user/generate_auth_cookie/?second=120960000000&username='
                                  +action.payload.data.username+'&password='+action.payload.data.password+'&nonce=inspireui&insecure=cool')
                      if (loginresponse.status=='ok') {
                        const responses= yield call(Auth.fetchUserInfo,'/getUser?user_id='+loginresponse.user.id) 
                        if (responses.status=='success') {
                            const getToken= yield call(Auth.getToken,'/getMantisUserId?user_id='+loginresponse.user.id);
                            if (getToken) {
                                const registerNotification = yield call(Auth.registerNotification,'/settoken?user_id='+responses.data.id+'&token='+action.payload.firebasetoken);
                               if (registerNotification.status == 'success') {
                                console.log('getToken')
                                console.log(getToken.muser_id)
                                ToastHelper.success(Languages.loginSuccess);  
                                yield put({
                                    type:'Login_Success',
                                    payload: loginresponse,
                                    userinfo:responses.data,
                                    usertokens:getToken
                                }) 
                                AsyncStorage.setItem('User',JSON.stringify(responses.data))
                                AsyncStorage.setItem('MUSERTOKEN',getToken.muser_id)
                                action.payload.navigation.navigate('user')       
                                } else {
                                console.log('registerNotification')    
                                ToastHelper.danger(Languages.invalidlogin);   
                            }    
                            } else {
                                console.log('getToken')
                                ToastHelper.danger(Languages.invalidlogin);   
                            } 
                        } else {
                            ToastHelper.danger(Languages.invalidlogin);    
                        }
                      } else {
                        ToastHelper.danger(Languages.invalidlogin);      
                      }            
                    }
        
                 else {
                    ToastHelper.danger(Languages.invalidlogin);   
                }
            }

         else {
          //  ToastHelper.danger(response.error);  
        }
           
     } catch (error) {
        console.log(error)
       // ToastHelper.danger(error.message);
    }

}

function* doLogin(action){

    try {
        const response= yield call(Auth.fetchToken,'/wp-json/jwt-auth/v1/token',action.payload.data)
       
        if (response.token != undefined) {
        const loginresponse= yield call(Auth.doLogin,'/api/mstore_user/generate_auth_cookie/?second=120960000000&username='
                          +action.payload.data.username+'&password='+action.payload.data.password+'&nonce=inspireui&insecure=cool')
              if (loginresponse.status=='ok') {
                  console.log('loginresponse')
                  console.log(loginresponse)
                const responses= yield call(Auth.fetchUserInfo,'/getUser?user_id='+loginresponse.user.id) 
                console.log('myresponses')
                console.log(responses)
                if (responses.status=='success') {
                    console.log('responses')
                    console.log(responses)
                    const getToken= yield call(Auth.getToken,'/getMantisUserId?user_id='+loginresponse.user.id);
                    if (getToken) {
                        const registerNotification = yield call(Auth.registerNotification,'/settoken?user_id='+responses.data.id+'&token='+action.payload.firebasetoken);
                       if (registerNotification.status == 'success') {
                        console.log('getToken')
                        console.log(getToken.muser_id)
                        ToastHelper.success(Languages.loginSuccess);  
                        yield put({
                            type:'Login_Success',
                            payload: loginresponse,
                            userinfo:responses.data,
                            usertokens:getToken
                        }) 
                        AsyncStorage.setItem('User',JSON.stringify(responses.data))
                        AsyncStorage.setItem('MUSERTOKEN',getToken.muser_id)
                        action.payload.navigation.navigate('user')       
                        } else {
                        console.log('registerNotification')  
                        console.log(registerNotification)  
                        ToastHelper.danger(Languages.invalidlogin); 
                        yield put({
                            type:'Login_Failure',
                            error:Languages.invalidlogin
                        })   
                    }    
                    } else {
                        console.log('getToken')
                        console.log(getToken)
                        ToastHelper.danger(Languages.invalidlogin);
                        yield put({
                            type:'Login_Failure',
                            error:Languages.invalidlogin
                        })   
                    }    
                 
                } else {
                    console.log('responses')
                    console.log(responses)
                    ToastHelper.danger(Languages.invalidlogin);
                    yield put({
                        type:'Login_Failure',
                        error:Languages.invalidlogin
                    })    
                }
              } else {
                console.log('loginresponse')
                console.log(loginresponse)  
                ToastHelper.danger(Languages.invalidlogin);
                yield put({
                    type:'Login_Failure',
                    error:Languages.invalidlogin
                })       
              }            
            }

         else {
            console.log('responsetoken') 
            console.log(responsetoken)
            ToastHelper.danger(Languages.invalidlogin); 
            yield put({
                type:'Login_Failure',
                error:Languages.invalidlogin
            })   
        }
           
     } catch (error) {
         console.log(error.response)
         if(error.message == 'Network Error'){
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Login_Failure',
                error:'no internet'
            })
         } else{
             console.log('errorrrr')
             ToastHelper.danger(Languages.invalidlogin);
        yield put({
            type:'Login_Failure',
            error:Languages.invalidlogin
        })
         } 
    }

}

export default function* authSaga() {
    yield takeEvery('User_Register_Request', userRegister);
    yield takeEvery('Send_Code_Request', sendVCode);
    yield takeEvery('Update_Status_Request', updateVStatus);
    yield takeEvery('Update_Status_Login_Request', updateVStatusLogin);
    yield takeEvery('Login_Request', doLogin);
    yield takeEvery('Fetch_User_Request', fetchUser);
  }