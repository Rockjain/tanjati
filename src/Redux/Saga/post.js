import {call, put, takeEvery} from 'redux-saga/effects';
import Post from '../Api/post';
import Languages from '../../Config/Languages';
import ToastHelper from '../../Config/ToastHelper';

function* fetchPost(action){

    try {
        const response= yield call(Post.fetchPost,action.payload.url)
        console.log(response)
            yield put({
                type:'Fetch_Post_Success',
                payload: response
            })
     } catch (error) {
        console.log(error.message)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Fetch_Post_Failure',
            })
        } else {
            yield put({
                type:'Fetch_Post_Failure',
            })
        }
    }

}

function* addWishlist(action){

    try {
        const response= yield call(Post.addWishlist,action.payload.url)
        console.log(response)
            yield put({
                type:'Add_Wishlist_Success',
                payload: response
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Add_Wishlist_Failure',
            })
        } else {
        ToastHelper.danger(Languages.somethingwrong)
        yield put({
            type:'Add_Wishlist_Failure',
        })
    }
    }

}

function* removeWishlist(action){

    try {
        const response= yield call(Post.removeWishlist,action.payload.url)
        console.log(response)
            yield put({
                type:'Remove_Wishlist_Success',
                payload: response
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Remove_Wishlist_Failure',
            })
        } else {
        ToastHelper.danger(Languages.somethingwrong)
        yield put({
            type:'Remove_Wishlist_Failure',
        })
    }
    }

}

function* getWishlist(action){

    try {
        const response= yield call(Post.addWishlist,action.payload.url)
       // console.log(response)
            yield put({
                type:'Get_Wishlist_Success',
                payload: response.data
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Get_Wishlist_Failure',
            })
        } else {
            yield put({
                type:'Get_Wishlist_Failure',
            })
        }
    }

}

function* searchPost(action){

    try {
        const response= yield call(Post.searchPost,action.payload.url)
       // console.log(response)
            yield put({
                type:'Search_Post_Success',
                payload: response
            })
     } catch (error) {
        console.log(error)
        if (error.message == 'Network Error') {
            ToastHelper.danger(Languages.nointernet)
            yield put({
                type:'Search_Post_Failure',
            })
        } else {
        ToastHelper.danger(Languages.somethingwrong)
        yield put({
            type:'Search_Post_Failure',
        })
    }
    }

}

export default function* postSaga() {
    yield takeEvery('Fetch_Post_Request', fetchPost);
    yield takeEvery('Add_Wishlist_Request', addWishlist);
    yield takeEvery('Remove_Wishlist_Request', removeWishlist);
    yield takeEvery('Get_Wishlist_Request', getWishlist);
    yield takeEvery('Search_Post_Request', searchPost);
  }