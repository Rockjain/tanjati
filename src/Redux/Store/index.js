import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import Reducer from '../Reducer';
import { createStore, applyMiddleware } from 'redux';
import categorySaga from '../Saga/category';
import authSaga from '../Saga/auth';
import postSaga from '../Saga/post';
import mapSaga from '../Saga/map';
import issueSaga from '../Saga/issue';

const sagaMiddleware = createSagaMiddleware();

const store=createStore(Reducer,applyMiddleware(sagaMiddleware,logger))

sagaMiddleware.run(categorySaga)
sagaMiddleware.run(authSaga)
sagaMiddleware.run(postSaga)
sagaMiddleware.run(mapSaga)
sagaMiddleware.run(issueSaga)

export default store;