import { _get,_post,wp_post,wp_get} from '../Action';
import Constants from '../../Config/Constants';
import axios from 'axios';

export default class Category {


       static fetchCategory= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static fetchSearchCategory= async(url)=>{
        try {
            const response = await _get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }


}