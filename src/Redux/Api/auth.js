import { _get,_post,wp_post,wp_get,mantis_post,mantis_get} from '../Action';
import Constants from '../../Config/Constants';
import axios from 'axios';

export default class Auth {

    static getNonce= async(url)=>{
        try {
            const response = await _get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static doRegister= async(url)=>{
        try {
            const response = await _get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static doMantisRegister= async(url,data)=>{
        try {
            const response = await mantis_post (url,data);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static setMantisWp= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static setPhoneNumber= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static registerNotification= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static sendCode= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       
       static updateStatus= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static fetchUserInfo= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static fetchToken= async(url,data)=>{
        try {
            const response = await _post (url,data);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static doLogin= async(url)=>{
        try {
            const response = await _get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static fetchMentisToken= async(url,data)=>{
        try {
            const response = await mantis_post (url,data);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static getToken= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

}