import { _get,_post,wp_post,wp_get, wp_get_search} from '../Action';
import Constants from '../../Config/Constants';
import axios from 'axios';

export default class Category {


       static fetchPost= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static addWishlist= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static removeWishlist= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static getWishlist= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static searchPost= async(url)=>{
        try {
            const response = await wp_get_search (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

}