import { wp_post,wp_get,mantis_get,mantis_post,mantis_get_user, mantis_post_user} from '../Action';
import Constants from '../../Config/Constants';
import axios from 'axios';

export default class Issues {


       static fetchProduct= async(url)=>{
        try {
            const response = await mantis_get_user (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static addIssues= async(url,data)=>{
        try {
            const response = await mantis_post_user (url,data);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static addIssueAttach= async(url,data)=>{
        try {
            const response = await mantis_post_user (url,data);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static getAllIssue= async(url)=>{
        try {
            const response = await mantis_get_user (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static getIssueFile= async(url)=>{
        try {
            const response = await mantis_get_user (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }

       static getIssueData= async(url)=>{
        try {
            const response = await mantis_get_user (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }
       static getIssueNotification= async(url)=>{
        try {
            const response = await wp_get (url);
            return response.data;
        } catch (error) {
            throw error
        }
       }


}