/** @format */

import React from 'react'
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { connect } from 'react-redux'
import * as Animatable from 'react-native-animatable'
import Colors from '../../Config/Colors';
var createReactClass = require('create-react-class')
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';
import { Toast } from 'native-base';
const TabBar = createReactClass({
 
  onPress(key) {
    if (key=='Issue') {
      if (this.props.user) {
      if (this.props.user.status=='') {
        Toast.show({text:'Please verify your number!',position:'top',type:'warning',duration:2000})
      }else{
        this.props.navigation.navigate(key)
      }
      }else{
        this.props.navigation.navigate(key)
      }
    } else {
      this.props.navigation.navigate(key)
    }
   
  },

  render() {
    const {
      navigation,
      renderIcon,
      activeTintColor,
      inactiveTintColor,
      jumpToIndex,
      colorConfig,
    } = this.props

    const { routes } = navigation.state

    return (
      <View
        style={[
          styles.tabbar,
          {
            backgroundColor: Colors.main
          }
        ]}>
        {routes &&
          routes.map((route, index) => {
            const focused = index === navigation.state.index
            let tintColor = focused ? activeTintColor : inactiveTintColor

            return (
              <TouchableWithoutFeedback
                key={route.key}
                style={styles.tab}
                onPress={this.onPress.bind(this, route.key)}>
                <Animatable.View ref={'tabItem' + index} style={styles.tab}>
                  {renderIcon({
                    route,
                    index,
                    focused,
                    tintColor,
                  })}
                </Animatable.View>
              </TouchableWithoutFeedback>
            )
          })}
      </View>
    )
  },
})

const mapStateToProps = (state) => ({
  user: state.Auth.UserInfo,
})
export default connect(mapStateToProps)(TabBar)
