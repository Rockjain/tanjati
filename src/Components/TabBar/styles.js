import React from 'react'
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native'

export default StyleSheet.create({
    tabbar: {
      height: 49,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderTopWidth: 0.5,
      borderTopColor: '#eee',
    },
    tab: {
      alignSelf: 'stretch',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  })