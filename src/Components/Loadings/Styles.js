import {StyleSheet} from 'react-native';
import Colors from '../../Config/Colors';

export default StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center'
    },
    loadingview:{
        backgroundColor: Colors.main,
        flexDirection: 'row',
        alignSelf: 'center',
        padding: 10,
    },
    txt:{
      marginLeft: 10,
      marginTop: 5
    },
   
})