import React from 'react';
import {View,Text,ActivityIndicator,StatusBar,Platform,ImageBackground} from 'react-native';
import styles from './Styles';
import banner from '../../assets/Images/banner/banner3.jpg';
import Colors from '../../Config/Colors';
export default class Loading extends React.Component{
    render(){
        return(
            <ImageBackground source={banner} style={ styles.container }>
                  <ActivityIndicator
                  size= {Platform.OS == 'ios'?'large':30}
                  color='#fff'
                  ></ActivityIndicator>
               <StatusBar hidden={true} />
            </ImageBackground>
        )
    }
}