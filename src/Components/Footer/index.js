import React from 'react';
import { View,ScrollView, ImageBackground, TextInput,Animated,
    Text,
    Image, FlatList, TouchableOpacity, SafeAreaView,AsyncStorage,Platform} from 'react-native';
const menu = require('../../assets/Images/menu.png')
const newplus = require('../../assets/Images/newplus.png');
import Languages from '../../Config/Languages';
import { connect } from 'react-redux';
const iconburger = require('../../assets/Images/icon-burger.png');
import ToastHelper from '../../Config/ToastHelper';
import { withNavigation } from 'react-navigation';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import Styles from './Styles';
import Colors from '../../Config/Colors';

class Footer extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            myuser:null,
            isToogle:false
        }
    }
    componentWillMount = async() =>{
        let userinfo = await AsyncStorage.getItem('User');
        let getuser = JSON.parse(userinfo)
        if (getuser){
          this.props.dispatch({type:'Fetch_User_Request',payload:{userid:getuser.id}}) 
        }
        this.setState({myuser:JSON.parse(userinfo)})
    }

    checkVerify = () =>{
      const {navigation,Users} = this.props;
      if (Users && Users.status) {
        if (Users.status==''|| Users.status==0) {
          ToastHelper.danger(Languages.verifymobile)
        } else {
          navigation.navigate('Issue')
        }
      } else {
        ToastHelper.danger(Languages.nointernet)
      }
    }
    checkBottomUser = () =>{
        const {myuser} = this.state;
        const {navigation,Users} = this.props;
        console.log('Users')
        console.log(Users)
        if (myuser == null || myuser == undefined) {
            return(
              <View style={Styles.bottomdarkview}>
              <Text style={Styles.footertxt}>Reclamation</Text>
              <Text style={[Styles.footertxt,{marginLeft:10,marginRight:15}]}>استصلاح</Text>
      </View>
            )
        } else {
          return(
            <TouchableOpacity style={Styles.bottomdarkview} onPress={()=>this.checkVerify()}>
            <Text style={Styles.footertxt}>Reclamation</Text>
            <Image
            source={newplus}
            style={{height:25,width:25,marginHorizontal:18}}
            ></Image>
            <Text style={[Styles.footertxt,{marginRight:33}]}>استصلاح</Text>
    </TouchableOpacity>
            )
        }
      }
      getSelect  = (key) =>{
        this.props.navigation.navigate(key)
      }
    render(){
      const {navigation} = this.props;
      const {isToogle} = this.state;
        return(
            <View style={Styles.footerrowview}>
            <View>
            <View style={{borderBottomColor:Colors.lightGrey,borderBottomWidth:1}}></View>
            <View style={Styles.menuview}>
              <TouchableOpacity onPress={()=>navigation.toggleDrawer()}>
              <Image
              source={iconburger}
              style={{ width:26.8,
                height:17.5, marginTop:16
              }}
              ></Image>
              </TouchableOpacity>
              </View>
            </View>
            {this.checkBottomUser()}
      </View>
        )
    }
}

const mapStateToProps=(state)=>{
  return{
    Users:state.Auth.UserInfo,
  }
  }
  export default connect(
    mapStateToProps
  )(withNavigation(Footer))