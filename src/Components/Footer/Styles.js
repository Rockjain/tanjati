import { StyleSheet, Dimensions,Platform } from 'react-native';
import Colors from '../../Config/Colors';
const {width,height}=Dimensions.get('window')

export default StyleSheet.create({
    footerrowview:{
        flexDirection:'row',
        height:50,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor:Colors.main
    },
    menutxt:{
        color:Colors.bottomblue,
        fontSize:12,
        ...Platform.select({
            ios: {
                marginLeft:12,
            },
            android: {
                marginLeft:0,
            },
          }),
       
    },
    menuview:{
        marginHorizontal:10
    },
    bottomdarkview:{
        flexDirection:'row',
        backgroundColor:Colors.bottomblue,
        justifyContent:'center',
        alignItems:'center',
        width:Dimensions.get('window').width-30,
        height:'100%'
    },
    footertxt:{
        color:Colors.main,
        fontWeight:'bold'
    }
})