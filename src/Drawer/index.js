import React from 'react';
import { View, Text, TouchableOpacity,AsyncStorage } from 'react-native';
import Styles from './Styles';
import Languages from '../Config/Languages';
import { connect } from 'react-redux';
import ToastHelper from '../Config/ToastHelper';
import Loadings from '../Components/Loadings';
 class Drawer extends React.Component {
constructor(props){
    super(props)
   this.state={
        user:'',
        routeLogin: [
            { label: Languages.home, key: 'Home' },
            { label: Languages.category, key: 'Category' },
            { label: Languages.Map, key: 'Map' },
            { label: Languages.profile, key: 'Profile' },
            { label: Languages.setting, key: 'setting' },
            { label: Languages.contact, key: 'contact' },
            { label: Languages.login, key: 'login' },
        ],
        routeLogout: [
            { label: Languages.home, key: 'Home' },
            { label: Languages.category, key: 'Category' },
            { label: Languages.Map, key: 'Map' },
            { label: Languages.profile, key: 'Profile' },
            { label: Languages.setting, key: 'setting' },
            { label: Languages.contact, key: 'contact' },
            { label: Languages.logout, key: 'logout' },
        ],
        isLoad:false,
    }
}    


componentWillMount = async() =>{
    let user = await AsyncStorage.getItem('User');
    
    this.setState({
        user:JSON.parse(user),
    })
}
renderMenu = (route) => {
const onpress = (route.key === 'logout') ?
() => this.logOut()
: () =>{
    if(route.key ==='Home'){
        this.props.navigation.closeDrawer();
     }else if(route.key ==='setting'){
        this.props.navigation.closeDrawer();
     }else if(route.key ==='contact'){
        this.props.navigation.closeDrawer();
     }else if(route.key ==='login'){
        this.props.navigation.closeDrawer();
     }
     else if(route.key ==='Category'){
        this.props.navigation.closeDrawer();
     }
     else if(route.key ==='Map'){
        this.props.navigation.closeDrawer();
     }
     else if(route.key ==='Profile'){
        this.props.navigation.closeDrawer();
     }
    this.props.navigation.navigate(route.key)
} 
return (
<TouchableOpacity onPress={onpress} style={[Styles.menuview,route.key =='Map'?{marginRight:10}:(route.key =='Category'?{marginLeft:24}:(route.key =='login'?{marginRight:8}:(route.key =='Profile'?{marginLeft:8}:(route.key =='setting'?{marginLeft:8}:(route.key =='contact'?{marginLeft:8}:{})))))]}>
<Text style={Styles.menutext}>{route.label}</Text>
</TouchableOpacity>
)
}

logOut =  () => {
    const {navigation,Users} = this.props;
         this.setState({ isLoad:true })
         AsyncStorage.clear();
         this.props.dispatch({type:'logout_success'}) 
         setTimeout(() => {
            ToastHelper.success(Languages.logoutMessage)  
            this.setState({ isLoad:false })
            navigation.navigate('main')
        }, 100);
}


render(){
    const {user,routeLogin,routeLogout,isLoad}=this.state
    const route=user?routeLogout:routeLogin
    // if(isLoad == true){
    //     return(
    //         <Loadings />
    //     )
    // }
return(
<View style={Styles.container}>
{route
.map(route => <View key={route.key}>
{this.renderMenu(route)}</View>)}
</View>
)
}

}

const mapStateToProps=(state)=>{
    return{
      isFetching:state.Auth.isFetching,
      vcode: state.Auth.VCode,
      register:state.Auth.Register,
      issue:state.Issue.issues,
      Users:state.Auth.UserInfo,
      getwishlist:state.Post.getWishlist,
    }
    }
    
  export default connect(
      mapStateToProps
    )(Drawer)