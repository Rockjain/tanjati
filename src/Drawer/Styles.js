import { StyleSheet } from 'react-native';
import Colors from '../Config/Colors';
export default StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
         alignItems:'center'
    },
    menuview:{
        marginVertical:20
    },
    menutext:{
        fontSize:20,
        color:Colors.attributes.black
    }
})