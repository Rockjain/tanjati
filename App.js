
import React, {Component} from 'react';
import Route from './src/Navigation';
import { Platform,AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import Store from './src/Redux/Store';
import { Root} from 'native-base';
import firebase from 'react-native-firebase';
import { RemoteMessage } from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
import { MenuProvider } from 'react-native-popup-menu';
import Axios from 'axios';
import Constants from './src/Config/Constants';
import ToastHelper from './src/Config/ToastHelper';
import Loadings from './src/Components/Loadings';
import Languages from './src/Config/Languages';

  class App extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        myuser:null
      }
    }
  async componentDidMount() {
    let userinfo = await AsyncStorage.getItem('User');
    this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
    user=JSON.parse(userinfo)
    if(user){
      this.props.dispatch({type:'Get_AllIssue_Request', payload:{url:'/api/rest/issues?filter_id=reported'}})
      this.setState({ myuser:user })
    } 
    this.createNotificationListeners();
  }
  async componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    let userinfo = await AsyncStorage.getItem('User');
    user=JSON.parse(userinfo)
    if (user) {
    }
    this.notificationListener();
    this.notificationOpenedListener(); 
  }
  
  async createNotificationListeners() {
    
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
        console.log('heloooo')
        console.log(notification)
        this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
        const channelId = new firebase.notifications.Android.Channel('Tanjati', 'Tanjati', firebase.notifications.Android.Importance.Max);
        firebase.notifications().android.createChannel(channelId);
            console.log('in foreground')
                notification
                .android.setChannelId('Tanjati')
                 .android.setLargeIcon('ic_iconss')
                 .android.setSmallIcon('ic_notification')
                 .android.setTicker('Tanjati')
                 .android.setAutoCancel(true)
                 .setSound('default')
            firebase.notifications().displayNotification(notification);
    });
    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
       this.props.dispatch({type:'Fetch_Post_Request',payload:{url:'/listing'}})
      Axios.get(Constants.Base_WP_url+'/listing',{
    }).then((res)=>{
      console.log('res')
      console.log(res)
      if(res.status == 200){
        console.log('notificationOpen') 
        console.log(notificationOpen)
          console.log('post')
          console.log(this.props.posts)
          console.log('post notification id')
          console.log(notificationOpen.notification.data.tid)
          if (res.data && res.data.length != 0) {
            if (notificationOpen.notification.data.type == 'post') {
              console.log('in action')
              const rest = res.data.filter(function(item){
                return (item.id == notificationOpen.notification.data.tid)
              })
              console.log('rest')
              console.log(rest)
              this.navigator &&
              this.navigator.dispatch(
                NavigationActions.navigate({routeName: 'postdetail',params: {items:rest[0], navigateTo:'Home'}})
              );
            }
          }
            if (this.props.issue && this.props.issue.length != 0) {
              if(notificationOpen.notification.data.type == 'issue'){
                if (this.state.myuser != null) {
                  console.log('issue')
                  console.log(this.props.issue)
                  const restissue = this.props.issue.filter(function(item){
                    return (item.id == notificationOpen.notification.data.tid)
                  })
                  console.log('restissue')
                  console.log(restissue)
                  this.navigator &&
                  this.navigator.dispatch(
                    NavigationActions.navigate({routeName: 'issuedetail',params: {getissue:restissue[0]}})
                  );
                } else {
                  ToastHelper.danger(Languages.hasloggedout)
                  this.navigator &&
                  this.navigator.dispatch(
                    NavigationActions.navigate({routeName: 'home'})
                  );
                }
               
              } 
            }
      }
    })
      const { title, body } = notificationOpen.notification;        
        console.log(body)        
        notificationOpen.notification
         .android.setAutoCancel(true)
    });
    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpens = await firebase.notifications().getInitialNotification();
    console.log('notificationOpen2') 
      console.log(notificationOpens) 
        if (notificationOpens) {
          AsyncStorage.setItem('ID', notificationOpens.notification.data.tid.toString())
          AsyncStorage.setItem('TYPE', notificationOpens.notification.data.type)
      }
    
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log('message')
      console.log(message)
    });
  }  
  
  render() {
    return(
      <MenuProvider>
        <Root>
          <Route ref={nav =>
            this.navigator = nav
          } />
        </Root>
        </MenuProvider>
    )
  }
}

const mapStateToProps=(state)=>{
  return{
     posts:state.Post.posts,
     issue:state.Issue.issues,
     isFetchingPost:state.Post.isFetching,
     isFetchingIssue:state.Issue.isFetching,
  }
}
export default connect(mapStateToProps)(App)
